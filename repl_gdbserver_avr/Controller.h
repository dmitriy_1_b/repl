/*This file is provided under license: see license.txt*/


/* 
 * File:   Controller.h
 * Author: root
 *
 * Created on September 8, 2019, 2:54 AM
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H
#define _WIN32_WINNT 0x0501

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string> 
#include <stdint.h>
#include <sstream>
#include <algorithm>    
//#include <windows.h>      // Needed for all Winsock stuff
//#include <ws2tcpip.h>
//#include <winsock2.h>
#include <time.h>
#include "Buffer.h"
#include <iostream> 
#include <map> 
#include <iterator> 
#include "../repl_common/usb.h"
#include "../repl_common/common.h"

using namespace std;

#define REPL_IS_BREAK 0//return from break point
#define REPL_IS_STEP 1//return after step;
#define REPL_STEP 2
#define REPL_IS_STEP_CALL_AFTER_BREAK 3
#define REPL_BREAK_POINT_FIRST_INSTRUCTION 4
#define REPL_BREAK_POINT_SECOND_INSTRUCTION 5
#define REPL_IS_PAUSE 6//only by pause;
#define REPL_IS_BREAKPOINT_SET 7

#define REPL_COMMAND_NOP 0
#define REPL_COMMAND_RUN 1
#define REPL_COMMAND_WRITE_FLASH 2
#define REPL_COMMAND_WRITE_RAM 3
#define REPL_COMMAND_WRITE_EEPROM 4
#define REPL_COMMAND_READ_FLASH 5
#define REPL_COMMAND_READ_RAM 6
#define REPL_COMMAND_READ_EEPROM 7
#define REPL_COMMAND_PAUSE 8
#define REPL_COMMAND_WRITE_AND_REWRITE_FLASH 9
#define REPL_COMMAND_STEP 10
#define REPL_COMMAND_SET_BREAKPOINT 11
#define REPL_COMMAND_DELETE_ALL_BREAKPOINTS 12
#define REPL_COMMAND_DELETE_BREAKPOINTS 13
#define REPL_COMMAND_READ_NEAR_PC 14
#define REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT 15

#define REPL_START_BYTE 0xb7//just random number
#define DATA_START 0x800000
#define DATA_END 0x8008FF
#define EEPROM_START 0x810000

#define MIN_BYTES_BETWEEN_SEGMENTS 128
#define RECEIVE_BUFF_LENGTH 10*1024//100kByte
#define MAX_SEGMENT_SIZE 1024*1024*1024//big number
#define BUFFER_TIME_OUT_S 30
#define REPL_INFO_SIZE 42
#define PORTION_SIZE 256

class Controller {
    typedef struct __attribute__((__packed__))  replStructRegisters{
		uint8_t sreg;
		uint8_t r31;
		uint8_t r30;
		uint8_t r29;
		uint8_t r28;
		uint8_t r27;
		uint8_t r26;
		uint8_t r25;
		uint8_t r24;
		uint8_t r23;
		uint8_t r22;
		uint8_t r21;
		uint8_t r20;
		uint8_t r19;
		uint8_t r18;
		uint8_t r17;
		uint8_t r16;
		uint8_t r15;
		uint8_t r14;
		uint8_t r13;
		uint8_t r12;
		uint8_t r11;
		uint8_t r10;
		uint8_t r9;
		uint8_t r8;
		uint8_t r7;
		uint8_t r6;
		uint8_t r5;
		uint8_t r4;
		uint8_t r3;
		uint8_t r2;
		uint8_t r1;
		uint8_t r0;
		uint8_t pch;
		uint8_t pcl;
	};
public:
    Controller();
    virtual string readMemory(uint32_t address, uint32_t length);
    virtual void writeMemory(uint32_t address, uint32_t length, string data);
    virtual void setBreakPoint(uint32_t address);
    virtual void deleteBreakPoint(uint32_t address);
    virtual string readRegisters();
    virtual void writeRegister(uint32_t address, string value);
    virtual bool isPause();
    virtual string pause();
    virtual void run();
    virtual void step();
    virtual bool isCheckIsBreak();
    virtual ~Controller();
    virtual bool isLastCommandRun();
private:
    char HOST[1024];
    char CONTROLLER_ID[1024];
    bool _useGSM;
    void _readNearProgramCounter(int len); 
    void _updateRegisters();
    void _deleteUnSetBreakPoints();
    uint32_t _lastReadTime;
    Buffer _buffer;
    uint8_t _recvbuf[RECEIVE_BUFF_LENGTH];
    int _sendData(uint8_t* data, uint32_t size, bool check=false);
    int _sendPost(uint8_t* data, uint32_t size, bool check=false, int timeOutSec=30);
    uint8_t _replFlagsRegister;
    uint16_t _stackPointer;
    uint32_t _replInterrupts;
    replStructRegisters _regs;
    bool _lastCommandRun;
    bool _lastCommandStep;
    map <uint32_t,bool> _breakPoints;
};

#endif /* CONTROLLER_H */

