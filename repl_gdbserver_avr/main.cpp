/*This file is provided under license: see license.txt*/
#define _WIN32_WINNT 0x0501

//#undef UNICODE

#define WIN32_LEAN_AND_MEAN

//#include <windows.h>
//#include <winsock2.h>
//#include <ws2tcpip.h>
//#include <sys/ioctl.h>
//#include <netdb.h>
//#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "Controller.h"
#include "GDBDecoder.h"
#include "../repl_common/common.h"
using namespace std;

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")
// #pragma comment (lib, "Mswsock.lib")

#define DEFAULT_BUFLEN 1024
#define DEFAULT_PORT 27015

char recvbuf[2 * DEFAULT_BUFLEN];

int main(void) {
    TcpServer server;
    Controller controller;
    GDBDecoder decoder(&controller);
    char* pointer;
    int recvbuflen = DEFAULT_BUFLEN;
    int iResult, iSendResult;

    server.init(DEFAULT_PORT);
    server.waitClient();
    server.acceptClient();
    bzero(recvbuf, recvbuflen);
    // Receive until the peer shuts down the connection
    pointer = recvbuf; // p &recvbuf
    FILE * logFile;
    //logFile = fopen("log.txt", "a");
    do {
        iResult = server.readClient(pointer, recvbuflen);
        if (iResult > 0) {
            pointer[iResult] = 0x00;
            string input = pointer;
            string output;
            if (iResult == 1 && pointer[iResult - 1] == '+') {
                continue;
            }
            logFile = fopen("log.txt", "a");
            fprintf(logFile, "Input:");
            fprintf(logFile, input.c_str());
            fprintf(logFile, "\r\n");
            decoder.decode(input, output);
            fprintf(logFile, "Output:");
            fprintf(logFile, output.c_str());
            fprintf(logFile, "\r\n");
            fclose(logFile);
            //pointer = pointer + iResult;
            iSendResult = server.writeClient(output.c_str(), output.length());

            if (iSendResult == -1) {
                return 1;
            }
            if (decoder.isLastCommandRun()) {
                server.unblockClient();
            } else {
                server.blockClient();
            }
        } else if (errno != EWOULDBLOCK) {
            break;
        } else {
            string output = decoder.isCheckIsBreak();
            if (output.length() > 0) {
                iSendResult = server.writeClient(output.c_str(), output.length());

                if (iSendResult == -1) {
                    return 1;
                }
                server.blockClient();
            } else {
                Sleep(1000);
            }
        }
    } while (((pointer - recvbuf) < DEFAULT_BUFLEN));
    server.closeClient();

    return 0;
}