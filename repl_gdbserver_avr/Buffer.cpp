/*This file is provided under license: see license.txt*/


/* 
 * File:   Buffer.cpp
 * Author: root
 * 
 * Created on September 18, 2019, 11:21 AM
 */

#include "Buffer.h"

Buffer::Buffer() {
    _len = 0;
    _pointers = NULL;
}

Buffer::~Buffer() {
}

void Buffer::set(uint8_t* buff, uint32_t address, uint32_t length) {

    uint8_t* newBuff;
    bool flag;
    do {
        flag = false;
        for (int i = 0; i < _len; ++i) {
            if (_pointers[i] == NULL)
                continue;
            uint32_t* a = (uint32_t*) _pointers[i];
            uint32_t* l = (uint32_t*) (_pointers[i] + 4);
            //outside portion
            if ((address <= *a) && ((address + length) >= (*a + *l))) {
                free(_pointers[i]);
                _pointers[i] = NULL;
                flag = true;
                break;
                //inside portion
            } else if ((*a <= address) && ((*a + *l) >= (address + length))) {
                memcpy((_pointers[i] + 8 + address - *a), buff, length);
                return;
                //cross portion in the right    
            } else if ((*a <= address) && ((*a + *l) >= address)) {
                newBuff = (uint8_t*) malloc(address - *a + length);
                memcpy(newBuff, (_pointers[i] + 8), address - *a);
                memcpy(newBuff + address - *a, buff, length);
                length = address - *a + length;
                address = *a;
                buff = newBuff;
                flag = true;
                free(_pointers[i]);
                _pointers[i] = NULL;
                break;
                //cross portion in the left  
            } else if ((address <= *a) && ((address + length) >= *a)) {
                newBuff = (uint8_t*) malloc(*a - address + *l);
                memcpy(newBuff, buff, length);
                memcpy(newBuff + length, (_pointers[i] + 8) + length + address - *a, *a + *l - address - length);
                length = *a - address + *l;
                buff = newBuff;
                flag = true;
                free(_pointers[i]);
                _pointers[i] = NULL;
                break;
            }
        }
    } while (flag);
    //delete nulls
    int count = 0;
    for (int i = 0; i < _len; ++i) {
        if (_pointers[i] != NULL) {
            _pointers[count] = _pointers[i];
            ++count;
        }
    }
    if (count != _len) {
        _len = count;
        _pointers = (void_pointer*) realloc(_pointers, (_len) * sizeof (void_pointer));
    }
    if (_len == 0)
        _pointers = NULL;
    //create new portion
    ++_len;
    void * p = malloc(length + 4 + 4);
    if (_pointers != NULL)
        _pointers = (void_pointer*) realloc(_pointers, (_len) * sizeof (void_pointer));
    else
        _pointers = (void_pointer*) malloc((_len) * sizeof (void_pointer));
    _pointers[_len - 1] = (uint8_t*) p;
    memcpy(p, (void*) &address, 4);
    p += 4;
    memcpy(p, (void*) &length, 4);
    p += 4;
    memcpy(p, (void*) buff, length);
}

uint8_t* Buffer::get(uint32_t address, uint32_t length) {
    for (int i = 0; i < _len; ++i) {
        uint32_t* a = (uint32_t*) _pointers[i];
        uint32_t* l = (uint32_t*) (_pointers[i] + 4);
        if ((*a <= address) && ((*a + *l) >= (address + length))) {
            return (uint8_t*) _pointers[i] + 8 + address - *a;
        }
    }
    return NULL;
}

void Buffer::clear(void) {
    for (int i = 0; i < _len; ++i) {
        free(_pointers[i]);
    }
    free(_pointers);
    _len = 0;
}