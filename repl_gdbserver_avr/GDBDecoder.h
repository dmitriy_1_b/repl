/*This file is provided under license: see license.txt*/


/* 
 * File:   GDBDecoder.h
 * Author: root
 *
 * Created on September 15, 2019, 8:40 PM
 */

#ifndef GDBDECODER_H
#define GDBDECODER_H
#include "Controller.h"

class GDBDecoder {
public:
    GDBDecoder(Controller* controller);
    virtual void decode(string &input, string &output);
    virtual string isCheckIsBreak();
    virtual bool isLastCommandRun();
    virtual ~GDBDecoder();
private:
Controller* _controller;
};

#endif /* GDBDECODER_H */

