#include <stdlib.h>
#include <iostream>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../GDBDecoder.h"
#include "../Controller.h"
#include "MockController.h"
using namespace std;
using ::testing::Return;


TEST(GDBDecoderTest, decode) {
    MockController cont;
    string ret="AABB";
    EXPECT_CALL(cont, readMemory(0x4015bc,0x02))
    .WillOnce(Return(ret));
    GDBDecoder dec(&cont);
    string out;
    string in="$m4015bc,2#5a";
    dec.decode(in,out);
    EXPECT_STREQ(out.c_str(),"+$AABB#06");
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
