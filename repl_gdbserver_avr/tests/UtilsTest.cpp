#include <stdlib.h>
#include <iostream>
#include "gtest/gtest.h"
#include "../Utils.h"
using namespace std;

TEST(UtilsTest, substr) {
    Utils ut;        
    EXPECT_STREQ(ut.substr("Hello $mWork#","$m","#").c_str(),"Work");
    EXPECT_STREQ(ut.substr("Hello $AmWork#BC","$Am","#B").c_str(),"Work");
    EXPECT_STREQ(ut.substr("Hello $AmWork#BCDFFDS","$Am","#BCDF").c_str(),"Work");
    EXPECT_STREQ(ut.substr("Hello $AmWork#BCDFFDS","$Am","#BCDR").c_str(),"");
}

TEST(UtilsTest, hex2int) {
    Utils ut;        
    EXPECT_EQ(ut.hex2int("AA"),0xAA);
    EXPECT_EQ(ut.hex2int("01"),0x01);
    EXPECT_EQ(ut.hex2int("2369"),0x2369);
    EXPECT_EQ(ut.hex2int("4015BC"),0x4015bc);
    EXPECT_EQ(ut.hex2int("4015bc"),0x4015bc);
}

TEST(UtilsTest, checkSum) {
    Utils ut;        
    EXPECT_STREQ(ut.checkSum("AABBCC012569").c_str(),"C3");
    EXPECT_STREQ(ut.checkSum("").c_str(),"00");
}

TEST(UtilsTest,crc16){
    Utils ut;        
    uint16_t crc=0xffff;
    ut.crc16(0x03,crc);
    ut.crc16(0x66,crc);
    ut.crc16(0x58,crc);
    ut.crc16(0xf3,crc);
    EXPECT_EQ(crc,0xFA9B);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
