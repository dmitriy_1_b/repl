/*This file is provided under license: see license.txt*/

/* 
 * File:   main.cpp
 * Author: root
 *
 * Created on July 12, 2019, 10:25 AM
 */
#define _WIN32_WINNT 0x0501

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string> 
#include <sstream>
#include <algorithm>
//#include <sys/socket.h> 
//#include <arpa/inet.h> 
//#include <netdb.h>    
//#include <windows.h>      // Needed for all Winsock stuff
//#include <ws2tcpip.h>
//#include <winsock2.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../repl_common/common.h"

using namespace std; 

//#define MIN_BYTES_BETWEEN_SEGMENTS 128
#define MAX_SEGMENT_SIZE 1024*1024*1024//big number
//#define CONTROLLER_ID "164588878643469"
//#define REPL_NEW_BIN "./repl.bin"
//#define REPL_OLD_BIN "./replOld.bin"
#define CONFIG "./config.properties"
char HOST[1024];
char ELF_NAME[1024];
char OBJ_COPY_PATH[1024];
char CONTROLLER_ID[1024];
uint32_t MIN_BYTES_BETWEEN_SEGMENTS;
uint32_t FLASH_OFFSET;
uint32_t LOAD_ADDRESS;

static void replWriteUint32(uint8_t * buff, uint32_t i) {
    buff[0] = i >> 24;
    buff[1] = (i >> 16)&0xff;
    buff[2] = (i >> 8)&0xff;
    buff[3] = i & 0xff;
}

void strTrim(char* str) {
    int pos = strlen(str) - 1;
    while ((str[pos] == '\n') || (str[pos] == '\r') || (str[pos] == '\t') || (str[pos] == ' ')) {
        str[pos] = 0x00;
        if (pos > 0) {
            --pos;
        } else {
            break;
        }
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    FILE * config = fopen(CONFIG, "r");
    char str[1024];
    while (!feof(config)) {
        if (fgets(str, 1023, config) != NULL) {
            char* p = strchr(str, '=');
            if(p==NULL)
                break;
            *p = 0x00;
            if (strcmp(str, "ELF_NAME") == 0) {
                ++p;
                strcpy(ELF_NAME, p);
                strTrim(ELF_NAME);
            } else if (strcmp(str, "OBJ_COPY_PATH") == 0) {
                ++p;
                strcpy(OBJ_COPY_PATH, p);
                strTrim(OBJ_COPY_PATH);
            } else if (strcmp(str, "MIN_BYTES_BETWEEN_SEGMENTS") == 0) {
                ++p;
                //strcpy(MIN_BYTES_BETWEEN_SEGMENTS, p);
                strTrim(p);
                MIN_BYTES_BETWEEN_SEGMENTS=atol(p);
            } else if (strcmp(str, "CONTROLLER_ID") == 0) {
                ++p;
                strcpy(CONTROLLER_ID, p);
                strTrim(CONTROLLER_ID);
            } else if (strcmp(str, "FLASH_OFFSET") == 0) {
                ++p;
                //strcpy(MIN_BYTES_BETWEEN_SEGMENTS, p);
                strTrim(p);
                FLASH_OFFSET=(long)strtol(p, NULL, 0);
            } else if (strcmp(str, "HOST") == 0) {
                ++p;
                strcpy(HOST, p);
                strTrim(HOST);
            } else if (strcmp(str, "LOAD_ADDRESS") == 0) {
                ++p;
                //strcpy(MIN_BYTES_BETWEEN_SEGMENTS, p);
                strTrim(p);
                LOAD_ADDRESS=(long)strtol(p, NULL, 0);
            }
        } else {
            break;
        }
    }
    fclose(config);

    system(((std::string)""+OBJ_COPY_PATH+" -O binary "+ELF_NAME+".elf "+ELF_NAME+".bin").c_str());
    ifstream binFile, oldFile;
    binFile.open((std::string)"./"+ELF_NAME+".bin", ios::in | ios::binary);
    if (!binFile.is_open()) {
        return -1;
    }
    binFile.seekg(0, ios::end);
    uint32_t size = binFile.tellg();
    oldFile.open((std::string)"./"+ELF_NAME+"Old.bin", ios::in | ios::binary);
    if (!oldFile.is_open()) {
        return -1;
    }
    uint8_t bin[size];
    binFile.seekg(0, ios::beg);
    binFile.read((char*) bin, size);
    binFile.close();
    uint8_t old[size];
    oldFile.seekg(0, ios::beg);
    oldFile.read((char*) old, size);
    oldFile.close();
    uint32_t maxSize = size + 8 + 1;
    uint8_t result[maxSize];
    uint32_t resultSize = 1;
    result[0] = 0; //segments count
    uint32_t segmentEnd = 0;
    uint32_t segmentSize = 0;
    int32_t counter = 0;
    bool flag = false;
    bool biggerMaxSize = false;
    for (int i = 0; i < size; ++i) {
        if (bin[i] != old[i]) {
            counter = MIN_BYTES_BETWEEN_SEGMENTS;
        } else {
            if (counter >= 0)
                --counter;
        }
        if (counter >= 0) {
            if (flag == false) {
                if (resultSize + 9 < maxSize) {
                    segmentSize = 1;
                    replWriteUint32(&(result[resultSize]), i+FLASH_OFFSET);
                    resultSize += 4;
                    segmentEnd = resultSize;
                    replWriteUint32(&(result[resultSize]), i+FLASH_OFFSET);
                    resultSize += 4;
                    result[resultSize] = bin[i];
                    ++resultSize;
                    flag = true;
                    ++result[0]; //segments count
                } else {
                    biggerMaxSize = true;
                    break;
                }
            } else {
                if (resultSize + 1 < maxSize) {
                    replWriteUint32(&(result[segmentEnd]), i+FLASH_OFFSET);
                    result[resultSize] = bin[i];
                    ++resultSize;
                    ++segmentSize;
                    //Check max segment size
                    if (segmentSize >= MAX_SEGMENT_SIZE) {
                        replWriteUint32(&(result[segmentEnd]), i+FLASH_OFFSET - MIN_BYTES_BETWEEN_SEGMENTS + counter);
                        resultSize -= MIN_BYTES_BETWEEN_SEGMENTS - counter;
                        counter = -1;
                        flag = false;
                    }
                } else {
                    biggerMaxSize = true;
                    break;
                }
            }
        } else {
            if (flag) {
                replWriteUint32(&(result[segmentEnd]), i+FLASH_OFFSET - MIN_BYTES_BETWEEN_SEGMENTS - 1);
                resultSize -= MIN_BYTES_BETWEEN_SEGMENTS;
            }
            flag = false;
        }
    }
    if (flag) {
        replWriteUint32(&(result[segmentEnd]), size+FLASH_OFFSET - MIN_BYTES_BETWEEN_SEGMENTS + counter - 1);
        resultSize -= MIN_BYTES_BETWEEN_SEGMENTS - counter;
        result[0] = 1; //segments count
    }

    if (biggerMaxSize) {
        replWriteUint32(&(result[0]), FLASH_OFFSET);
        replWriteUint32(&(result[4]), size+FLASH_OFFSET);
        memcpy(&(result[8]), bin, size);
        resultSize = size + 8;
    }

    if (resultSize == 1) {
        return 0;
    }
    TcpClient client;
    int res = client.connectToServer(HOST,"80");
    if (res == -1) {
        return false;
    }

    string header;
    header = "POST /FirmwareSaver.php?controller_id=";
    header += CONTROLLER_ID;
    header += " HTTP/1.1\r\n";
    header += "Host: ";
    header += HOST;
    header += "\r\n";
    header += "Content-Type: binary/octet-stream\r\n";
    header += "Content-Length: ";
    //mingw glitch https://stackoverflow.com/questions/12975341/to-string-is-not-a-member-of-std-says-g-mingw
    std::ostringstream os;
    os << resultSize+4;
    header += os.str();
    header += "\r\n";
    header += "\r\n";
    
    uint8_t sendBuff[resultSize+4+header.size()];
    memcpy(sendBuff,header.c_str(),header.size());
    uint8_t loadAddress[5];
    replWriteUint32(loadAddress,LOAD_ADDRESS);   
    memcpy(sendBuff+header.size(),loadAddress,4);
    memcpy(sendBuff+header.size()+4,result,resultSize);
    res = client.sendToServer((char*) sendBuff, resultSize+4+header.size());    
    if (res == -1) {
        return false;
    }
    //Sleep(1);    
    for (int i = 0; i < 10; ++i) {
        int recvbuflen = 1024;
        char recvbuf[recvbuflen];
        //char* point = recvbuf;
        res = client.recvFromServer(recvbuf, recvbuflen);
        if (res > 0) {
            break;
        }
        usleep(1000);
    }

    client.closeSocket();

    remove(((std::string)((std::string)"./"+ELF_NAME+"Old.bin")).c_str());

    res = rename(((std::string)((std::string)"./"+ELF_NAME+".bin")).c_str(), 
            ((std::string)((std::string)"./"+ELF_NAME+"Old.bin")).c_str());

    if (res != 0) {
        printf(((std::string)((std::string)"Unable to rename "+ELF_NAME+".bin")).c_str());
    }

    return 0;
}

