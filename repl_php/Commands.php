<?php

/*This file is provided under license: see license.txt*/

/**
 * Description of Commands
 *
 * @author root
 */
class Commands {

    const REPL_CRC_START_VALUE = 0xffff;
    const REPL_START_BYTE = 0xb7;
    const REPL_COMMAND_NOP = 0;
    const REPL_COMMAND_RUN = 1;
    const REPL_COMMAND_WRITE_FLASH = 2;
    const REPL_COMMAND_WRITE_RAM = 3;
    const REPL_COMMAND_WRITE_EEPROM = 4;
    const REPL_COMMAND_READ_FLASH = 5;
    const REPL_COMMAND_READ_RAM = 6;
    const REPL_COMMAND_READ_EEPROM = 7;
    const REPL_COMMAND_PAUSE = 8;
    const REPL_COMMAND_WRITE_AND_REWRITE_FLASH = 9;
    const REPL_COMMAND_STEP = 10;
    const REPL_COMMAND_SET_BREAKPOINT = 11;
    const REPL_COMMAND_DELETE_ALL_BREAKPOINTS = 12;
    const REPL_COMMAND_DELETE_BREAKPOINTS = 13;
    const REPL_COMMAND_READ_NEAR_PC = 14; //call if somebody try to read flash where address=pc
    const COMMAND_WRITE = 2;
    const COMMAND_WRITE_AND_REWRITE = 9;
    const MAX_SAVE_SIZE = 512;
    const LOAD_ADDRESS_AVR = 16384; //134348800-stm32f303vg; //16384;//avr - 16384
    const LOAD_ADDRESS_STM = 134348800;
    //const GET_MARK = "SELECT mark FROM `config` WHERE 1;UPDATE `config` SET mark=mark+1";
    const INSERT_RESPONSE = "INSERT INTO response  (`controller_id`,`data`, `date`) VALUES (?,?, current_timestamp(6))";
    const INSERT_STATE = "INSERT INTO `commands` (`command_id`, `mark`, `date`, `controller_id`, `command`, `data`, `active`) VALUES (NULL, ?, current_timestamp(6), ?, ?, ?, 1)";
    const SELECT_DATE_FROM_STATE = "SELECT concat(`date`,'') as date FROM commands WHERE `command_id`=?";
    const SELECT_COMMAND = "SELECT `command`, `data` FROM commands WHERE `controller_id`=? AND `active`=1 ORDER BY `command_id` ASC LIMIT 1";
    const UPDATE_COMMAND_STATE = "UPDATE commands SET `active`=0 WHERE `controller_id`=? AND `active`=1 ORDER BY `command_id` ASC LIMIT 1";
    const SELECT_FROM_RESPONSE = "SELECT data FROM `response` WHERE controller_id=? and date>=? order by date desc limit 1"; //and OCTET_LENGTH(data)>42  
    const SELECT_DATA_FROM_RESPONSE = "SELECT data FROM `response` WHERE controller_id=? and OCTET_LENGTH(data)>=? and date>=? order by date desc limit 1"; //  

    public function saveFirmware($controller_id, $data) {
        $dt = DatabaseManager::getInstance();
        $mark = $this->getMark();
        $loadAddress=unpack("N",substr($data, 0, 4))[1];
        $data=substr($data,4);
        $start = 0;
        $send = substr($data, $start, self::MAX_SAVE_SIZE);
        while ($send != '') {
            $next = substr($data, $start + self::MAX_SAVE_SIZE, self::MAX_SAVE_SIZE);
            $crc = self::REPL_CRC_START_VALUE;
            $dataStartPart = pack("C", self::REPL_START_BYTE) . pack("N", strlen($send) + 8);
            for ($i = 0; $i < 5; ++$i) {
                $val = unpack("C", $dataStartPart[$i])[1];
                $this->crc16($val, $crc);
            }
            $dataStartPart = $dataStartPart . pack("n", $crc);
            $cmd = null;
            if ($next != '') {
                $cmd = self::COMMAND_WRITE;
            } else {
                $cmd = self::COMMAND_WRITE_AND_REWRITE;
            }
            $dataEndPart = pack("C", $cmd) . pack("N", $loadAddress + $start) .
                    pack("N", $loadAddress + $start + strlen($send) - 1) .
                    $send;
            for ($i = 0; $i < strlen($dataEndPart); ++$i) {
                $val = unpack("C", $dataEndPart[$i])[1];
                $this->crc16($val, $crc);
            }
            $dt->insertRow(self::INSERT_STATE, array($mark, $controller_id, $cmd,
                $dataStartPart . $dataEndPart . pack("n", $crc)
            ));
            $send = $next;
            $start += self::MAX_SAVE_SIZE;
        }
    }

    public function crc16($addValue, &$crc) {
        $addValue = $addValue & 0xff;
        ////CRC-16/CCITT-FALSE
        //    uint8_t x;
        //    x = crc >> 8 ^ addValue;
        //    x ^= x >> 4;
        //    crc = (crc << 8) ^ ((uint16_t) (x << 12)) ^ ((uint16_t) (x << 5)) ^ ((uint16_t) x);
        //    return;
        $crc ^= $addValue; // XOR byte into least sig. byte of crc
        for ($i = 8; $i != 0; $i--) { // Loop over each bit
            if (($crc & 0x0001) != 0) { // If the LSB is set
                $crc >>= 1; // Shift right and XOR 0xA001
                $crc ^= 0xA001;
            } else // Else LSB is not set
                $crc >>= 1; // Just shift right
        }
        $crc = $crc & 0xffff;
    }

    public function saveResponse($controller_id, $data) {
        $dt = DatabaseManager::getInstance();
        $dt->insertRow(self::INSERT_RESPONSE, array($controller_id, $data));
    }

    public function saveCommand($controller_id, $data) {
        $dt = DatabaseManager::getInstance();
        $mark = $this->getMark();
        $command = unpack("C1command", $data[7])['command'];
//        $start = 1;
//        $data = substr($data, 1);
        $id = $dt->insertRow(self::INSERT_STATE, array($mark, $controller_id, $command,
            $data));
        $array = $dt->readData(self::SELECT_DATE_FROM_STATE, array($id));
        if (count($array) > 0) {
            return $array[0]['date'];
        } else {
            return NULL;
        }
    }

    public function getEarliestActiveCommand($controller_id) {
        $dt = DatabaseManager::getInstance();
        $array = $dt->readData(self::SELECT_COMMAND, array($controller_id));
        if (count($array) > 0) {
            return $array[0];
        } else {
            return NULL;
        }
    }

    public function getResponse($controller_id, $date) {
        $dt = DatabaseManager::getInstance();
        $array = $dt->readData(self::SELECT_FROM_RESPONSE, array($controller_id, $date));
        if (count($array) > 0) {
            return $array[0]['data'];
        } else {
            return NULL;
        }
    }

    public function getResponseWithData($controller_id, $date, $data_length_awaiting) {
        $dt = DatabaseManager::getInstance();
        $array = $dt->readData(self::SELECT_DATA_FROM_RESPONSE, array($controller_id, $data_length_awaiting, $date));
        if (count($array) > 0) {
            return $array[0]['data'];
        } else {
            return NULL;
        }
    }

    public function deactivateEarliestActiveCommand($controller_id) {
        $dt = DatabaseManager::getInstance();
        $dt->updateDeleteRow(self::UPDATE_COMMAND_STATE, array($controller_id));
    }

    private function getMark() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < 10; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }

        return $randomString;
    }

}
