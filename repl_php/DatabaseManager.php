<?php
/*This file is provided under license: see license.txt*/
class DatabaseManager {

    /**
     *
     * @var DatabaseManager 
     */
    private static $instance;

    const DRIVER_FILE = 'base.ini';
    const SERVER = 'server';
    const DATABASE = 'database';
    const PASSWORD = 'password';
    const USER = 'user';
    const PORT = 'port';
    const MYSQL_EXCEPTION = 'MYSQL EXCEPTION: ';
    const MYSQL_EXCEPTION_DUPLICATE = 'Duplicate';

    /**
     *
     * @var \mysqli 
     */
    private $mysqli;

    /**
     * 
     * @return DatabaseManager
     */
    public static function getInstance() {

        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        $params = parse_ini_file(self::DRIVER_FILE);
        if ($params == FALSE) {
            throw new Exception('Faile to open base.ini');
        }
        $db = new \mysqli($params[self::SERVER], $params[self::USER], $params[self::PASSWORD], $params[self::DATABASE], $params[self::PORT]);
        if ($db->connect_errno) {
            throw new Exception('Cannot connect to the server:' . $db->connect_error);
        }
        $db->set_charset('utf8');
        $this->mysqli = $db;
    }

    /**
     * 
     * @param string $request
     * @param array $params
     * @return array
     * @throws Exception
     */
    public function readData($request, $params = array()) {
        $stmt = $this->prepare($request, $params);
        $stmt->store_result();
        while ($row = $this->fetchAssocStatement($stmt)) {
            $array[] = $row;
        }
        return $array;
    }

    /**
     * 
     * @param string $request
     * @param array $params
     * @return Integer|boolean
     * @throws Exception
     */
    public function insertRow($request, $params = array()) {
        $stmt = $this->prepare($request, $params);
        $stmt->close();
        return $this->mysqli->insert_id;
    }

    /**
     * 
     * @param string $request
     * @param array $params
     * @return boolean
     * @throws Exception
     */
    public function updateDeleteRow($request, $params = array()) {
        $stmt = $this->prepare($request, $params);
        $stmt->close();
        return true;
    }

    private function refValues($arr) {
        $refs = array();
        foreach ($arr as $key => $value)
            $refs[$key] = &$arr[$key];
        return $refs;
    }

    /**
     * 
     * @param type $request
     * @param type $params
     * @return \mysqli_stmt
     */
    private function prepare(&$request, $params) {
        $stmt = $this->mysqli->prepare($request);
        if (count($params)) {
            $types = '';
            if (is_array($params)) {
                foreach ($params as $param) {
                    if (is_string($param)) {
                        $types .= 's';
                    } elseif (is_integer($param)) {
                        $types .= 'i';
                    } elseif (is_float($param)) {
                        $types .= 'd';
                    } else {
                        $types .= 's';
                    }
                }
            }
            array_unshift($params, $types);
            call_user_func_array(array($stmt, 'bind_param'), $this->refValues($params));
        }
        if ($stmt === false) {
            throw new \Exception(self::MYSQL_EXCEPTION . ' ,REQUEST: ' . $request);
        }
        if (!$stmt->execute()) {
            throw new \Exception(self::MYSQL_EXCEPTION . $stmt->error . ' ,REQUEST: ' . $request);
        }
        return $stmt;
    }

    private function insertParams(&$request, $params) {
        if (is_array($params)) {
            foreach ($params as $param) {
                $pos = strpos($request, '?');
                if ($pos === FALSE)
                    break;
                if (is_null($param)) {
                    $param = 'NULL';
                } else {
                    $param = "'$param'";
                }
                $request = substr_replace($request, $param, $pos, 1);
            }
        }
    }

    private function fetchAssocStatement($stmt) {
        if ($stmt->num_rows > 0) {
            $result = array();
            $md = $stmt->result_metadata();
            $params = array();
            while ($field = $md->fetch_field()) {
                $params[] = &$result[$field->name];
            }
            call_user_func_array(array($stmt, 'bind_result'), $params);
            if ($stmt->fetch())
                return $result;
        }
        return null;
    }

}
