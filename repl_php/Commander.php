<?php
/*This file is provided under license: see license.txt*/

require_once './RequestLogger.php';
require_once './Commands.php';
set_time_limit(0);
//header('Content-Type: application/octet-stream');
header('Content-Transfer-Encoding: binary');
$data=file_get_contents('php://input');
$commands = new Commands();
$commands->saveResponse($_REQUEST['controller_id'], $data);
ob_start();
/* nop 0 */
//echo 'PPPP'.chr(0).chr(0).chr(0).chr(0).chr(0);
/* write ram 3 */
//echo 'PPPP'.chr(0).chr(0).chr(0).chr(10).chr(3);
//echo chr(0).chr(0).chr(8).chr(0xFE);
//echo chr(0).chr(0).chr(8).chr(0xFF);
//echo chr(0xAA);
//echo chr(0xAB);
/* write eeprom 4 */
//echo 'PPPP'.chr(0).chr(0).chr(0).chr(10).chr(4);
//echo chr(0).chr(0).chr(3).chr(0xFE);
//echo chr(0).chr(0).chr(3).chr(0xFF);
//echo chr(0xAA);
//echo chr(0xAB);
/* read ram 6 */
//echo 'PPPP'.chr(0).chr(0).chr(0).chr(8).chr(6);
//echo chr(0).chr(0).chr(0x00).chr(0x00);
//echo chr(0).chr(0).chr(0x01).chr(0x00);
/* read eeprom 7 */
//echo 'PPPP'.chr(0).chr(0).chr(0).chr(8).chr(7);
//echo chr(0).chr(0).chr(0x00).chr(0x00);
//echo chr(0).chr(0).chr(0x01).chr(0x00);
/* read flash 5 */
//echo 'PPPP'.chr(0).chr(0).chr(0).chr(8).chr(5);
//echo chr(0).chr(0).chr(0x00).chr(0x00);
//echo chr(0).chr(0).chr(0x01).chr(0x00);
$flag = false;
for ($i = 0; $i < 200; ++$i) {
    $arr = $commands->getEarliestActiveCommand($_REQUEST['controller_id']);
    if (!is_null($arr)) {
        $flag = true;
        echo 'PPPP';/* . pack("N", strlen($arr['data'])).chr($arr['command']);*/
        echo $arr['data'];
        $commands->deactivateEarliestActiveCommand($_REQUEST['controller_id']);
        break;
    }
    usleep(100*1000);
}
if (!$flag) {
    /* nop 0 */
    echo 'PPPP' .chr(0xb7) . chr(0) . chr(0) . chr(0) . chr(0) 
            . chr(0x1a) . chr(0xd0)
            . chr(0)
            . chr(0x9c) . chr(0x1b);
}
$output = ob_get_contents();

ob_end_clean();
$checkSum = 0;
for ($i = 0; $i < strlen($output); $i++) {
    $checkSum += ord($output[$i]);
}
header('Content-Type: binary/octet-stream');
header('Content-Length: ' . (strlen($output) + 1));
echo $output;
$checkSum = chr($checkSum % 256);
echo $checkSum;

$logger = new RequestLogger();
$request_header = '';
foreach (getallheaders() as $name => $value) {
    $request_header .= "$name: $value\r\n";
}
$logger->log($request_header, $data, $output . $checkSum);
