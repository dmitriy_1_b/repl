<?php
/*This file is provided under license: see license.txt*/
require_once './DatabaseManager.php';

/**
 * Description of RequestLogger
 *
 * @author root
 */
class RequestLogger {

    const INSERT_LOG = 'INSERT INTO `log`(`date`, `request_header`, `request_body`, `response_body`) VALUES (now(),?,?,?)';

    public function log($request_header,$request_body,$response) {
        $dt = DatabaseManager::getInstance();
        $dt->insertRow(self::INSERT_LOG, array($request_header,$request_body,$response));
    }

}
