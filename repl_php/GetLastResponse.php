<?php
/*This file is provided under license: see license.txt*/
require_once './DatabaseManager.php';
require_once './Commands.php';
$saver = new Commands();
$controller_id = $_REQUEST['controller_id'];
$date = '1970-01-01 00:00:01';

header('Content-Type: binary/octet-stream');
$data = $saver->getResponse($controller_id, $date);

if (!is_null($data)) {
    header('Content-Length: ' . (strlen($data)));
    echo $data;
}
  