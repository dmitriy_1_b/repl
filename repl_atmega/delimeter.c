#include <stdio.h>
#define PART_SIZE 512
#define PART_COUNT 64

void put_data(const unsigned char* data,int i){
	char str[20];
	unsigned char check_sum=0;
	sprintf(str,"p%i",i);
	FILE *fp;
	fp=fopen(str, "wb");	
	fprintf(fp, "PPPP");
	for(int j=0;j<PART_SIZE;j++){
	check_sum=check_sum+data[j];
	}
	fwrite(data, PART_SIZE, 1, fp);
	fputc(check_sum,fp);
	fclose(fp);
}

int main(int argc, char *argv[])
{
	unsigned char data[PART_SIZE];
	printf("Start!");
	FILE *fp;
	fp=fopen("WaterCounter.bin", "rb");
	for(int i=1;i<=PART_COUNT;i++){
	fread(data, PART_SIZE, 1, fp);
	//for(int j=0;j<PART_SIZE;j++)
	//if(data[j]==0xff)
	//	printf("Indata%i,%i",i,j);
	put_data(data,i);
	}
	fclose(fp);
	getchar();
}
		
		