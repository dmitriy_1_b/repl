/*This file is provided under license: see license.txt*/
#include "modem.h"

volatile unsigned char csqLevel = 0;
volatile boolean resetByRepl = false;
volatile uint8_t cipStatus = 0;

#define BASE_DELAY_MS 500

boolean modemConnect() {

	if (!modemSendATCommand_P(F("AT+CSQ"), F("CSQ:"), F(",0"),false)) {
		return false;
	}
	if (!modemSendATCommand_P(F("AT+IFC=0,0"),F("OK"),F(""),false)) {
		return false;
	}
	
	if (!modemSendATCommand_P(F("ATE0"),F("OK"),F(""),false)) {
		return false;
	}
	
	//	char baud[15];
	//	baud[0] = 0x00;
	//	strcpy(baud, "AT+IPR=");
	//	utoa(BAUDRATE_END, baud + 7, 10);
	//	if (!modemSendATCommand(baud, F("OK"), F(""))) {
	//		return false;
	//	}
	//	uartSetBaud(BAUDRATE_END);
	//_delay_ms(BASE_DELAY_MS);
	if (!modemSendATCommand_P(F("AT+CREG?"), F("+CREG:"), F("0,1"),false)) {
		return false;
	}
	replDelayMs(BASE_DELAY_MS);
	if (!modemSendATCommand_P(F("AT+CSTT?"), F("OK"), F(APN_ONLY),false)) {
		if (!modemSendATCommand_P(F(AT_COMMAND_APN),F( "OK"),F( ""),false)) {
			return false;
		}
		replDelayMs(BASE_DELAY_MS);
	}
	if (modemSendATCommand_P(F("AT+CIPSTATUS"), F(""), F(""),true)) {
		if ((cipStatus == CIPSTATUS_PDP_DEACT)
		|| (cipStatus == CIPSTATUS_TCP_CLOSED)
		|| (cipStatus == CIPSTATUS_TCP_CLOSING)) {
			modemSendATCommand_P(F("AT+CIPSHUT"), F("SHUT OK"), F(""),false);
			replDelayMs(BASE_DELAY_MS);
			if (!modemSendATCommand_P(F(AT_COMMAND_APN), F("OK"), F(""),false)) {
				return false;
			}
			replDelayMs(BASE_DELAY_MS);
			if (!modemSendATCommand_P(F("AT+CIICR"), F("OK"), F(""),true)) {
				return false;
			}
			} else if (cipStatus == CIPSTATUS_IP_GPRSACT) {

			} else if (cipStatus == CIPSTATUS_CONNECT_OK) {
			return true;
			} else if (cipStatus == CIPSTATUS_IP_INITIAL) {
			if (!modemSendATCommand_P(F(AT_COMMAND_APN),F( "OK"),F( ""),false)) {
				return false;
			}
			if (!modemSendATCommand_P(F("AT+CIICR"), F("OK"), F(""),true)) {
				return false;
			}
			} else {
			if (!modemSendATCommand_P(F("AT+CIICR"), F("OK"), F(""),true)) {
				return false;
			}
		}
	}
	replDelayMs(BASE_DELAY_MS);
	if (!modemSendATCommand_P(F("AT+CIFSR"), F("."), F(""),false)) {
		return false;
	}
	//_delay_ms(BASE_DELAY_MS);
	if (!modemSendATCommand_P(F(AT_COMMAND_CIPSTART),
	F("CONNECT"), F(""),true)) {//CONNECT OK or ALREADY CONNECT
		return false;
	}
	replDelayMs(BASE_DELAY_MS);
	return true;
}

boolean modemSendATCommand(const char *atcommand, const char *mustInResponse1,
const char *mustInResponse2) {
	while (uartAvailable()) {
		uartRead();
	}
	uartPrint(atcommand);
	uartPrint_P(F("\r\n"));
	//while(1);
	unsigned long int delayCounter = 0;
	char response[20];
	response[0] = 0x00;
	int resLeng = 0;
	//replDelayMs(BASE_DELAY_MS);
	while (true) {
		if (delayCounter >= MODEM_COMMAND_MAX_TIME_MS) {
			return false;
		}
		if (uartAvailable()) {
			while (uartAvailable()) {
				response[resLeng] = (char) uartRead();
				if (resLeng < 18) {
					++resLeng;
					response[resLeng] = 0x00;
				}
			}

			if (strstr_P(response, F("ERROR")) != NULL) {
				return false;
			}
			
			if (strlen_P(mustInResponse1) != 0) {
				if (strstr_P(response, mustInResponse1) == NULL)
				return false;
			}

			if (strlen_P(mustInResponse2) != 0) {
				if (strstr_P(response, mustInResponse2) == NULL)
				return false;
				else
				return true;
				} else {
				return true;
			}
		}
		replDelayMs(100);
		delayCounter += 100;
	}
}
/**
*
*/
boolean modemSendATCommand_P(const char *atcommand, const char *mustInResponse1,
const char *mustInResponse2, bool additionalDelay) {
	while (uartAvailable()) {
		uartRead();
	}
	uartPrint_P(atcommand);
	uartPrint_P(F("\r\n"));
	//if ((strstr_P(atcommand, F("AT+CIPSTATUS")) != NULL)||
	//(strstr_P(atcommand, F("AT+CIPSTART")) != NULL)||
	//(strstr_P(atcommand, F("AT+CIICR")) != NULL)) {
	//replDelayMs(BASE_DELAY_MS);
	//}
	if(additionalDelay){
		replDelayMs(BASE_DELAY_MS);
	}
	unsigned long int delayCounter = 0;
	char response[100];
	response[0] = 0x00;
	int resLeng = 0;
	while (true) {
		if (delayCounter >= MODEM_COMMAND_MAX_TIME_MS) {
			return false;
		}
		if (uartAvailable()) {
			while (uartAvailable()) {
				response[resLeng] = (char) uartRead();
				if (resLeng < 98) {
					++resLeng;
					response[resLeng] = 0x00;
				}
			}

			if (strstr_P(response, F("ERROR")) != NULL) {
				return false;
			}

			if (strstr_P(response, F("+CSQ")) != NULL) {
				char *stStart = strstr_P(response, F("+CSQ: "));
				char *stEnd = NULL;
				if (stStart != NULL) {
					stEnd = strstr_P(stStart, F(","));
				}
				if (stStart == NULL || stEnd == NULL) {
					return false;
				}
				stStart += 6;
				stEnd[0] = 0x00;
				csqLevel = atol(stStart);
				return true;
			}
			//AT+CIPSTATUS
			if (strstr_P(response, F("STATE:")) != NULL) {
				if (strstr_P(response, F("IP START")) != NULL) {
					cipStatus = CIPSTATUS_IP_START;
					} else if (strstr_P(response, F("PDP DEACT")) != NULL) {
					cipStatus = CIPSTATUS_PDP_DEACT;
					} else if (strstr_P(response, F("IP INITIAL")) != NULL) {
					cipStatus = CIPSTATUS_IP_INITIAL;
					} else if (strstr_P(response, F("IP GPRSACT")) != NULL) {
					cipStatus = CIPSTATUS_IP_GPRSACT;
					} else if (strstr_P(response, F("CONNECT OK")) != NULL) {
					cipStatus = CIPSTATUS_CONNECT_OK;
					} else if (strstr_P(response, F("TCP CLOSED")) != NULL) {
					cipStatus = CIPSTATUS_TCP_CLOSED;
					} else if (strstr_P(response, F("TCP CLOSING")) != NULL) {
					cipStatus = CIPSTATUS_TCP_CLOSING;
					} else if (strstr_P(response, F("TCP CONNECTING")) != NULL) {
					replDelayMs(1000);
					cipStatus = CIPSTATUS_CONNECT_OK;
					} else if (strstr_P(response, F("IP CONFIG")) != NULL) {
					cipStatus = CIPSTATUS_IP_CONFIG;
					} else {
					cipStatus = 0;
				}
				return true;
			}

			//if (strstr_P(response, F("AT+CIPSTART")) != NULL) {
				//if (strstr_P(response, F("CONNECT OK")) != NULL) {
					//return true;
					//} else if (strstr_P(response, F("ALREADY CONNECT")) != NULL) {
					//return true;
					//} else {
					//return false;
				//}
			//}

			if (strlen_P(mustInResponse1) != 0) {
				if (strstr_P(response, mustInResponse1) == NULL)
				return false;
			}

			if (strlen_P(mustInResponse2) != 0) {
				if (strstr_P(response, mustInResponse2) == NULL)
				return false;
				else
				return true;
				} else {
				return true;
			}
		}
		replDelayMs(100);
		delayCounter += 100;
	}
}

/**
*
*/
void modemCloseConnection()
{
	modemSendATCommand_P(F("AT+CIPCLOSE=1"), F("CLOSE OK"), F(""),false);
	modemSendATCommand_P(F("AT+CIPSHUT"), F("SHUT OK"), F(""),false);
}

/**
*
*/
boolean modemEstablishConnection()
{
	boolean first = true;
	while (true) {
		if (!resetByRepl) {
			if (!first) {
				first = false;
				modemCloseConnection();
				modemTurnOff();
			}
			modemTurnOn();
			if (!modemWaitNetworkConnection()) {
				continue;
			}
			if (!modemConnect()) {
				continue;
			}
			} else {
			resetByRepl = false;
		}
		while (true) {
			if (!modemSendData()) {
				modemSendATCommand_P(F("AT+CIPCLOSE=1"), F("CLOSE OK"), F(""),false);
				replDelayMs(BASE_DELAY_MS);
				if (!modemSendATCommand_P(
				F(AT_COMMAND_CIPSTART),
				F("CONNECT OK"), F(""),false)) {
					break;
				}
				replDelayMs(BASE_DELAY_MS);
			}
			replDelayMs(10);
		}
	}
	//closeConnection();
	//turnOffModem();
	return true;
}

void modemConfig(){
	uartBegin(BAUDRATE);         
	#ifndef FIRMWARE_WITHOUT_TRANSISTOR
	//pinMode(pwSwGPRSPin, OUTPUT);       
	//setPinOut(PIN_POWER_SWITCH);
	#else
	//pinMode(pwSwGPRSPin, INPUT);       
	//setPinInput(PIN_POWER_SWITCH);
	#endif
	//digitalWrite(pwSwGPRSPin,LOW);
	//clearPin(PIN_POWER_SWITCH);
	//pinMode(pwStatusGPRSPin, INPUT);      
	//setPinInput(PIN_POWER_STATUS);
}

boolean modemWaitNetworkConnection(){
	//setPinOut(PIN_MODEM_RTS);
	unsigned long int counter=0;
	while (!modemSendATCommand_P(F("AT"),F("OK"),F(""),false)){
		if(counter>MODEM_AT_MAX_TIMES){
			errorPush(ERROR_NOT_RESPONSE_TO_AT);
			return false;
		}
		counter+=1;
		replDelayMs(BASE_DELAY_MS);
	}
	//if (!processedATCommand("AT+CPIN?", "+CPIN:", "READY")) {
	//return false;
	//}
	counter=0;
	while (!modemSendATCommand_P(F("AT+CGATT?"),F("+CGATT:"),F("1"),false)){
		if(counter>MODEM_TURN_ON_MAX_TIME_MS){
			errorPush(ERROR_NOT_CONNECT_TO_NETWORK);
			return false;
		}
		counter+=1000;
		replDelayMs(BASE_DELAY_MS);
	}
	return true;
}

void modemTurnOffPins(){
	////Destroy GSM TX
	//DDRD&=~(1<PIND5);
	//PORTD&=~(1<PIND5);
	////Destroy GSM Rx
	//PORTD&=~(1<PIND6);
	//_delay_us(1);
	//digitalWrite(txGsmPin,LOW);
	UCSR0B&=~((1<<RXEN0)|(1<<TXEN0));
	clearPin(PIN_TX_GSM);
	//pinMode(txGsmPin, INPUT);
	setPinInput(PIN_TX_GSM);
	//digitalWrite(rxGsmPin,LOW);
	clearPin(PIN_RX_GSM);
}

void modemTurnOnPins(){
	////Destroy GSM TX
	//DDRD|=(1<PIND5);
	//PORTD|=(1<PIND5);
	////Destroy GSM Rx
	//PORTD|=(1<PIND6);
	//_delay_us(1);
	//digitalWrite(txGsmPin,HIGH);
	setPin(PIN_TX_GSM);
	//pinMode(txGsmPin, OUTPUT);
	setPinOut(PIN_TX_GSM);
	//digitalWrite(rxGsmPin,HIGH);
	setPin(PIN_RX_GSM);
	UCSR0B|=(1<<RXEN0)|(1<<TXEN0);
}


void modemReadResetByRepl(){
	uint8_t b = eepromReadByte(EEPROM_RESET_BY_REPL_OFFSET);
	if(b==EEPROM_RESET_BY_REPL_VALUE){
		resetByRepl=true;
		eepromWriteByte(EEPROM_RESET_BY_REPL_OFFSET,EEPROM_NOT_RESET_BY_REPL_VALUE);
		setPinOut(PIN_TRANSISTOR);
		modemTurnOnPins();
	}
}

void modemSetResetByRepl(){
	eepromWriteByte(EEPROM_RESET_BY_REPL_OFFSET,EEPROM_RESET_BY_REPL_VALUE);
}
