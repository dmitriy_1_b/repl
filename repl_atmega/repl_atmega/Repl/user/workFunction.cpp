/*
* workFunction.cpp
*
* Created: 12.02.2019 21:43:23
*  Author: root
*/
#include "workFunction.h"

void newF();

void doSomething();
inline void doSwitch(uint8_t var);

uint8_t var = 0;
uint8_t var2 = 69;
uint64_t var64 = 55;
uint64_t varB64 = 33;

TestClass::TestClass(uint8_t b)
{
	a = 15;
	this->b = b + 15;
	++var;
}

TestClass::~TestClass()
{
	a = 5;
	b = 5;
	--var;
}

void TestClass::setB(uint8_t newB)
{
	b = 0x25;
	++var;
}

TestClass myClass(1); // __attribute__ ((section (".work_data")));
TestClass myClass2(2); // __attribute__ ((section (".work_data")));
uint8_t testData = 10; // __attribute__ ((section (".work_data")))=10;

void replWorkFunction(); //__attribute__ ((section (".work_function")));


ISR(TIMER0_OVF_vect){
	replBeforeIrq();
	static uint8_t counter=10;
	static bool flag=false;
	if(counter==0){
		flag=!flag;
		if(flag){
			setPin(PIN_LIMIT_SWITCH);
			}else{
			clearPin(PIN_LIMIT_SWITCH);
		}
		counter=10;
		}else{
		--counter;
	}
	replAfterIrq();
}

void replWorkFunction()
{
	setPinOut(PIN_LIMIT_SWITCH);
	//TIMSK0=1<<TOIE0;
	//TCCR0B=(1<<CS02)|(1<<CS00);
	while(1){
		_delay_ms(100);
		setPin(PIN_LIMIT_SWITCH);
		_delay_ms(100);
		clearPin(PIN_LIMIT_SWITCH);
		_delay_ms(100);
		clearPin(PIN_LIMIT_SWITCH);
	}
	////myClass = *new TestClass(3);
	//testData = 123;
	////myClass.~TestClass();
	////clearPin(PIN_METER_RELAY2);
	//myClass2.setB(0xdd);
	//if (myString == "Test String")
	//{
		//myString = "Test String2";
	//}
	//else
	//{
		//myString = F("&csq=");
	//}
	//TestClass myClass3(4);
	//myClass3.setB(0x55);
}

inline void doSwitch(uint8_t var)
{
	switch (var)
	{
		case 1:doSomething();
		case 2:--var;
		case 3:testData = 68;
		case 5:newF();
	}
}

void notCallFunction()
{
	++var;
	doSwitch(var);
}

void newF()
{
	++var64;
	--varB64;
	--var;
	--var2;
}

void doSomething()
{
	++var64;
	--varB64;
	--var;
	TestClass myClass4(5);
	myClass4.setB(0x55);
}

