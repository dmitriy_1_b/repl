/*This file is provided under license: see license.txt*/
#include "delay.h"
//need to recalculate
void replDelayUs(uint8_t us) {
	volatile uint16_t counter = us;
	while(counter--);
}

void replDelayMs(uint32_t ms) {
	for(uint32_t i=0;i<ms;++i){
		replDelayUs(250);
		replDelayUs(250);
		replDelayUs(250);
		replDelayUs(250);
	}
}
