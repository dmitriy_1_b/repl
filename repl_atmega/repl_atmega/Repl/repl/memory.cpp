/*This file is provided under license: see license.txt*/
/*
* flash.cpp
*
* Created: 28.03.2019 18:23:29
*  Author: root
*/
#include "memory.h"

extern volatile replStructRegisters * regs;

#define LOAD_ADDRESS  16384

void replWriteFlash(uint8_t* buff,uint32_t start,uint32_t end)__attribute__ ((section (".boot")));
void replReWriteFlash(uint8_t* buff,uint32_t start,uint32_t end)__attribute__ ((section (".boot")));

void (*resetAVR)( void ) = 0x0000;
void bootProgramPage(uint32_t page, uint8_t *buf) __attribute__ ((section (".boot")));// BOOTLOADER_SECTION; //__attribute__ ((section (".text.myboot")));//BOOTLOADER_SECTION;//__attribute__ ((section (".text"))) = 0x3e00;


void  bootProgramPage(uint32_t page, uint8_t *buf)
{
	uint16_t i;
	uint8_t sreg;
	// Disable interrupts.
	sreg = SREG;
	cli();
	eeprom_busy_wait ();
	boot_page_erase (page);
	boot_spm_busy_wait ();      // Wait until the memory is erased.
	for (i=0; i<SPM_PAGESIZE; i+=2)
	{
		// Set up little-endian word.
		uint16_t w = *buf++;
		w += (*buf++) << 8;
		
		boot_page_fill (page + i, w);
	}
	boot_page_write (page);     // Store buffer in flash page.
	boot_spm_busy_wait();       // Wait until the memory is written.
	// Reenable RWW-section again. We need this if we want to jump back
	// to the application after bootloading.
	boot_rww_enable ();
	// Re-enable interrupts (if they were ever enabled).
	SREG = sreg;
}

void replWriteFlash(uint8_t* buff,uint32_t start,uint32_t end){
	uint8_t offset = start%128UL;
	if(offset>0){
		for(int32_t i=end-start;i>=0;--i){
			buff[i+offset]=buff[i];
		}
		for(int32_t i=0;i<offset;++i){
			buff[i]=pgm_read_byte(start+i-offset);
		}
		start=start-offset;
	}
	offset =127UL-(end%128UL);
	if(offset>0){
		for(int32_t i=end-start+offset;i>end-start;--i){
			buff[i]=pgm_read_byte(i+start);
		}
		end=end+offset;
	}
	uint8_t oldSREG = SREG;
	cli();
	while(start<=end){
		bootProgramPage(start,buff);
		start+=128UL; buff+=128UL;
	}
	SREG = oldSREG;
}

void replReWriteFlash(uint8_t* buff,uint32_t start,uint32_t end){
	replWriteFlash(buff,start,end);
	uint32_t sectionOffset=LOAD_ADDRESS;
	uint8_t sectionCount = pgm_read_byte(sectionOffset);
	++sectionOffset;
	if(sectionCount==0)
	return;
	for(uint8_t s=0;s<sectionCount;++s){
		for(uint8_t i=0;i<8;++i){
			buff[i]=pgm_read_byte(sectionOffset+i);
		}
		sectionOffset+=8;
		start = replReadUint32(buff);
		end = replReadUint32(buff+4);
		uint8_t startOffset = start%128UL;
		uint8_t j=0;
		if(startOffset>0){
			for(j;j<startOffset;++j){
				buff[j]=pgm_read_byte(start+j-startOffset);
			}
			//start=start-startOffset;
		}
		for(int32_t i=startOffset;i<=startOffset+end-start;++i){
			buff[j]=pgm_read_byte(sectionOffset+i-startOffset);
			++j;
			if(j==128UL){
				bootProgramPage(start+i-startOffset-127UL,buff);
				j=0;
			}
		}
		uint8_t endOffset =127UL-(end%128UL);
		if(endOffset>0){
			for(j;j<128UL;++j){
				buff[j]=pgm_read_byte(end+endOffset+j-127UL);
			}
			bootProgramPage(end+endOffset-127UL,buff);
		}
		sectionOffset+=end-start+1;
	}
	
}

void replWriteRAM(uint8_t* buff,uint32_t start,uint32_t end){
	uint8_t oldSREG = SREG;
	cli();
	while(start<=end){
		*((uint8_t*)start)=*buff;
		start+=1UL; buff+=1UL;
	}
	SREG = oldSREG;
}

void replWriteEEPROM(uint8_t* buff,uint32_t start,uint32_t end){
	uint8_t oldSREG = SREG;
	cli();
	while(start<=end){
		eeprom_write_byte((uint8_t *)start,*buff);
		start+=1UL; buff+=1UL;
	}
	SREG = oldSREG;
}

uint8_t replReadFlash(uint32_t pointer){
	return pgm_read_byte(pointer);
}
uint8_t replReadRAM(uint32_t pointer){
	//if(pointer<32){
		//return *((uint8_t*)regs+1+31-pointer);		
	//}
	//if(pointer==&SREG){
		//return regs->sreg;
	//}
	//
	//if(pointer==&SPL){
		//return ((uint16_t)regs+sizeof(replStructRegisters)-1)&0xff;
	//}
	//
	//if(pointer==&SPH){
		//return (((uint16_t)regs+sizeof(replStructRegisters)-1)>>8)&0xff;
	//}	
	return *((uint8_t*)(pointer));
}
uint8_t replReadEEPROM(uint32_t pointer){
	return eeprom_read_byte((uint8_t *)pointer);
}