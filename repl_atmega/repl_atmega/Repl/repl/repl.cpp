/*This file is provided under license: see license.txt*/
/*
* repl.cpp
*
* Created: 28.03.2019 7:21:38
*  Author: root
*/
#include "repl.h"

#define REPL_STATE_START_BYTE 0//need to detect start of request if something before going wrong
#define REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE 1
#define REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE 2
#define REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE 3
#define REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE 4
#define REPL_STATE_READ_DATA_CRC_FIRST_BYTE 5
#define REPL_STATE_READ_DATA_CRC_SECOND_BYTE 6
#define REPL_STATE_READ_COMMAND 7
#define REPL_STATE_READ_DATA 8
#define REPL_STATE_READ_CRC_FIRST_BYTE 9
#define REPL_STATE_READ_CRC_SECOND_BYTE 10
#define REPL_STATE_PROCESSING 11


#define REPL_COMMAND_NOP 0
#define REPL_COMMAND_RUN 1
#define REPL_COMMAND_WRITE_FLASH 2
#define REPL_COMMAND_WRITE_RAM 3
#define REPL_COMMAND_WRITE_EEPROM 4
#define REPL_COMMAND_READ_FLASH 5
#define REPL_COMMAND_READ_RAM 6
#define REPL_COMMAND_READ_EEPROM 7
#define REPL_COMMAND_PAUSE 8
#define REPL_COMMAND_WRITE_AND_REWRITE_FLASH 9
#define REPL_COMMAND_STEP 10
#define REPL_COMMAND_SET_BREAKPOINT 11
#define REPL_COMMAND_DELETE_ALL_BREAKPOINTS 12
#define REPL_COMMAND_DELETE_BREAKPOINTS 13
#define REPL_COMMAND_READ_NEAR_PC 14//call if somebody try to read flash where address=pc
#define REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT 15//additional run command that didn't work if it's stopped by breakpoint, 
//this command very useful if program run by sending run command repeatedly from computer
#define REPL_LAST_COMMAND 15

#define REPL_START_BYTE 0xb7//just random number
#define REPL_CRC_LENGTH 2
#define REPL_CRC_START_VALUE 0xffff
#define REPL_RESPONSE_BASE_PART_SIZE 45
#define REPL_DATA_LENGTH 2//bytes for save length of data

static uint8_t state=REPL_STATE_START_BYTE;

static uint32_t dataLength;
static uint32_t dataAwating;
static uint8_t command=0;
volatile uint8_t replFlagsRegister;
extern volatile replStructRegisters * regs;
extern volatile uint32_t replInterrupts;
volatile static uint16_t crc;
volatile static uint16_t crcResponse;
static uint32_t start=1;//must bigger than end,because if start equals end we send byte
static uint32_t end=0;
static uint8_t responseSendP=0;
static bool replProcessed=false;


uint8_t flashBuff[REPL_BUFFER_SIZE];

static void crc16(uint8_t addValue, volatile uint16_t &crc) {
	////CRC-16/CCITT-FALSE
	//    uint8_t x;
	//    x = crc >> 8 ^ addValue;
	//    x ^= x >> 4;
	//    crc = (crc << 8) ^ ((uint16_t) (x << 12)) ^ ((uint16_t) (x << 5)) ^ ((uint16_t) x);
	//    return;
	crc ^= addValue; // XOR byte into least sig. byte of crc
	for (uint8_t i = 8; i != 0; i--) { // Loop over each bit
		if ((crc & 0x0001) != 0) { // If the LSB is set
			crc >>= 1; // Shift right and XOR 0xA001
			crc ^= 0xA001;
		} else // Else LSB is not set
		crc >>= 1; // Just shift right
	}
}

uint8_t replGetFlagRegister(){
	return replFlagsRegister;
}

void replSetFlagRegister(uint8_t value){
	replFlagsRegister=value;
}

bool replGetFlag(uint8_t number){
	if(replFlagsRegister&(1<<number)){
		return true;
		}else{
		return false;
	}
}

void replSetFlag(uint8_t number,bool value){
	if(value){
		replFlagsRegister|=1<<number;
		}else{
		replFlagsRegister&=~(1<<number);
	}
	
}

uint32_t replBytesAwating(){
	switch(state){
		case REPL_STATE_START_BYTE: return 6+REPL_CRC_LENGTH+REPL_CRC_LENGTH;
		case REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE: return 5+REPL_CRC_LENGTH+REPL_CRC_LENGTH;//4 bytes data, 1 byte command
		case REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE: return 4+REPL_CRC_LENGTH+REPL_CRC_LENGTH;
		case REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE: return 3+REPL_CRC_LENGTH+REPL_CRC_LENGTH;
		case REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE: return 2+REPL_CRC_LENGTH+REPL_CRC_LENGTH;
		case REPL_STATE_READ_DATA_CRC_FIRST_BYTE:return 2+REPL_CRC_LENGTH+1;//don't add dataLength before checking that it's valid
		case REPL_STATE_READ_DATA_CRC_SECOND_BYTE:return 2+REPL_CRC_LENGTH;
		case REPL_STATE_READ_COMMAND: return dataLength+1+REPL_CRC_LENGTH;
		case REPL_STATE_READ_DATA:return dataAwating+REPL_CRC_LENGTH;
		case REPL_STATE_READ_CRC_FIRST_BYTE:return 2;
		case REPL_STATE_READ_CRC_SECOND_BYTE:return 1;
		case REPL_STATE_PROCESSING:return 0;
	}
}

/*
return false if wrong data, wrong protocol
*/
bool replSendByte(uint8_t b){
	switch(state){
		case REPL_STATE_START_BYTE:if(b!=REPL_START_BYTE) {replReset();return false;}responseSendP=0; state=REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE;crc=REPL_CRC_START_VALUE;crc16(b,crc);return true;
		case REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE: dataLength=((uint32_t)b)<<24;state=REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE;crc16(b,crc);return true;
		case REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE: dataLength+=((uint32_t)b)<<16;state=REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE;crc16(b,crc);return true;
		case REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE: dataLength+=((uint32_t)b)<<8;state=REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE;crc16(b,crc);return true;
		case REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE: dataLength+=(uint32_t)b;dataAwating=dataLength;state=REPL_STATE_READ_DATA_CRC_FIRST_BYTE;crc16(b,crc);return true;
		case REPL_STATE_READ_DATA_CRC_FIRST_BYTE:if(b!=(crc>>8)){replReset();return false;} state=REPL_STATE_READ_DATA_CRC_SECOND_BYTE;return true;
		case REPL_STATE_READ_DATA_CRC_SECOND_BYTE:if(b!=(crc&0xff)){replReset();return false;} state=REPL_STATE_READ_COMMAND;return true;
		case REPL_STATE_READ_COMMAND:
		command=b;replProcessed=false;
		crc16(b,crc);
		if(command<=REPL_LAST_COMMAND)
		{
			if(dataAwating>0) {
				state=REPL_STATE_READ_DATA;
				return true;
			}
			else
			{
				state=REPL_STATE_READ_CRC_FIRST_BYTE;
				return true;
			}
		}
		else return false;
		case REPL_STATE_READ_DATA:
		crc16(b,crc);
		flashBuff[dataLength-dataAwating]=b;
		--dataAwating;
		if(dataAwating>0){
			return true;
			}else {
			state=REPL_STATE_READ_CRC_FIRST_BYTE;
			return true;
		}
		case REPL_STATE_READ_CRC_FIRST_BYTE:if(b!=(crc>>8)){replReset();return false;} state=REPL_STATE_READ_CRC_SECOND_BYTE;return true;
		case REPL_STATE_READ_CRC_SECOND_BYTE:if(b!=(crc&0xff)){replReset();return false;} state=REPL_STATE_PROCESSING;return true;
	}
	return false;
}

uint8_t _replReadByte(){
	if(responseSendP<REPL_RESPONSE_BASE_PART_SIZE){
		++responseSendP;
		if(responseSendP==1){
			crcResponse=REPL_CRC_START_VALUE;
			return ((replBytesAvalible()-REPL_CRC_LENGTH)>>8)&0xff;
		}
		if(responseSendP==2){
			return (replBytesAvalible()-REPL_CRC_LENGTH)&0xff;
		}
		if(responseSendP==3)
		return command;
		if(responseSendP==4)
		return replFlagsRegister;
		if(responseSendP==5||responseSendP==6){
			uint16_t stackPointer = (uint16_t)regs+sizeof(replStructRegisters)-1;
			if(responseSendP==5){
				return stackPointer&0xff;
				}else{
				return (stackPointer>>8)&0xff;
			}
		}
		if(responseSendP<11){
			uint8_t* p = (uint8_t*)&replInterrupts;
			return *(p+responseSendP-7);
		}
		uint8_t ret =*((uint8_t*)regs+responseSendP-11);
		return ret;
	}
	switch(command){
		case REPL_COMMAND_READ_RAM:if(start<=end){++start;return replReadRAM(start-1);}break;
		case REPL_COMMAND_READ_EEPROM:if(start<=end){++start;return replReadEEPROM(start-1);}break;
		default:if(start<=end){++start;return replReadMemWithBreakPointCheck(start-1);}break;//case REPL_COMMAND_READ_FLASH,REPL_COMMAND_STEP,REPL_COMMAND_PAUSE
	}
	return 0;
}

uint8_t replReadByte(){
	if((responseSendP<REPL_RESPONSE_BASE_PART_SIZE)||(end>=start)){
		uint8_t val = _replReadByte();
		crc16(val,crcResponse);
		return val;
	}else{
		if(responseSendP==REPL_RESPONSE_BASE_PART_SIZE){
			++responseSendP;
			return (crcResponse>>8)&0xff;
		}
		if(responseSendP==(REPL_RESPONSE_BASE_PART_SIZE+1)){				
			state=REPL_STATE_START_BYTE;
			++responseSendP;
			return crcResponse&0xff;
		}
		return 0;
	}
}

uint32_t replBytesAvalible(){
	return end+1-start+REPL_RESPONSE_BASE_PART_SIZE+REPL_CRC_LENGTH;
}

/*
Reset repl if you send wrong data
*/
void replReset(){
	state=REPL_STATE_START_BYTE;
	start=1;//must be bigger than end
	end=0;
	responseSendP=0;
}

uint32_t replReadUint32(uint8_t * buff){
	uint32_t out;
	out=((uint32_t)(*buff))<<24;
	out+=((uint32_t)(*(buff+1)))<<16;
	out+=((uint32_t)(*(buff+2)))<<8;
	out+=((uint32_t)(*(buff+3)));
	return out;
}

void replReadFlashNearProgramCounter(){
	start=(regs->pcl+((uint32_t)regs->pch<<8))*2;
	if(start>128){
		start-=128;
		end=start+255;
		}else{
		start=0;
		end=255;
	}
}

extern volatile uint32_t timerCallCounter;

bool replProcessing(){
	if(replProcessed&&(command==REPL_COMMAND_STEP))//temporary solution 
		return false;
	replProcessed=true;
	replTimerStop();
	#ifdef REPL_USE_GSM
		bool runFlag=false;
	#endif
	switch(command){
		case REPL_COMMAND_NOP: replSetFlag(REPL_IS_BREAKPOINT_SET,false); break;
		case REPL_COMMAND_WRITE_FLASH: replWriteFlash(flashBuff+8,replReadUint32(flashBuff),replReadUint32(flashBuff+4)); break;
		case REPL_COMMAND_WRITE_RAM: replWriteRAM(flashBuff+8,replReadUint32(flashBuff),replReadUint32(flashBuff+4)); break;
		case REPL_COMMAND_WRITE_EEPROM: replWriteEEPROM(flashBuff+8,replReadUint32(flashBuff),replReadUint32(flashBuff+4)); break;
		case REPL_COMMAND_WRITE_AND_REWRITE_FLASH:replReWriteFlash(flashBuff+8,replReadUint32(flashBuff),replReadUint32(flashBuff+4));
		modemSetResetByRepl(); cli();wdt_enable(WDTO_15MS);	/*reset mcu*/ break;
		case REPL_COMMAND_READ_FLASH: start=replReadUint32(flashBuff);end=replReadUint32(flashBuff+4);break;
		case REPL_COMMAND_READ_RAM: start=replReadUint32(flashBuff);end=replReadUint32(flashBuff+4);break;
		case REPL_COMMAND_READ_EEPROM: start=replReadUint32(flashBuff);end=replReadUint32(flashBuff+4);break;
		case REPL_COMMAND_PAUSE:replTimerPause();replReadFlashNearProgramCounter(); break;
		case REPL_COMMAND_RUN:replTimerSetRun();/*runFlag=true;*/break;
		case REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT:if(!replGetFlag(REPL_IS_BREAK))replTimerSetRun();
		#ifdef REPL_USE_GSM 
		runFlag=true; 
		#endif 
		break;
		case REPL_COMMAND_STEP:replTimerSetStep();/*replReadFlashNearProgramCounter();-call inside timer function*/ /*replTimerResumeFromPause();*/break;
		case REPL_COMMAND_SET_BREAKPOINT: replSetBreakPoint(replReadUint32(flashBuff)); break;
		case REPL_COMMAND_DELETE_ALL_BREAKPOINTS: replDeleteAllBreakPoints();break;
		case REPL_COMMAND_DELETE_BREAKPOINTS: replDeleteBreakPoints(flashBuff); break;
		case REPL_COMMAND_READ_NEAR_PC:	replReadFlashNearProgramCounter(); break;
	}
	replTimerResume();
	#ifdef REPL_USE_GSM
	if(runFlag){
	replDelayMs(1000);
	}
	#endif
	if(command!=REPL_COMMAND_READ_FLASH&&
	command!=REPL_COMMAND_READ_RAM&&
	command!=REPL_COMMAND_READ_EEPROM&&
	command!=REPL_COMMAND_STEP&&
	command!=REPL_COMMAND_READ_NEAR_PC&&
	command!=REPL_COMMAND_PAUSE)
	state=REPL_STATE_START_BYTE;
	return true;
}

extern uint16_t __datz_start;
extern uint16_t __datz_end;
extern uint16_t __datz_load_start;
extern uint16_t __datz_load_end;

extern uint16_t __bsz_start;
extern uint16_t __bsz_end;

extern uint16_t __ctorz_start;
extern uint16_t __ctorz_end;

typedef void ctorz(void);

void replInitProgram(){
	uint8_t oldSREG = SREG;
	cli();
	volatile uint16_t i=(uint16_t)&__datz_start;
	volatile uint16_t j=(uint16_t)&__datz_load_start;
	while(i<(uint16_t)&__datz_end){
		*((uint8_t*)i)=pgm_read_byte(j);
		i+=1UL; j+=1UL;
	}
	
	i=(uint16_t)&__bsz_start;
	while(i<(uint16_t)&__bsz_end){
		*((uint8_t*)i)=0x00;
		i+=1UL;
	}
	i=(uint16_t)&__ctorz_start;
	while(i<(uint16_t)&__ctorz_end){
		ctorz* f = (ctorz*)pgm_read_word(i);
		f();
		i+=2UL;
	}
	SREG = oldSREG;
}

static bool replIsInterruptAlreadyCall=false;

bool replIsInterrupt(){
	if(!replGetFlag(REPL_IS_BREAK)){
		replIsInterruptAlreadyCall=false;
		}else if(!replIsInterruptAlreadyCall){
		replIsInterruptAlreadyCall=true;
		return true;
	}
	return false;
}