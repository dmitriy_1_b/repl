/*This file is provided under license: see license.txt*/
/*
 * breakPoints.h
 *
 * Created: 03-Sep-19 04:13:44 PM
 *  Author: root
 */ 


#ifndef BREAKPOINTS_H_
#define BREAKPOINTS_H_

#include "../config.h"

void replSetBreakPoint(uint32_t address);
void replDeleteBreakPoints(uint8_t buff[]);
void replDeleteAllBreakPoints(void);
void replBreakPoint(void);
bool replCheckBreakPoint(void);
void replBreakPointExecute();
uint8_t replReadMemWithBreakPointCheck(uint32_t pointer);

#endif /* BREAKPOINTS_H_ */