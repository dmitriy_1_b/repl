/*This file is provided under license: see license.txt*/
/*
 * repl.h
 *
 * Created: 28.03.2019 7:22:07
 *  Author: root
 */ 


#ifndef REPL_H_
#define REPL_H_

#define REPL_IS_BREAK 0//return from break point
#define REPL_IS_STEP 1//return after step;
#define REPL_STEP 2
#define REPL_IS_STEP_CALL_AFTER_BREAK 3
#define REPL_BREAK_POINT_FIRST_INSTRUCTION 4
#define REPL_IS_PAUSE 6
#define REPL_IS_BREAKPOINT_SET 7

#define REPL_DEFAULT_PROCESSOR_CYCLES 1000

#include "../config.h"


uint32_t replBytesAwating();
/*
return false if wrong data
*/
bool replSendByte(uint8_t b);

uint8_t replReadByte();

uint32_t replBytesAvalible();
/*
Reset repl if you send wrong data
*/
void replReset();

bool replProcessing();

void replInitProgram();

uint32_t replReadUint32(uint8_t * buff);

uint8_t replGetFlagRegister();

void replSetFlagRegister(uint8_t value);

bool replGetFlag(uint8_t number);

void replSetFlag(uint8_t number,bool value);

bool replIsInterrupt();

void replReadFlashNearProgramCounter();


#endif /* REPL_H_ */