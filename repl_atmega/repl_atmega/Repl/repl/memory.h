/*This file is provided under license: see license.txt*/
/*
* flash.h
*
* Created: 28.03.2019 18:23:47
*  Author: root
*/


#ifndef REPL_MEMORY_H
#define REPL_MEMORY_H

#include "../config.h"
#include <avr/boot.h>
#include <inttypes.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
/*
must be from page to page
*/
void replWriteFlash(uint8_t* buff,uint32_t start,uint32_t end);
void replWriteRAM(uint8_t* buff,uint32_t start,uint32_t end);
void replWriteEEPROM(uint8_t* buff,uint32_t start,uint32_t end);

uint8_t replReadFlash(uint32_t pointer);
uint8_t replReadRAM(uint32_t pointer);
uint8_t replReadEEPROM(uint32_t pointer);

void replReWriteFlash(uint8_t* buff,uint32_t start,uint32_t end);



#endif /* REPL_MEMORY_H */