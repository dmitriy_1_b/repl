/*This file is provided under license: see license.txt*/
/*
* timer.cpp
*
* Created: 29-Aug-19 10:25:01 PM
*  Author: root
*/
#include "timer.h"

volatile uint32_t replInterrupts;
volatile uint16_t mainThreadTime=REPL_DEFAULT_PROCESSOR_CYCLES;
volatile uint16_t pc;
extern volatile uint16_t breakPointReturnPC;
extern volatile uint16_t breakPointFirstInstructionPC;
volatile replStructRegisters * regs;
volatile bool runningFlag=false;

static bool replTimerStopFlag = false;
static bool replTimerPauseFlag = false;

static void replTim1Disable(){
	uint8_t oldSREG = SREG;
	cli();
	TCCR1B=0;
	SREG = oldSREG;
}

static void replTim1Enable(){
	uint8_t oldSREG = SREG;
	cli();
	if(OCR1A!=65535){
		TCCR1B=1<<CS10;
		}else{
		TCCR1B=1<<CS11;
	}
	SREG = oldSREG;
}


void replTimerSetStep(void){
	replTimerPauseFlag = false;
	if(replGetFlag(REPL_IS_BREAK)){
		replSetFlag(REPL_IS_STEP_CALL_AFTER_BREAK,true);
		}else{
		replSetFlag(REPL_STEP,true);
		mainThreadTime=1;//always execute one instruction after "reti" independent from interrupts flag  (maybe any number between 1 and 75)
	}
	if (replGetFlag(REPL_IS_BREAK)) {
		replBreakPointExecute();	//can call replTimerPause
	}
	if (!replTimerPauseFlag) {
		replSetFlag(REPL_IS_PAUSE, false);
		OCR1A=65535;//need to complete usb transaction
	}
	if ((!replTimerStopFlag) && (!replTimerPauseFlag)) {
		replTim1Enable();
	}
}



void replTimerSetRun(void){
	mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
	replTimerPauseFlag = false;
	if (replGetFlag(REPL_IS_BREAK)) {
		replBreakPointExecute();	//can call replTimerPause
	}
	if (!replTimerPauseFlag) {
		replSetFlag(REPL_IS_PAUSE, false);
		runningFlag=true;
		OCR1A=65535;//need to complete usb transaction
	}
	if ((!replTimerStopFlag) && (!replTimerPauseFlag)) {
		replTim1Enable();
	}
}


volatile bool isAlreadySave = false;

void replSaveAndDisableIRQS(volatile uint32_t *interrupts){
	*interrupts=0x00;
	if(bit_is_set(ACSR,ACIE)){
		*interrupts|=1UL<<0;
		ACSR&=~(1<<ACIE);
	}
	if(bit_is_set(ADCSRA,ADIE)){
		*interrupts|=1UL<<1;
		ADCSRA&=~(1<<ADIE);
	}
	if(bit_is_set(SPMCSR,SPMIE)){
		*interrupts|=1UL<<2;
		SPMCSR&=~(1<<SPMIE);
	}
	if(bit_is_set(EECR,EERIE)){
		*interrupts|=1UL<<3;
		EECR&=~(1<<EERIE);
	}
	if(bit_is_set(PCICR,PCIE0)){
		*interrupts|=1UL<<4;
		PCICR&=~(1<<PCIE0);
	}
	if(bit_is_set(PCICR,PCIE1)){
		*interrupts|=1UL<<5;
		PCICR&=~(1<<PCIE1);
	}
	if(bit_is_set(PCICR,PCIE2)){
		*interrupts|=1UL<<6;
		PCICR&=~(1<<PCIE2);
	}
	#ifndef REPL_USE_USB
	if(bit_is_set(EIMSK,INT0)){
		*interrupts|=1UL<<7;
		EIMSK&=~(1<<INT0);
	}
	#endif
	if(bit_is_set(EIMSK,INT1)){
		*interrupts|=1UL<<8;
		EIMSK&=~(1<<INT1);
	}
	if(bit_is_set(SPCR,SPIE)){
		*interrupts|=1UL<<9;
		SPCR&=~(1<<SPIE);
	}
	//if(bit_is_set(TIMSK1,ICIE1)){
	//interrupts|=1UL<<10;
	//TIMSK1&=~(1<<ICIE1);
	//}
	//if(bit_is_set(TIMSK1,OCIE1B)){
	//interrupts|=1UL<<11;
	//TIMSK1&=~(1<<OCIE1B);
	//}
	//if(bit_is_set(TIMSK1,OCIE1A)){
	//interrupts|=1UL<<12;
	//TIMSK1&=~(1<<OCIE1A);
	//}
	//if(bit_is_set(TIMSK1,TOIE1)){
	//interrupts|=1UL<<13;
	//TIMSK1&=~(1<<TOIE1);
	//}
	if(bit_is_set(TIMSK0,OCIE0B)){
		*interrupts|=1UL<<14;
		TIMSK0&=~(1<<OCIE0B);
	}
	if(bit_is_set(TIMSK0,OCIE0A)){
		*interrupts|=1UL<<15;
		TIMSK0&=~(1<<OCIE0A);
	}
	if(bit_is_set(TIMSK0,TOIE0)){
		*interrupts|=1UL<<16;
		TIMSK0&=~(1<<TOIE0);
	}
	if(bit_is_set(TIMSK2,OCIE2B)){
		*interrupts|=1UL<<17;
		TIMSK2&=~(1<<OCIE2B);
	}
	if(bit_is_set(TIMSK2,OCIE2A)){
		*interrupts|=1UL<<18;
		TIMSK2&=~(1<<OCIE2A);
	}
	if(bit_is_set(TIMSK2,TOIE2)){
		*interrupts|=1UL<<19;
		TIMSK2&=~(1<<TOIE2);
	}
	if(bit_is_set(TWCR,TWIE)){
		*interrupts|=1UL<<20;
		TWCR&=~(1<<TWIE);
	}
	#ifndef REPL_USE_GSM
	if(bit_is_set(UCSR0B,RXCIE0)){
		*interrupts|=1UL<<21;
		UCSR0B&=~(1<<RXCIE0);
	}
	if(bit_is_set(UCSR0B,TXCIE0)){
		*interrupts|=1UL<<22;
		UCSR0B&=~(1<<TXCIE0);
	}
	if(bit_is_set(UCSR0B,UDRIE0)){
		*interrupts|=1UL<<23;
		UCSR0B&=~(1<<UDRIE0);
	}
	#endif
}

void replLoadAndEnableIRQS(volatile uint32_t *interrupts){
	
	if(*interrupts&(1UL<<0)){
		ACSR|=1<<ACIE;
	}
	if(*interrupts&(1UL<<1)){
		ADCSRA|=1<<ADIE;
	}
	if(*interrupts&(1UL<<2)){
		SPMCSR|=1<<SPMIE;
	}
	if(*interrupts&(1UL<<3)){
		EECR|=1<<EERIE;
	}
	if(*interrupts&(1UL<<4)){
		PCICR|=1<<PCIE0;
	}
	if(*interrupts&(1UL<<5)){
		PCICR|=1<<PCIE1;
	}
	if(*interrupts&(1UL<<6)){
		PCICR|=1<<PCIE2;
	}
	#ifndef REPL_USE_USB
	if(*interrupts&(1UL<<7)){
		EIMSK|=1<<INT0;
	}
	#endif
	if(*interrupts&(1UL<<8)){
		EIMSK|=1<<INT1;
	}
	if(*interrupts&(1UL<<9)){
		SPCR|=1<<SPIE;
	}
	//if(interrupts&(1UL<<10)){
	//TIMSK1|=1<<ICIE1;
	//}
	//if(interrupts&(1UL<<11)){
	//TIMSK1|=1<<OCIE1B;
	//}
	//if(interrupts&(1UL<<12)){
	//TIMSK1|=1<<OCIE1A;
	//}
	//if(interrupts&(1UL<<13)){
	//TIMSK1|=1<<TOIE1;
	//}
	if(*interrupts&(1UL<<14)){
		TIMSK0|=1<<OCIE0B;
	}
	if(*interrupts&(1UL<<15)){
		TIMSK0|=1<<OCIE0A;
	}
	if(*interrupts&(1UL<<16)){
		TIMSK0|=1<<TOIE0;
	}
	if(*interrupts&(1UL<<17)){
		TIMSK2|=1<<OCIE2B;
	}
	if(*interrupts&(1UL<<18)){
		TIMSK2|=1<<OCIE2A;
	}
	if(*interrupts&(1UL<<19)){
		TIMSK2|=1<<TOIE2;
	}
	if(*interrupts&(1UL<<20)){
		TWCR|=1<<TWIE;
	}
	#ifndef REPL_USE_GSM
	if(*interrupts&(1UL<<21)){
		UCSR0B|=1<<RXCIE0;
	}
	if(*interrupts&(1UL<<22)){
		UCSR0B|=1<<TXCIE0;
	}
	if(*interrupts&(1UL<<23)){
		UCSR0B|=1<<UDRIE0;
	}
	#endif
}

void saveInterruptsEnableBits(void){
	if (!isAlreadySave) {
		replSaveAndDisableIRQS(&replInterrupts);
		isAlreadySave = true;
	}
}

void loadInterruptsEnableBits(void){
	isAlreadySave=false;
	replLoadAndEnableIRQS(&replInterrupts);
}

volatile uint32_t replUserInterrups;

void replBeforeIrq(void){
	replSaveAndDisableIRQS(&replUserInterrups);
	sei();
}

void replAfterIrq(void){
	replLoadAndEnableIRQS(&replUserInterrups);
}

//volatile uint16_t a;

void repl(void){
	//while(1){
	//for(uint16_t i=0;i<10000;++i){
	//a=i;
	//}
	//}
	#ifdef REPL_USE_GSM
	modemConfig();
	modemEstablishConnection();
	#endif
	#ifdef REPL_USE_USB
	mainUsb();
	#endif
}


volatile bool first=true;
volatile bool replCallFromISR=false;
volatile uint8_t debugStack[300];//start size from something
volatile uint16_t replBStack,replDStack;

//#define SP  _SFR_IO16(0x3D)

void replTimerInit(void){
	replDStack=(uint16_t)&debugStack+299;
	uint32_t replFlagReg=0;
	//replSetFlag(REPL_ISBREAK,false);
	//TCCR1B=0;
	//TCNT1=0;
	OCR1A=REPL_DEFAULT_PROCESSOR_CYCLES;
	TIMSK1=1<<OCIE1A;
	//TCCR1B=1<<CS10;//no prescales
	replTim1Enable();
}

void replTimerStop(void){
	replTim1Disable();
	replTimerStopFlag = true;
}

void replTimerResume(void){
	replTimerStopFlag = false;
	if (!replTimerPauseFlag){
		replTim1Enable();
	}
}

void replTimerPause(void){
	replTim1Disable();
	replSetFlag(REPL_IS_PAUSE,true);
	replTimerPauseFlag = true;
}

extern uint16_t __replBreakPointStart__;
extern uint16_t __replBreakPointEnd__;


//extern uint16_t instructionTest;


/*call from asm linked by ld script*/
void replTimerISR(void)
{
	//setPinInput(PIN_MODEM_RTS);
	//setPin(PIN_MODEM_RTS);
	replTim1Disable();
	TCNT1=0;
	TIFR1|=1<<OCF1A;
	#ifdef REPL_USE_USB
	EIMSK|=1<<INT0;//only to usb
	replTimerPause();
	#endif
	
	if(replCallFromISR){
		replCallFromISR=false;
		//cli();
		//replDStack=SP;
		//SP=replBStack;
		loadInterruptsEnableBits();
		//restartTimerToCallRepl;
		OCR1A=mainThreadTime;//75 and below - 1 processor clock  76 - 2 clocks .....
		#ifdef REPL_USE_USB
		EIMSK&=~(1<<INT0);
		if(runningFlag && (!replGetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION))){
			runningFlag=false;
			OCR1A=32767;
			TCCR1B=1<<CS12;
			}else{
			replTim1Enable();
		}
		#else
		replTim1Enable();
		#endif
		}else{
		replCallFromISR=true;
		saveInterruptsEnableBits();
		//replBStack=SP;
		//SP=replDStack;
		regs=(volatile replStructRegisters*)(replBStack+1);
		pc=regs->pcl+((uint16_t)regs->pch<<8);
		
		if(replReadFlash(pc*2)==0xff && replReadFlash(pc*2+1)==0xcf && replCheckBreakPoint()){
			#ifdef REPL_USE_USB
			EIMSK|=1<<INT0;//only to usb
			#endif
			replSetFlag(REPL_IS_BREAK,true);
			replTimerPause();
			//if breakpoint after step command, that regulated by code after that code
			}else{
			replSetFlag(REPL_IS_BREAK,false);
		}
		if(replGetFlag(REPL_STEP)){
			replSetFlag(REPL_IS_STEP,true);
			mainThreadTime=REPL_DEFAULT_PROCESSOR_CYCLES;
			replSetFlag(REPL_STEP,false);
			replTimerPause();
			}else{
			replSetFlag(REPL_IS_STEP,false);
		}
		OCR1A=REPL_DEFAULT_PROCESSOR_CYCLES;//0xffff;
		if(replGetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION)){
			if(pc!=breakPointFirstInstructionPC){// instruction executed no matter how much cycles need for this
				replSetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION,false);
				mainThreadTime=REPL_DEFAULT_PROCESSOR_CYCLES;
				pc=breakPointReturnPC;
				regs->pcl=pc&0xff;
				regs->pch=(pc>>8)&0xff;
				OCR1A=1;//reduce debug flow time
				//check if next instruction is  breakpoint, set REPL_IS_BREAK flag
				if(replReadFlash(pc*2)==0xff && replReadFlash(pc*2+1)==0xcf && replCheckBreakPoint()){
					replSetFlag(REPL_IS_BREAK, true);
					#ifdef REPL_USE_USB
					EIMSK|=1<<INT0;//only to usb
					#endif
					replTimerPause();
				}
				if(replGetFlag(REPL_IS_STEP_CALL_AFTER_BREAK)){
					replSetFlag(REPL_IS_STEP,true);
					replSetFlag(REPL_IS_STEP_CALL_AFTER_BREAK,false);
					mainThreadTime=REPL_DEFAULT_PROCESSOR_CYCLES;
					replTimerPause();
				}
				}else{
				OCR1A=1;//reduce debug flow time
			}
		}
		if(replGetFlag(REPL_IS_STEP)){
			replReadFlashNearProgramCounter();
		}
		//restartTimerToPauseRepl;
		if(first){
			first=false;
			//clearPin(PIN_MODEM_RTS);
			//setPinOut(PIN_MODEM_RTS);
			//replTim1Enable();//not pause from start
			replTimerPause();//pause from start
			sei();
			repl();//never return
		}
		if ((!replTimerStopFlag) && (!replTimerPauseFlag)) {
			replTim1Enable();
		}
		//uint16_t t=(uint16_t)&instructionTest;
		//t=t>>1;
		//t+=6;
		//regs->pcl=t&0xff;
		//regs->pch=(t>>8)&0xff;
		//replEmulate(regs->pcl+(((uint16_t)regs->pch)<<8));
	}
	//clearPin(PIN_MODEM_RTS);
	//setPinOut(PIN_MODEM_RTS);
	return;
}