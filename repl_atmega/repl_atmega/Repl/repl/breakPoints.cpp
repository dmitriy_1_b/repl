/*This file is provided under license: see license.txt*/
#include "breakPoints.h"
#define REPL_BREAKPOINT_NULL_INDEX 0xff

extern uint8_t flashBuff[REPL_BUFFER_SIZE];

volatile uint16_t breakPointReturnPC;
volatile uint16_t breakPointFirstInstructionPC;
extern volatile uint16_t pc;
extern volatile replStructRegisters * regs;
extern volatile uint16_t mainThreadTime;
extern uint16_t replInstructionTable;

void replSetBreakPoint(uint32_t address){
	volatile uint16_t table = (uint16_t) &replInstructionTable;
	if (address == 0) {
		return;
	}
	address=address&0xFFFE;//you cannot add breakpoint to odd address, sometimes gdb tries to do this
	uint8_t freeCell = REPL_BREAK_POINTS_COUNT;
	uint16_t a;
	for (int16_t i = REPL_BREAK_POINTS_COUNT-1; i >=0 ; --i) {
		uint8_t *pointer = (uint8_t*) &a;
		*pointer = replReadFlash(table + i * 6);
		*(pointer + 1) = replReadFlash(table + i * 6 + 1);
		if (a == 0x00) {
			freeCell = i;
			} else if (a == address) {
			replSetFlag(REPL_IS_BREAKPOINT_SET, true);
			return;
		}
	}
	if (freeCell == REPL_BREAK_POINTS_COUNT) {
		return;
	}
	uint8_t *pointer = (uint8_t*) &address;
	flashBuff[0] = *pointer;
	flashBuff[1] = *(pointer + 1);
	flashBuff[2] = replReadFlash(address);
	flashBuff[3] = replReadFlash(address + 1);
	if (replInstructionIs32bit(*((uint16_t*)&flashBuff[2]))) {
		flashBuff[4] = replReadFlash(address + 2);
		flashBuff[5] = replReadFlash(address + 3);
		replWriteFlash((uint8_t*) flashBuff, table + freeCell * 6,
		table + freeCell * 6 + 5);
		}else{
		replWriteFlash((uint8_t*) flashBuff, table + freeCell * 6,
		table + freeCell * 6 + 3);
	}

	//0xffcf rjmp PC+0
	flashBuff[0] = 0xff;
	flashBuff[1] = 0xcf;
	replWriteFlash((uint8_t*) flashBuff, address, address + 1);
	replSetFlag(REPL_IS_BREAKPOINT_SET, true);

}

uint8_t replBreakPointAddressToIndex(uint32_t address){
	address=address&0xFFFE;
	volatile uint16_t table = (uint16_t) &replInstructionTable;
	if (address == 0) {
		return REPL_BREAKPOINT_NULL_INDEX;
	}
	uint16_t a;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
		uint8_t *pointer = (uint8_t*) &a;
		*pointer = replReadFlash(table + i * 6);
		*(pointer + 1) = replReadFlash(table + i * 6 + 1);
		if (a == address) {
			return i;
		}
	}
	return REPL_BREAKPOINT_NULL_INDEX;
}


void replDeleteBreakPointByIndex(uint8_t i){
	if(i==REPL_BREAKPOINT_NULL_INDEX)
		return;
	volatile uint16_t table = (uint16_t) &replInstructionTable;
	uint16_t address;
	uint8_t *pointer = (uint8_t*) &address;
	*pointer = replReadFlash(table + i * 6);
	*(pointer + 1) = replReadFlash(table + i * 6 + 1);
	flashBuff[0] = replReadFlash(table + i * 6 + 2);
	flashBuff[1] = replReadFlash(table + i * 6 + 3);
	if (replInstructionIs32bit(*((uint16_t*)&flashBuff[0]))) {
		flashBuff[2] = replReadFlash(table + i * 6 + 4);
		flashBuff[3] = replReadFlash(table + i * 6 + 5);
		replWriteFlash((uint8_t*) flashBuff, address, address + 3);
		} else {
		replWriteFlash((uint8_t*) flashBuff, address, address + 1);
	}
	flashBuff[0] = 0x00;
	flashBuff[1] = 0x00;
	replWriteFlash((uint8_t*) flashBuff, table + i * 6,
	table + i * 6 + 1);
	return;
}

void replDeleteBreakPoints(uint8_t* buff){
	uint8_t breakPoints[20];
	uint8_t count = buff[0];
	++buff;
	for(uint8_t i=0;i<count;++i){
		breakPoints[i]=replBreakPointAddressToIndex(replReadUint32(buff+i*4));
	}
	for(uint8_t i=0;i<count;++i){
		replDeleteBreakPointByIndex(breakPoints[i]);
	}
}

void replDeleteAllBreakPoints(void){
	volatile uint16_t table = (uint16_t) &replInstructionTable;
	volatile uint16_t address;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadFlash(table + i * 6);
		*(pointer + 1) = replReadFlash(table + i * 6 + 1);
		if (address==(pc*2)) {
			replSetFlag(REPL_IS_BREAK,false);
		}
		if (address != 0x00) {
			flashBuff[0] = replReadFlash(table + i * 6 + 2);
			flashBuff[1] = replReadFlash(table + i * 6 + 3);
			if (replInstructionIs32bit(*((uint16_t*)&flashBuff[0]))) {
				flashBuff[2] = replReadFlash(table + i * 6 + 4);
				flashBuff[3] = replReadFlash(table + i * 6 + 5);
				replWriteFlash((uint8_t*) flashBuff, address, address + 3);
				} else {
				replWriteFlash((uint8_t*) flashBuff, address, address + 1);
			}
			flashBuff[0] = 0x00;
			flashBuff[1] = 0x00;
			replWriteFlash((uint8_t*) flashBuff, table + i * 6,
			table + i * 6 + 1);
		}
	}
}

bool replCheckBreakPoint(void){
	volatile uint16_t table = (uint16_t) &replInstructionTable;
	volatile uint16_t address;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadFlash(table + i * 6);
		*(pointer + 1) = replReadFlash(table + i * 6 + 1);
		if ((pc*2) == address) {
			return true;
		}
	}
	return false;
}

void replBreakPointExecute(){
	volatile uint16_t table = (uint16_t) &replInstructionTable;
	volatile uint16_t address;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadFlash(table + i * 6);
		*(pointer + 1) = replReadFlash(table + i * 6 + 1);
		if ((pc*2) == address) {
			if (!replEmulate((table + i * 6 + 2)/2)) {
				//mainThreadTime = 1;
				//TIM2->ARR = 1;//reduce debug flow time
				uint16_t instruction;
				uint8_t* pointer;
				pointer=(uint8_t*)&instruction;
				*pointer=replReadFlash(table + i * 6 + 2);
				++pointer;
				*pointer=replReadFlash(table + i * 6 + 3);
				if (replInstructionIs32bit(instruction)) {
					breakPointReturnPC = pc + 2;
					} else {
					breakPointReturnPC = pc + 1;
				}
				pc=(table + i * 6 + 2)/2;
				breakPointFirstInstructionPC = pc;
				regs->pch=(pc>>8)&0xff;
				regs->pcl=pc&0xff;
				replSetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION, true);
				mainThreadTime=1;
				//TCNT1=0;
				//OCR1A=1;
				}else{
				//check if next instruction is not breakpoint, reset REPL_IS_BREAK flag
				if(!(replReadFlash(pc*2)==0xff && replReadFlash(pc*2+1)==0xcf && replCheckBreakPoint())){
					replSetFlag(REPL_IS_BREAK, false);
				}
				if (replGetFlag(REPL_IS_STEP_CALL_AFTER_BREAK)) {
					//replReset();
					replSetFlag(REPL_IS_STEP, true);
					replSetFlag(REPL_IS_STEP_CALL_AFTER_BREAK, false);
					replReadFlashNearProgramCounter();//call that function as it's not called at timerISR function
					replTimerPause();
				}
			}
			return;
		}
	}
}

uint8_t replReadMemWithBreakPointCheck(uint32_t pointer) {
	uint8_t outM=replReadFlash(pointer-1);
	uint8_t out=replReadFlash(pointer);
	uint8_t outP=replReadFlash(pointer+1);
	uint16_t breakPointAddress=0;
	if((out==0xff) && (outP==0xcf) && ((pointer&0x01)==0)){
		breakPointAddress=pointer;
	}
	if((out==0xcf) && (outM==0xff) && ((pointer&0x01)==1)){
		breakPointAddress=pointer-1;
	}
	if(breakPointAddress!=0){
		volatile uint16_t address;
		volatile uint16_t table = (uint16_t) &replInstructionTable;
		for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
			uint8_t *p = (uint8_t*) &address;
			*p = replReadFlash(table + i * 6);
			*(p + 1) = replReadFlash(table + i * 6 + 1);
			if (breakPointAddress == address) {
				if((pointer&0x01)==0){
					out=replReadFlash(table + i * 6 + 2);
					}else{
					out=replReadFlash(table + i * 6 + 3);
				}
				return out;
			}
		}
	}
	return out; //pgm_read_byte(pointer);
}

////uses for 4 bytes breakpoints
//void replBreakPoint(void){
//cli();
//TCCR1B=0;
//OCR1A=REPL_DEFAULT_PROCESSOR_CYCLES;
//TCNT1=REPL_DEFAULT_PROCESSOR_CYCLES-1;
//TCCR1B=1<<CS10;
//sei();
//while(1);
//}

