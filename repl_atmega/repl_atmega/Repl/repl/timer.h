/*This file is provided under license: see license.txt*/
/*
* timer.h
*
* Created: 29-Aug-19 10:24:46 PM
*  Author: root
*/


#ifndef REPL_TIMER_H_
#define REPL_TIMER_H_

#include "../config.h"
	
	typedef struct __attribute__((__packed__))  replStructRegisters{
		uint8_t sreg;
		uint8_t r31;
		uint8_t r30;
		uint8_t r29;
		uint8_t r28;
		uint8_t r27;
		uint8_t r26;
		uint8_t r25;
		uint8_t r24;
		uint8_t r23;
		uint8_t r22;
		uint8_t r21;
		uint8_t r20;
		uint8_t r19;
		uint8_t r18;
		uint8_t r17;
		uint8_t r16;
		uint8_t r15;
		uint8_t r14;
		uint8_t r13;
		uint8_t r12;
		uint8_t r11;
		uint8_t r10;
		uint8_t r9;
		uint8_t r8;
		uint8_t r7;
		uint8_t r6;
		uint8_t r5;
		uint8_t r4;
		uint8_t r3;
		uint8_t r2;
		uint8_t r1;
		uint8_t r0;
		uint8_t pch;
		uint8_t pcl;
	};
	
	extern  volatile bool replCallFromISR;
	extern  volatile uint16_t replBStack,replDStack;

	void replTimerInit(void);
	void replTimerStop(void);
	void replTimerResume(void);
	void replTimerPause(void);
	void replTimerSetRun(void);
	void replTimerSetStep(void);
	void replBeforeIrq(void);
	void replAfterIrq(void);
	


#endif /* TIMER_H_ */