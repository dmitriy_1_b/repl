/*This file is provided under license: see license.txt*/
/*
* emulator.cpp
*
* Created: 21-Mar-20 14:21:00
*  Author: root
*/
#include "emulator.h"

extern volatile replStructRegisters *regs;
extern volatile uint32_t replInterrupts;
extern volatile uint16_t replBStack;
volatile uint16_t stackCopy;
extern volatile uint16_t pc;

volatile uint16_t instruction;

static void stackChangeApply() {
	uint16_t oldStack = replBStack + 35 * 1;
	//pop
	if (stackCopy > oldStack) {
		uint8_t *p;
		for (int16_t i = 0; i < 35 * 1; ++i) {
			p = (uint8_t*) (stackCopy - i);
			*p = *((uint8_t*) (oldStack - i));
		}
	} else { //push
		uint8_t *p;
		for (int16_t i = 35 * 1 - 1; i >= 0; --i) {
			p = (uint8_t*) (stackCopy - i);
			*p = *((uint8_t*) (oldStack - i));
		}
	}
	replBStack = stackCopy - 35 * 1;
	regs = (volatile replStructRegisters*) (replBStack+1);
}

bool replInstructionIs32bit(uint16_t instruction){
	if (((instruction & 0xFE0C) == 0x940C) || ((instruction & 0xFC0F) == 0x9000)) {
		return true;
		}else{
		return false;
	}
}

static uint8_t* numberToRegister(uint8_t n) {
	return (uint8_t*)regs+1+31-n;
}


bool replEmulate(uint16_t instructionPc){
	stackCopy = replBStack + 35 * 1;
	uint8_t* pointer;
	pointer=(uint8_t*)&instruction;
	*pointer=replReadFlash(instructionPc*2);
	++pointer;
	*pointer=replReadFlash(instructionPc*2+1);
	//uint16_t pc=regs->pcl+(((uint16_t)regs->pch)<<8);
	uint16_t oldPc=pc+1;
	//RJMP � Relative Jump
	int16_t signK;
	if((instruction&0xF000)==0xC000){
		signK=instruction&0x0FFF;
		if (signK & 0x800) {
			signK = -(0x1000 - signK);
		}
		pc+=signK+1;
	//IJMP � Indirect Jump	
	}else if(instruction==0x9409){
		pc=regs->r31+(((uint16_t)regs->r30)<<8);
	//JMP � Jump, need to correct for use in device with bigger flash memory(>64k)
	}else if((instruction&0xFE0E)==0x940C){
		pointer=(uint8_t*)&pc;
		*pointer=replReadFlash(instructionPc*2+2);
		++pointer;
		*pointer=replReadFlash(instructionPc*2+3);
	//RCALL � Relative Call to Subroutine
	}else if((instruction&0xF000)==0xD000){
		signK=instruction&0x0FFF;
		if (signK & 0x800) {
			signK = -(0x1000 - signK);
		}
		pc+=signK+1;
		stackCopy-=2;
		stackChangeApply();
		*((uint8_t*)(stackCopy+1))=(oldPc>>8)&0xff;
		*((uint8_t*)(stackCopy+2))=oldPc&0xff;
	//ICALL � Indirect Call to Subroutine
	}else if(instruction==0x9509){
		pc=regs->r31+(((uint16_t)regs->r30)<<8);
		stackCopy-=2;
		stackChangeApply();
		*((uint8_t*)(stackCopy+1))=(oldPc>>8)&0xff;
		*((uint8_t*)(stackCopy+2))=oldPc&0xff;
	//CALL � Long Call to a Subroutine, need to correct for use in device with bigger flash memory(>64k)	
	}else if((instruction&0xFE0E)==0x940E){
		pointer=(uint8_t*)&pc;
		*pointer=replReadFlash(instructionPc*2+2);
		++pointer;
		*pointer=replReadFlash(instructionPc*2+3);
		stackCopy-=2;
		stackChangeApply();
		*((uint8_t*)(stackCopy+1))=(oldPc>>8)&0xff;
		*((uint8_t*)(stackCopy+2))=oldPc&0xff;
	//RET � Return from Subroutine, RETI � Return from Interrupt, no matter interrupt must be enabled	
	}else if((instruction==0x9508) || (instruction==0x9518)){
		pc=*((uint8_t*)(stackCopy+2))+(((uint16_t)*((uint8_t*)(stackCopy+1)))<<8);
		stackCopy+=2;
		stackChangeApply();
		//CPSE � Compare Skip if Equal
	}else if((instruction&0xFC00)==0x1000){
		uint8_t r =(instruction&0x0f)+((instruction&0x200)>>(9-4));
		uint8_t d =(instruction&0x1f0)>>4;
		uint8_t *rr=numberToRegister(r);
		uint8_t *rd=numberToRegister(d);
		if(*rr==*rd){
			uint16_t nextInstruction;
			pointer=(uint8_t*)&nextInstruction;
			*pointer=replReadFlash(instructionPc*2+2);
			++pointer;
			*pointer=replReadFlash(instructionPc*2+3);
			if(replInstructionIs32bit(nextInstruction)){
				pc+=3;
			}else{
				pc+=2;
			}			
		}else{
			++pc;
	    }
		//SBRC � Skip if Bit in Register is Cleared,SBRS � Skip if Bit in Register is Set
		}else if((instruction&0xFC08)==0xFC00){			
			uint8_t r =(instruction&0x1f0)>>4;
			uint8_t b = instruction&0x07;
			bool isSet = (instruction>>9)&0x01;
			uint8_t *rr=numberToRegister(r);
			if(((*rr&(1<<b))&&isSet)||((!(*rr&(1<<b)))&&(!isSet))){
				uint16_t nextInstruction;
				pointer=(uint8_t*)&nextInstruction;
				*pointer=replReadFlash(instructionPc*2+2);
				++pointer;
				*pointer=replReadFlash(instructionPc*2+3);
				if(replInstructionIs32bit(nextInstruction)){
					pc+=3;
					}else{
					pc+=2;
				}
				}else{
				++pc;
			}
		//SBIC � Skip if Bit in I/O Register is Cleared, SBIS � Skip if Bit in I/O Register is Set	
		}else if((instruction&0xFD00)==0x9900){
		uint8_t a =(instruction&0xf8)>>3;
		uint8_t b = instruction&0x07;
		bool isSet = (instruction>>9)&0x01;
		a=*((uint8_t*)(a+0x20));
		if(((a&(1<<b))&&isSet)||((!(a&(1<<b)))&&(!isSet))){
			uint16_t nextInstruction;
			pointer=(uint8_t*)&nextInstruction;
			*pointer=replReadFlash(instructionPc*2+2);
			++pointer;
			*pointer=replReadFlash(instructionPc*2+3);
			if(replInstructionIs32bit(nextInstruction)){
				pc+=3;
				}else{
				pc+=2;
			}
			}else{
			++pc;
		}
	//BRBS � Branch if Bit in SREG is Set, BRCC � Branch if Carry Cleared
	}else if((instruction&0xF800)==0xF000){
		uint8_t b = instruction&0x07;
		bool isSet = !((instruction>>10)&0x01);
		signK = (instruction>>3)&0x7f;
		if (signK & 0x40) {
			signK = -(0x80 - signK);
		}
		if(((regs->sreg&(1<<b))&&isSet)||((!(regs->sreg&(1<<b)))&&(!isSet))){
			pc+=signK+1;
		}else{
			++pc;			
	    }
	}else{
		return false;
	}
	regs->pch=(pc>>8)&0xff;
	regs->pcl=pc&0xff;
	return true;	
}