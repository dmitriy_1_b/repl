 #include <avr/io.h>
 #include <avr/common.h>

 .extern __replTimerISR__
 .extern replBStack
 .extern replDStack
 .extern replCallFromISR

 .global TIMER1_COMPA_vect

	TIMER1_COMPA_vect:
		PUSH R0
		PUSH R1
		PUSH R2
		PUSH R3
		PUSH R4
		PUSH R5
		PUSH R6
		PUSH R7
		PUSH R8
		PUSH R9
		PUSH R10
		PUSH R11
		PUSH R12
		PUSH R13
		PUSH R14
		PUSH R15
		PUSH R16
		PUSH R17
		PUSH R18
		PUSH R19
		PUSH R20
		PUSH R21
		PUSH R22
		PUSH R23
		PUSH R24
		PUSH R25
		PUSH R26
		PUSH R27
		PUSH R28
		PUSH R29
		PUSH R30
		PUSH R31
		IN R1,0x3f;SREG
		PUSH R1
		PUSH R16
		LDS R16,replCallFromISR
		CPI R16,0
		BREQ label
		POP R16
		IN R1,0x3d
		STS replDStack,R1
		IN R1,0x3e
		STS replDStack+1,R1
		LDS R1,replBStack
		OUT 0x3d,R1
		LDS R1,replBStack+1
		OUT 0x3e,R1
		JMP end
label:	POP R16
		IN R1,0x3d
		STS replBStack,R1
		IN R1,0x3e
		STS replBStack+1,R1
		LDS R1,replDStack
		OUT 0x3d,R1
		LDS R1,replDStack+1
		OUT 0x3e,R1
end:	clr	R1
		call __replTimerISR__
		POP R1
		OUT 0x3f,R1;SREG
		POP R31
		POP R30
		POP R29
		POP R28
		POP R27
		POP R26
		POP R25
		POP R24
		POP R23
		POP R22
		POP R21
		POP R20
		POP R19
		POP R18
		POP R17
		POP R16
		POP R15
		POP R14
		POP R13
		POP R12
		POP R11
		POP R10
		POP R9
		POP R8
		POP R7
		POP R6
		POP R5
		POP R4
		POP R3
		POP R2
		POP R1
		POP R0
        reti										

		.global replInstructionTable; 20 breakpoints, 1 breakpoint =6 bytes, 2-address,2-4 instruction

		replInstructionTable:
		.space 120

/*		.global instructionTest;only to test replEmulate
instructionTest:	BRMI testLabel
					call testLabel
					nop
					nop
					nop
testLabel:			BRCC instructionTest*/