/*This file is provided under license: see license.txt*/
#include "main.h"


int main(void){
	MCUSR = 0;
	wdt_disable();
	//mainUsb();
	//asm volatile("ijmp"::);
	modemReadResetByRepl();
	replInitProgram();
	sei();
	errorRead();
	replTimerInit();
	while(1){
		replWorkFunction();
	}	
}