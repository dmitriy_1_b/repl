/*This file is provided under license: see license.txt*/
#include "modem.h"

void modemTurnOff()         // выкл модем
{
	//setPin(PIN_TRANSISTOR);
	//setPinOut(PIN_TRANSISTOR);
	//modemTurnOffPins();//через пины идет ток при выключенном модеме, поэтому режем его
	//replDelayMs(1000);
}

/**
*
*/
void modemTurnOn()        // вкл модем
{	
	//setPin(PIN_CHARGE1);
	//replDelayMs(10000);
	//clearPin(PIN_TRANSISTOR);
	//setPinOut(PIN_TRANSISTOR);
	//replDelayMs(1000);
	//clearPin(PIN_CHARGE1);
	//modemTurnOnPins();
	////_delay_ms(10000);//нужна задержка если команда может пойти после комманды выключить
	//if (!modemIsGPRSOn()) {      // если статус модема != 1
		//modemPowerSwitch();      // выключить питание на 3 секунды
		//int delayCounter = 0;
		//while(!modemIsGPRSOn()) {    // пока статус модема != 1
			//replDelayMs(1);
			//delayCounter++;
			//if (delayCounter >= MODEM_MAX_TURN_TIME_MS) { // если больше 6 сек
				//errorPush(ERROR_MODEM_NOT_ON);
				//return;
			//}
		//}
		//} else {
		//errorPush(ERROR_MODEM_ALREADY_ON);
	//}
}

/**
*
*/
void modemReboot()        // сброс модема
{
	//modemTurnOff();
	//replDelayMs(MODEM_REBOOT_TIME_MS);
	//modemTurnOn();
}

/**
*
*/
boolean modemIsGPRSOn()        // чтение статуса модема
{
	return readPin(PIN_POWER_STATUS);
}

/**
*
*/
void modemPowerSwitch()        // подача высокого уровня на pwrswich на 1500 ms
{
	//#ifndef FIRMWARE_WITHOUT_TRANSISTOR
	////digitalWrite(pwSwGPRSPin,HIGH);
	//setPin(PIN_POWER_SWITCH);
	//replDelayMs(MODEM_SWITCH_TIME_MS);
	////digitalWrite(pwSwGPRSPin,LOW);
	//clearPin(PIN_POWER_SWITCH);
	//#else
	////pinMode(pwSwGPRSPin,OUTPUT);
	//setPinOut(PIN_POWER_SWITCH);
	//replDelayMs(MODEM_SWITCH_TIME_MS);
	////pinMode(pwSwGPRSPin,INPUT);
	//setPinInput(PIN_POWER_SWITCH);
	//#endif
}

