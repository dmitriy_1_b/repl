/*This file is provided under license: see license.txt*/
#ifndef PINS_H_
#define PINS_H_

//#define setPinInput(pins,pin) *(&pins+1)&=~(1<<pin)
inline void setPinInput(volatile unsigned char *pins,unsigned char pin){
	*(pins+1)&=~(1<<pin);
}
//#define setPinOut(pins,pin) *(&pins+1)|=(1<<pin)
inline void setPinOut(volatile unsigned char *pins,unsigned char pin){
	*(pins+1)|=(1<<pin);
}
//#define clearPin(pins,pin) *(&pins+2)&=~(1<<pin)
inline void clearPin(volatile unsigned char *pins,unsigned char pin){
	*(pins+2)&=~(1<<pin);
}
//#define setPin(pins,pin) *(&pins+2)|=(1<<pin)
inline void setPin(volatile unsigned char *pins,unsigned char pin){
	*(pins+2)|=(1<<pin);
}
//#define readPin(pins,pin) pins&(1<<pin)
inline unsigned char readPin(volatile unsigned char *pins,unsigned char pin){
	return (*pins)&(1<<pin);
}




#endif /* PINS_H_ */