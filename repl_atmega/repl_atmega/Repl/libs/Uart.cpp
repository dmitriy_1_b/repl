/*This file is provided under license: see license.txt*/
/*
 * Uart.cpp
 *
 * Created: 27-Aug-19 07:57:45 PM
 *  Author: root
 *
 * light version of Arduino HardwareSerial class made to make code easy portable
 */
#include "Uart.h"

volatile uint8_t _rx_buffer_head;
volatile uint8_t _rx_buffer_tail;
volatile uint8_t _tx_buffer_head;
volatile uint8_t _tx_buffer_tail;
unsigned char _rx_buffer[UART_RX_BUFFER_SIZE];
unsigned char _tx_buffer[UART_TX_BUFFER_SIZE];
bool _written;

void uartBegin(uint32_t baud) {
	_written = false;
	uint16_t baud_setting = (F_CPU / 4 / baud - 1) / 2;
	UCSR0A = 1 << U2X0;
	UBRR0H = baud_setting >> 8;
	UBRR0L = baud_setting;
	//8N1
	UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
}

int uartAvailable(void) {
	return ((unsigned int) (UART_RX_BUFFER_SIZE + _rx_buffer_head
			- _rx_buffer_tail)) % UART_RX_BUFFER_SIZE;
}

int uartRead(void) {
	// if the head isn't ahead of the tail, we don't have any characters
	if (_rx_buffer_head == _rx_buffer_tail) {
		return -1;
	} else {
		unsigned char c = _rx_buffer[_rx_buffer_tail];
		_rx_buffer_tail = (uint8_t) (_rx_buffer_tail + 1) % UART_RX_BUFFER_SIZE;
		return c;
	}
}

void uartTx_udr_empty_irq(void) ;

void uartFlush() {
	// If we have never written a byte, no need to flush. This special
	// case is needed since there is no way to force the TXC (transmit
	// complete) bit to 1 during initialization
	if (!_written)
		return;

	while (bit_is_set(UCSR0B, UDRIE0) || bit_is_clear(UCSR0A, TXC0)) {
		if (bit_is_clear(SREG, SREG_I) && bit_is_set(UCSR0B, UDRIE0))
			// Interrupts are globally disabled, but the DR empty
			// interrupt should be enabled, so poll the DR empty flag to
			// prevent deadlock
			if (bit_is_set(UCSR0A, UDRE0))
				uartTx_udr_empty_irq();
	}
	// If we get here, nothing is queued anymore (DRIE is disabled) and
	// the hardware finished tranmission (TXC is set).
}

void uartTx_udr_empty_irq(void) {
	// If interrupts are enabled, there must be more data in the output
	// buffer. Send the next byte
	unsigned char c = _tx_buffer[_tx_buffer_tail];
	_tx_buffer_tail = (_tx_buffer_tail + 1) % UART_TX_BUFFER_SIZE;

	UDR0 = c;

	// clear the TXC bit -- "can be cleared by writing a one to its bit
	// location". This makes sure flush() won't return until the bytes
	// actually got written
	UCSR0A |= 1 << TXC0;

	if (_tx_buffer_head == _tx_buffer_tail) {
		// Buffer empty, so disable interrupts
		UCSR0B &= ~(1 << UDRIE0);
	}
}

void uartRx_complete_irq(void) {
	if (bit_is_clear(UCSR0A, UPE0)) {
		// No Parity error, read byte and store it in the buffer if there is
		// room
		unsigned char c = UDR0;
		uint8_t i = (unsigned int) (_rx_buffer_head + 1) % UART_RX_BUFFER_SIZE;

		// if we should be storing the received character into the location
		// just before the tail (meaning that the head would advance to the
		// current location of the tail), we're about to overflow the buffer
		// and so we don't write the character or advance the head.
		if (i != _rx_buffer_tail) {
			_rx_buffer[_rx_buffer_head] = c;
			_rx_buffer_head = i;
		}
	} else {
		// Parity error, read byte but discard it
		UDR0;
	};
}

size_t uartWrite(uint8_t c) {
	_written = true;
	// If the buffer and the data register is empty, just write the byte
	// to the data register and be done. This shortcut helps
	// significantly improve the effective datarate at high (>
	// 500kbit/s) bitrates, where interrupt overhead becomes a slowdown.
	if (_tx_buffer_head == _tx_buffer_tail && bit_is_set(UCSR0A, UDRE0)) {
		UDR0 = c;
		UCSR0A |= 1 << TXC0;
		return 1;
	}
	uint8_t i = (_tx_buffer_head + 1) % UART_TX_BUFFER_SIZE;

	// If the output buffer is full, there's nothing for it other than to
	// wait for the interrupt handler to empty it a bit
	while (i == _tx_buffer_tail) {
		if (bit_is_clear(SREG, SREG_I)) {
			// Interrupts are disabled, so we'll have to poll the data
			// register empty flag ourselves. If it is set, pretend an
			// interrupt has happened and call the handler to free up
			// space for us.
			if (bit_is_set(UCSR0A, UDRE0))
				uartTx_udr_empty_irq();
		} else {
			// nop, the interrupt handler will free up space for us
		}
	}

	_tx_buffer[_tx_buffer_head] = c;
	_tx_buffer_head = i;

	UCSR0B |= 1 << UDRIE0;

	return 1;
}

size_t uartPrint_P(const char *str) {
	size_t n = 0;
	size_t size = strlen_P(str);
	if (size == 0)
		return 0;
	while (size--) {
		if (uartWrite(pgm_read_byte_near(str++)))
			n++;
		else
			break;
	}
	return n;
}

size_t uartPrint(const char *str) {
	size_t n = 0;
	size_t size = strlen(str);
	if (size == 0)
	return 0;
	while (size--) {
		if (uartWrite(*str++))
		n++;
		else
		break;
	}
	return n;
}

size_t uartPrint(unsigned long n) {
  char buf[8 * sizeof(long) + 1]; // Assumes 8-bit chars plus zero byte.
  char *str = &buf[sizeof(buf) - 1];

  *str = '\0';
  uint8_t base = 10;

  do {
    unsigned long m = n;
    n /= base;
    char c = m - base * n;
    *--str = c < 10 ? c + '0' : c + 'A' - 10;
  } while(n);

  return uartPrint(str);
}

ISR(USART_RX_vect) {
	uartRx_complete_irq();
}

ISR(USART_UDRE_vect) {
	uartTx_udr_empty_irq();
}
