/*This file is provided under license: see license.txt*/
#ifndef CONFIG_H_
#define CONFIG_H_

#include <stddef.h>
typedef bool boolean;
//#define FIRMWARE_WITHOUT_TRANSISTOR
#include <avr/io.h>
//eclipse glitch
#ifndef _AVR_IOXXX_H_
#if defined (__AVR_ATmega328P__)
#  include <avr/iom328p.h>
#endif
#endif

#define F(string_literal) PSTR(string_literal)

#include <avr/wdt.h>
#include <util/delay.h>
#include <avr/common.h>
#include <inttypes.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "libs/Uart.h"

//#define REPL_USE_USB
#define REPL_USE_GSM

#define AT_COMMAND_APN "AT+CSTT=\"internet\""
#define APN_ONLY "internet"
#define AT_COMMAND_CIPSTART "AT+CIPSTART=\"TCP\",\"your site\",\"80\""
#define WEBSITE "your site"
#define CONTROLLER_ID "564588878643461"//must be 15 symbols

/*my files*/
#include "pins.h"
#include "error.h"
#include "eep.h"
#include "modem.h"
#include "main.h"
#include "repl/repl.h"
#include "user/workFunction.h"
#include "repl/memory.h"
#include "repl/timer.h"
#include "repl/breakPoints.h"
#include "repl/delay.h"
#include "repl/emulator.h"
#ifdef  REPL_USE_USB
#include "usb.h"
#include "usbExportFunctions.h"
#endif
//#include "repl/workFile.h"


#define ONE_BYTE_SEND_DELAY_US 88//10/19200*1000000us+1us (for guarantee)
#define BAUDRATE    115200//230400//115200//57600   
//0,1 error
#define EEPROM_ERROR_OFFSET 0
#define EEPROM_RESET_BY_REPL_OFFSET 2
#define EEPROM_RESET_BY_REPL_VALUE 0x49
#define EEPROM_NOT_RESET_BY_REPL_VALUE 0xFF
#define MODEM_MAX_TURN_TIME_MS 6000
#define MODEM_SWITCH_TIME_MS 1500
#define MODEM_REBOOT_TIME_MS 10000
#define MODEM_COMMAND_MAX_TIME_MS 10000
#define MODEM_AT_MAX_TIMES 3
#define MODEM_TURN_ON_MAX_TIME_MS 30000
#define MODEM_SEND_DATA_MAX_TIME_MS 30000
#define MODEM_RECEIVE_FIRWARE_MAX_TIME_MS 5000
#define MODEM_TIME_BETWEEN_ESTABLISH_CONNECTIONS 5000
#define MODEM_MAX_NTP_ERROR 3
#define MODEM_MAX_PRINT_LENGTH 1800
#define PIN_PULSE &PIND,PIND3
//#define PIN_PULSE2 &PIND,PIND4
#define PIN_RX_GSM &PIND,PIND0
#define PIN_TX_GSM &PIND,PIND1
#define PIN_POWER_SWITCH &PIND,PIND7
#define PIN_POWER_STATUS &PINB,PINB0
#define PIN_LIMIT_SWITCH &PINB,PINB1
#define PIN_POWER_REED &PINB,PINB4
#define PIN_BUTTON_SEND &PIND,PIND2
#define PIN_TAP_ON &PIND,PIND6//M1//красный
#define PIN_TAP_OFF &PIND,PIND5//M2//синий
#define PIN_TRANSISTOR &PINC,PINC2
#define PIN_CHARGE1 &PINC,PINC1
#define PIN_INTERRUPT &PIND,PIND2
#define PIN_METER_RELAY1 &PIND,PIND4
#define PIN_METER_RELAY2 &PIND,PIND2
#define PIN_METER_RELAY3 &PINB,PINB1
#define PIN_METER_RELAY4 &PIND,PIND6
#define PIN_MODEM_RTS &PINC,PINC0
#define ERROR_RTC_NOT_RESPONSE			0
#define ERROR_MODEM_ALREADY_ON			1
#define ERROR_MODEM_NOT_ON				2
#define ERROR_MODEM_NOT_OFF				3
#define ERROR_MODEM_NOT_SEND_FIRST		4
#define ERROR_MODEM_NOT_SEND_SECOND		5
#define ERROR_SYSTEM_RESET_BY_WDT		6
#define ERROR_MODEM_ALREADY_OFF			7
#define ERROR_MODEM_NOT_OFF_3_TIMES		8
#define ERROR_NOT_RESPONSE_TO_AT		9
#define ERROR_NOT_CONNECT_TO_NETWORK	10
#define ERROR_MODEM_SEND_TIMEOUT		11
#define ERROR_MODEM_WRONG_CHECK_SUM		12
#define ERROR_LIMIT_SWITCH		        13
#define ERROR_WRONG_REPL_PROTOCOL		14
#define FIRMWARE_UPLOAD_MAX_ERRORS 6
#define FIRMWARE_UPLOAD_ERRORS_BEFORE_RESET 3
#define PAGE_PER_SEND 4U
#define PROGRAM_PARTS_COUNT 32U
#define PROGRAM_LOAD_START_PAGE 128U
#define REPL_BREAK_POINTS_COUNT 20//1 breakpoint = 6 bytes = 3 nop
#define REPL_BUFFER_SIZE 520UL





#endif /* CONFIG_H_ */
