/*This file is provided under license: see license.txt*/
#ifndef EEP_H_
#define EEP_H_
#include "config.h"

void eepromWriteByte(int i,unsigned char b);

unsigned char eepromReadByte(int i);

#endif /* EEP_H_ */