/*This file is provided under license: see license.txt*/
#include "eep.h"


void eepromWriteByte(int i,unsigned char b){
	uint8_t oldSREG = SREG;
	cli();
	eeprom_write_byte((uint8_t *)i, b);//offset
	SREG = oldSREG;
}



unsigned char eepromReadByte(int i){
	unsigned char b;
	uint8_t oldSREG = SREG;
	cli();
	b = eeprom_read_byte((uint8_t *)i);//offset
	SREG = oldSREG;
	return b;
}