/*This file is provided under license: see license.txt*/
#include "error.h"

static unsigned int globalError=0;

static void errorWrite(){
	unsigned char *bytes;
	bytes = (unsigned char*)&globalError;
	uint8_t oldSREG = SREG;
	cli();
	eeprom_write_byte((uint8_t *)EEPROM_ERROR_OFFSET, bytes[0]);//offset
	eeprom_write_byte((uint8_t *)EEPROM_ERROR_OFFSET+1, bytes[1]);
	SREG = oldSREG;
}

void errorRead(){
	unsigned char *bytes;
	bytes = (unsigned char*)&globalError;
	uint8_t oldSREG = SREG;
	cli();
	bytes[0]=eeprom_read_byte((uint8_t *)EEPROM_ERROR_OFFSET);
	bytes[1]=eeprom_read_byte((uint8_t *)EEPROM_ERROR_OFFSET+1);
	SREG = oldSREG;
	globalError&=~(1<<ERROR_SYSTEM_RESET_BY_WDT);//?? ?????? ??????????? ? eeprom
}

unsigned int errorGet(){
	return globalError;
}

void errorReset(){
	globalError=0;
	errorWrite();
}

void errorPush(unsigned char error){
	globalError|=1<<error;
	errorWrite();
}

boolean errorCheck(unsigned char error){
	return globalError&(1<<error);
}