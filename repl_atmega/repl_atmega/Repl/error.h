/*This file is provided under license: see license.txt*/
#ifndef ERROR_H_
#define ERROR_H_
#include "config.h"

//void errorWrite();

void errorRead();

unsigned int errorGet();

void errorReset();

void errorPush(unsigned char error);

boolean errorCheck(unsigned char error);

#endif /* ERROR_H_ */