/*This file is provided under license: see license.txt*/
#include "modem.h"

const int DATA_PORTION = 1460;

static void writeWithDelay(uint8_t c) {
	//static volatile uint8_t d=c;
	uartWrite(c);
	replDelayUs(ONE_BYTE_SEND_DELAY_US);
}

static void waitSendOk() {
	char response[100];
	response[0] = 0x00;
	int resLeng = 0;
	for (int j = 0; j < 30; ++j) {
		while (uartAvailable()) {
			response[resLeng] = (char) uartRead();
			if (resLeng < 98) {
				++resLeng;
				response[resLeng] = 0x00;
				if (strstr_P(response, F("SEND OK")) != NULL) {
					return;
				}
			}
		}
		replDelayMs(100);
	}
}

extern uint8_t csqLevel;

#define HEADER_LENGTH 154+strlen_P(F(WEBSITE))

boolean modemSendData() {
	uint16_t count = replBytesAvalible();
	char contentLength[20];
	utoa(count,contentLength,10);
	volatile uint32_t counter = count + HEADER_LENGTH + strlen(contentLength);
	if (counter > DATA_PORTION) {
		counter = DATA_PORTION;
	}
	char cipsend[20];
	cipsend[0]=0x00;
	strcpy_P(cipsend, F("AT+CIPSEND="));
	utoa(counter,cipsend+11,10);
	//processedATCommand("AT+CIPSEND?","","");
	if (!modemSendATCommand(cipsend, F(">"), F(""))) {
		return false;
	} else {
		uartPrint_P(F("POST /Commander.php?controller_id="));
		uartPrint_P(F(CONTROLLER_ID));
		uartPrint_P(F("&csq="));
		if (csqLevel < 10)
			uartPrint((unsigned long)0);
		uartPrint((uint8_t)csqLevel); //always size 2
		uartPrint_P(F(" HTTP/1.1\r\nHost: "));
		uartPrint_P(F(WEBSITE));
		uartPrint_P(F("\r\nContent-Type: binary/octet-stream\r\nContent-Length: "));
		uartPrint(contentLength);
		uartPrint_P(F("\r\nConnection: Keep-Alive\r\n\r\n")); //len HEADER_LENGTH+contentLength.length()
		//GPRS.println();
		counter = counter - HEADER_LENGTH - strlen(contentLength);
		for (int i = 0; i < count; ++i) {
			if (counter == 0) {
				int last = count - i;
				if (last > DATA_PORTION) {
					last = DATA_PORTION;
				}
				counter = DATA_PORTION;
				waitSendOk();
				utoa(last,cipsend+11,10);
				if (!modemSendATCommand(cipsend , F(">"), F(""))) {
					return false;
				}
			}
			writeWithDelay(replReadByte());
			--counter;
		}
		replReset();
		//waitSendOk();
		int delayCounter = 0;
		volatile uint8_t c;
		volatile uint16_t pCounter = 0;
		volatile uint8_t checkSum = 'P' + 'P' + 'P' + 'P';
		while (true) {
			if (uartAvailable()) {
				c = (uint8_t) uartRead();
				if (pCounter < 4) {
					if (c == 'P') {
						pCounter++;
					} else {
						pCounter = 0;
					}
				} else {
					checkSum += c;
					if (replBytesAwating() > 0) {
						if (!replSendByte(c)) {
							errorPush(ERROR_WRONG_REPL_PROTOCOL);
							replReset();
							return true;		//change for continue connection
						}
					} else {
						checkSum -= c;
						if (checkSum == c) {
							if (!replProcessing()) {
								replReset();
								return true;
							}
							return true;
						} else {
							errorPush(ERROR_MODEM_WRONG_CHECK_SUM);
							replReset();
							return true;		//change for continue connection
						}
					}

				}
			} else {
				replDelayMs(1);
				delayCounter++;
			}
//			if(replIsInterrupt()){
//				return false;
//			}
			if (delayCounter >= MODEM_SEND_DATA_MAX_TIME_MS) {
				errorPush(ERROR_MODEM_SEND_TIMEOUT);
				return false;
			}
		}
	}
	return true;
}
