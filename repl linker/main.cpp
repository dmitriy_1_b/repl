/*This file is provided under license: see license.txt*/

/* 
 * File:   main.cpp
 * Author: root
 *
 * Created on June 24, 2019, 8:00 PM
 */

#include <unistd.h>
#include <iostream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <string.h>

using namespace std;

#define MAX_OUTPUT 10*1024
#define CONFIG "./config.properties"
char COMPILER_DIR[1024];
char AUTO_SEND[1024];
bool autoSend = false;

void strTrim(char* str) {
    int pos = strlen(str) - 1;
    while ((str[pos] == '\n') || (str[pos] == '\r') || (str[pos] == '\t') || (str[pos] == ' ')) {
        str[pos] = 0x00;
        if (pos > 0){            
        --pos;            
        } else {
        break;
        }
    }
}

/*
 * 
 */
int main(int argc, char** argv) {
    FILE * config = fopen(CONFIG, "r");
    char str[1024];
    while (!feof(config)) {
        if (fgets(str, 1023, config) != NULL) {
            char* p = strchr(str, '=');
            if(p==NULL)
                break;
            *p = 0x00;
            if (strcmp(str, "COMPILER_DIR") == 0) {
                ++p;
                strcpy(COMPILER_DIR, p);
                strTrim(COMPILER_DIR);
            } else if (strcmp(str, "AUTO_SEND") == 0) {
                ++p;
                strcpy(AUTO_SEND, p);
                strTrim(AUTO_SEND);
                if (strcmp(AUTO_SEND, "true") == 0) {
                    autoSend = true;
                }
            }
        } else {
            break;
        }
    }
    fclose(config);
    char output[MAX_OUTPUT];
    std::string base = argv[0];
    size_t pos = base.rfind("/");
    if (pos == std::string::npos) {
        pos = base.rfind("\\");
    }
    if (pos != std::string::npos) {
        base = base.substr(pos + 1);
    }
    base = COMPILER_DIR + base;
    //findAndReplaceAll(base, " ", "\" \"");
    //base = (std::string)"\"" + base + (std::string)"\"";
    bool runLinker = false; //run only on link
    for (int i = 1; i < argc; ++i) {
        std::string add = argv[i];
        //findAndReplaceAll(add, " ", "\" \"");
        pos = add.rfind(" ");
        if (pos != std::string::npos) {
            base += (std::string)" \"" + add + (std::string)"\"";
        } else {
            base += (std::string)" " + add;
        }
        if (strcmp(argv[i], "-T") == 0) {
            runLinker = true;
        }
    }
#ifdef _WIN32
    if(runLinker){
        system(((std::string)"cmd /S /C \"repl_object_worker.exe\"").c_str());
    }
    //std::cout << base;
    FILE *fp = popen(((std::string)"cmd /S /C \""+base+(std::string)"\"").c_str(), "r");
    fgets(output, MAX_OUTPUT, fp);
    printf(output);
    pclose(fp);
    if(runLinker&&autoSend){
        system(((std::string)"cmd /S /C \"repl_firmware_sender.exe\"").c_str());
    }
#else
    if (runLinker) {
        system(((std::string)"repl_object_worker").c_str());
    }
    //std::cout << base;
    FILE *fp = popen(base.c_str(), "r");
    fgets(output, MAX_OUTPUT, fp);
    printf(output);
    pclose(fp);
    if (runLinker && autoSend) {
        system(((std::string)"repl_firmware_sender").c_str());
    }
 #endif   
    return 0;
}

