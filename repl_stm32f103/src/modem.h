/*This file is provided under license: see license.txt*/
#ifndef MODEM_H_
#define MODEM_H_

#include "config.h"

#define CIPSTATUS_IP_START 1
#define CIPSTATUS_PDP_DEACT 2
#define CIPSTATUS_IP_INITIAL 3
#define CIPSTATUS_IP_GPRSACT 4
#define CIPSTATUS_CONNECT_OK 5
#define CIPSTATUS_TCP_CLOSED 6
#define CIPSTATUS_TCP_CLOSING 7
#define CIPSTATUS_IP_CONFIG 8
#define CIPSTATUS_TCP_CONNECTING 9

boolean modemSendData();
boolean modemConnect();
boolean modemSendATCommand(const char* atcommand, const char* mustInResponse1,
		const char* mustInResponse2);
void modemCloseConnection();
boolean modemEstablishConnection(); 
void modemTurnOff();
void modemTurnOn(); 
void modemReboot();
boolean modemIsGPRSOn();
void modemPowerSwitch(); 
boolean modemWaitNetworkConnection();
void modemTurnOffPins();
void modemTurnOnPins();
void modemConfig();
void modemReadResetByRepl();
void modemSetResetByRepl();

#endif /* MODEM_H_ */
