/*This file is provided under license: see license.txt*/
/*
 * step.cpp
 *
 *  Created on: Mar 9, 2020
 *      Author: root
 */
#include "step.h"
volatile uint8_t stepInstructionStorage[6]; //2-4 bytes instruction 2 bytes instruction b to them self
extern volatile uint32_t stepPointReturnPC;
extern volatile replStructRegisters *regs;

void replStepExecute(void) {
	if (!replEmulate(regs->pc)) {
		//mainThreadTime = 1;
		//TIM2->ARR = 1;//reduce debug flow time
		uint32_t offset=0;
		if (replCheck32Bit(replReadFlash(regs->pc + 1))) {
			stepPointReturnPC = regs->pc + 4;
			stepInstructionStorage[0] = replReadFlash(regs->pc);
			stepInstructionStorage[1] = replReadFlash(regs->pc + 1);
			stepInstructionStorage[2] = replReadFlash(regs->pc + 2);
			stepInstructionStorage[3] = replReadFlash(regs->pc + 3);
			//0xFEE7 rjmp PC+0
			stepInstructionStorage[4] = 0xfe;
			stepInstructionStorage[5] = 0xe7;
		} else {
			stepPointReturnPC = regs->pc + 2;
			offset = regs->pc%4;
			stepInstructionStorage[0+offset] = replReadFlash(regs->pc);
			stepInstructionStorage[1+offset] = replReadFlash(regs->pc + 1);
			//0xFEE7 rjmp PC+0
			stepInstructionStorage[2+offset] = 0xfe;
			stepInstructionStorage[3+offset] = 0xe7;
		}
		regs->pc=(uint32_t)&stepInstructionStorage+offset;
		replSetFlag(REPL_STEP_POINT_FIRST_INSTRUCTION, true);
	} else {
		replSetFlag(REPL_IS_STEP,true);
		replTimerPause();
		replReadFlashNearProgramCounter();
	}
}
