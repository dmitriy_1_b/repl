/*This file is provided under license: see license.txt*/
/*
 * flash.cpp
 *
 * Created: 28.03.2019 18:23:29
 *  Author: root
 */
#include "memory.h"

extern volatile replStructRegisters *regs;

void replWriteFlash(uint8_t *buff, uint32_t start, uint32_t end)
		__attribute__ ((section (".boot")));
void replReWriteFlash(uint8_t *buff, uint32_t start, uint32_t end)
		__attribute__ ((section (".boot")));

void (*resetSTM)(void) = 0x0000;
void bootProgramPage(uint32_t page, uint8_t *buf)
		__attribute__ ((section (".boot"))); // BOOTLOADER_SECTION; //__attribute__ ((section (".text.myboot")));//BOOTLOADER_SECTION;//__attribute__ ((section (".text"))) = 0x3e00;

void bootProgramPage(uint32_t page, uint8_t *buf) {
	page=page-((page-0x08000000)%REPL_FLASH_PAGE_SIZE);
	FLASH_Unlock();
	FLASH_ErasePage (page);
	uint32_t i;
	for (i = 0; i < REPL_FLASH_PAGE_SIZE/4; i++) {
		FLASH_ProgramWord(page + i * 4,
				*((uint32_t*) (buf + i * 4)));
	}
	FLASH_Lock();
}

void replWriteFlash(uint8_t *buff, uint32_t start, uint32_t end) {
	if (((start) >= 0x08000000) && ((start) <= REPL_FLASH_END_ADDRESS)) {
		uint32_t offset = start % REPL_FLASH_PAGE_SIZE;
		if (offset > 0) {
			for (int32_t i = end - start; i >= 0; --i) {
				buff[i + offset] = buff[i];
			}
			for (uint32_t i = 0; i < offset; ++i) {
				buff[i] = *((uint8_t*) (start + i - offset));
			}
			start = start - offset;
		}
		offset = REPL_FLASH_PAGE_SIZE-1 - (end % REPL_FLASH_PAGE_SIZE);
		__disable_irq();
		if (offset > 0) {
			for (uint32_t i = end - start + offset; i > end - start; --i) {
				buff[i] = *((uint8_t*) (i + start));
			}
			end = end + offset;
		}
		while (start <= end) {
			bootProgramPage(start, buff);
			start += REPL_FLASH_PAGE_SIZE;
			buff += REPL_FLASH_PAGE_SIZE;
		}
		__enable_irq();
	}
}

void replReWriteFlash(uint8_t *buff, uint32_t start, uint32_t end) {
	replWriteFlash(buff,start,end);
	uint32_t sectionOffset=LOAD_ADDRESS;
	uint8_t sectionCount = *((uint8_t*) (sectionOffset));
	++sectionOffset;
	if(sectionCount==0)
	return;
	for(uint32_t s=0;s<sectionCount;++s){
		for(uint32_t i=0;i<8;++i){
			buff[i]=*((uint8_t*) (sectionOffset+i));
		}
		sectionOffset+=8;
		start = replReadUint32(buff);
		end = replReadUint32(buff+4);
		uint32_t startOffset = start%REPL_FLASH_PAGE_SIZE;
		uint32_t j=0;
		if(startOffset>0){
			for(j;j<startOffset;++j){
				buff[j]=*((uint8_t*) (start+j-startOffset));
			}
			//start=start-startOffset;
		}
		for(int32_t i=startOffset;i<=startOffset+end-start;++i){
			buff[j]=*((uint8_t*) (sectionOffset+i-startOffset));
			++j;
			if(j==REPL_FLASH_PAGE_SIZE){
				bootProgramPage(start+i-startOffset-REPL_FLASH_PAGE_SIZE+1,buff);
				j=0;
			}
		}
		uint32_t endOffset =REPL_FLASH_PAGE_SIZE-1-(end%REPL_FLASH_PAGE_SIZE);
		if(endOffset>0){
			for(j;j<REPL_FLASH_PAGE_SIZE;++j){
				buff[j]=*((uint8_t*) (end+endOffset+j-REPL_FLASH_PAGE_SIZE+1));
			}
			bootProgramPage(end+endOffset-REPL_FLASH_PAGE_SIZE+1,buff);
		}
		sectionOffset+=end-start+1;
	}

}

void replWriteRAM(uint8_t *buff, uint32_t start, uint32_t end) {
	while(start<=end){
		*((uint8_t*)start)=*buff;
		start+=1UL; buff+=1UL;
	}
}

void replWriteEEPROM(uint8_t *buff, uint32_t start, uint32_t end) {
//	uint8_t oldSREG = SREG;
//	cli();
//	while(start<=end){
//		eeprom_write_byte((uint8_t *)start,*buff);
//		start+=1UL; buff+=1UL;
//	}
//	SREG = oldSREG;
}
/*if Exception occurred inside that function it return 0*/
uint8_t replReadFlash(uint32_t pointer) {
	return *((uint8_t*) (pointer)); //pgm_read_byte(pointer);
}

uint8_t replReadRAM(uint32_t pointer) {
	//if(pointer<32){
	//return *((uint8_t*)regs+1+31-pointer);
	//}
	//if(pointer==&SREG){
	//return regs->sreg;
	//}
	//
	//if(pointer==&SPL){
	//return ((uint16_t)regs+sizeof(replStructRegisters)-1)&0xff;
	//}
	//
	//if(pointer==&SPH){
	//return (((uint16_t)regs+sizeof(replStructRegisters)-1)>>8)&0xff;
	//}	
	return *((uint8_t*) (pointer));
}

uint8_t replReadEEPROM(uint32_t pointer) {
	return 0;	//eeprom_read_byte((uint8_t *)pointer);
}

