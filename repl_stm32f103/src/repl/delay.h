/*This file is provided under license: see license.txt*/
#ifndef REPL_DELAY_H_
#define REPL_DELAY_H_
#include "../config.h"

void replDelayUs(uint32_t us);

void replDelayMs(uint32_t ms);

#endif /* MAIN_H_ */

