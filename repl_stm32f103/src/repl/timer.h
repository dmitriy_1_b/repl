/*This file is provided under license: see license.txt*/
/*
* timer.h
*
* Created: 29-Aug-19 10:24:46 PM
*  Author: root
*/


#ifndef REPL_TIMER_H_
#define REPL_TIMER_H_

#include "../config.h"
	typedef struct __attribute__((__packed__))  replStructRegisters{
		uint32_t r8;
		uint32_t r9;
		uint32_t r10;
		uint32_t r11;
		uint32_t r4;
		uint32_t r5;
		uint32_t r6;
		uint32_t r7;
		uint32_t interruptLr;
		uint32_t r0;
		uint32_t r1;
		uint32_t r2;
		uint32_t r3;
		uint32_t r12;
		uint32_t lr;
		uint32_t pc;
		uint32_t psr;
	};
	
	extern  volatile bool replCallFromISR;
	extern  volatile uint32_t replBStack,replDStack;

	void replTimerInit(void);
	void replTimerStop(void);
	void replTimerResume(void);
	void replTimerPause(void);
	void replTimerResumeFromPause(void);
	void replTimerSetStep(void);
	void replTimerSetRun(void);
	void replTimerSetRunNotFromBreakPoint(void);
	


#endif /* TIMER_H_ */
