/*This file is provided under license: see license.txt*/
/*
* emulator.h
*
* Created: 28.03.2019 18:23:47
*  Author: root
*/


#ifndef REPL_EMULATOR_H
#define REPL_EMULATOR_H

#include "../config.h"

bool replEmulate(uint32_t instructionPc);

bool replInstructionIs32bit();

#endif
