/*This file is provided under license: see license.txt*/
#include "delay.h"

void replDelayUs(uint32_t us) {
	volatile uint32_t counter = 7*us;
	while(counter--);
}

void replDelayMs(uint32_t ms) {
	for(uint32_t i=0;i<ms;++i){
		replDelayUs(1000);
	}
}
