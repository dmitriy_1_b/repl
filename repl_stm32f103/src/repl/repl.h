/*This file is provided under license: see license.txt*/
/*
 * repl.h
 *
 * Created: 28.03.2019 7:22:07
 *  Author: root
 */ 


#ifndef REPL_H_
#define REPL_H_

#define REPL_IS_BREAK 0//return from break point
#define REPL_IS_STEP 1//return after step;
#define REPL_STEP 2
#define REPL_IS_STEP_CALL_AFTER_BREAK 3
#define REPL_BREAK_POINT_FIRST_INSTRUCTION 4
#define REPL_STEP_POINT_FIRST_INSTRUCTION 5
#define REPL_IS_PAUSE 6
#define REPL_IS_BREAKPOINT_SET 7

#define REPL_TIMER_PRESCALER 1000
#define REPL_DEFAULT_PROCESSOR_CYCLES 4//4000/1000
#define REPL_RUN_PROCESSOR_CYCLES 36000UL//36000000UL/1000=1 second
#define REPL_STEP_START_TIMER_ARR 1//16//16
#define REPL_STEP_END_TIMER_ARR 1//30//20

#define REPL_COMMAND_NOP 0
#define REPL_COMMAND_RUN 1
#define REPL_COMMAND_WRITE_FLASH 2
#define REPL_COMMAND_WRITE_RAM 3
#define REPL_COMMAND_WRITE_EEPROM 4
#define REPL_COMMAND_READ_FLASH 5
#define REPL_COMMAND_READ_RAM 6
#define REPL_COMMAND_READ_EEPROM 7
#define REPL_COMMAND_PAUSE 8
#define REPL_COMMAND_WRITE_AND_REWRITE_FLASH 9
#define REPL_COMMAND_STEP 10
#define REPL_COMMAND_SET_BREAKPOINT 11
#define REPL_COMMAND_DELETE_ALL_BREAKPOINTS 12
#define REPL_COMMAND_DELETE_BREAKPOINTS 13
#define REPL_COMMAND_READ_NEAR_PC 14//call if somebody try to read flash where address=pc
#define REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT 15
#define REPL_LAST_COMMAND 15

#include "../config.h"


uint32_t replBytesAwating();
/*
return false if wrong data
*/
bool replSendByte(uint8_t b);

uint8_t replReadByte();

uint32_t replBytesAvalible();
/*
Reset repl if you send wrong data
*/
void replReset();

bool replProcessing();

void replInitProgram();

uint32_t replReadUint32(uint8_t * buff);

uint8_t replGetFlagRegister();

void replSetFlagRegister(uint8_t value);

bool replGetFlag(uint8_t number);

void replSetFlag(uint8_t number,bool value);

bool replIsInterrupt();

void replReadFlashNearProgramCounter();


#endif /* REPL_H_ */
