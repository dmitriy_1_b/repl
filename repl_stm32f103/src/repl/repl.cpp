/*This file is provided under license: see license.txt*/
/*
 * repl.cpp
 *
 * Created: 28.03.2019 7:21:38
 *  Author: root
 */
#include "repl.h"

#include "stm32f10x.h"
#define REPL_STATE_START_BYTE 0//need to detect start of request if something before going wrong
#define REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE 1
#define REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE 2
#define REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE 3
#define REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE 4
#define REPL_STATE_READ_DATA_CRC_FIRST_BYTE 5
#define REPL_STATE_READ_DATA_CRC_SECOND_BYTE 6
#define REPL_STATE_READ_COMMAND 7
#define REPL_STATE_READ_DATA 8
#define REPL_STATE_READ_CRC_FIRST_BYTE 9
#define REPL_STATE_READ_CRC_SECOND_BYTE 10
#define REPL_STATE_PROCESSING 11

#define REPL_START_BYTE 0xb7//just random number
#define REPL_CRC_LENGTH 2
#define REPL_CRC_START_VALUE 0xffff
#define REPL_RESPONSE_BASE_PART_SIZE 243
#define REPL_DATA_LENGTH 2//bytes for save length of data

static uint8_t responseSendP = 0;
static uint8_t state = REPL_STATE_START_BYTE;

static uint32_t dataLength;
static uint32_t dataAwating;
uint8_t command;
volatile uint8_t replFlagsRegister;
extern volatile replStructRegisters *regs;
extern volatile uint8_t replInterrupts[30];
volatile static uint16_t crcRequest;
volatile static uint16_t crcResponse;

uint8_t flashBuff[REPL_BUFFER_SIZE];

static void crc16(uint8_t addValue, volatile uint16_t &crc) {
	////CRC-16/CCITT-FALSE
	//    uint8_t x;
	//    x = crc >> 8 ^ addValue;
	//    x ^= x >> 4;
	//    crc = (crc << 8) ^ ((uint16_t) (x << 12)) ^ ((uint16_t) (x << 5)) ^ ((uint16_t) x);
	//    return;
	crc ^= addValue; // XOR byte into least sig. byte of crc
	for (uint8_t i = 8; i != 0; i--) { // Loop over each bit
		if ((crc & 0x0001) != 0) { // If the LSB is set
			crc >>= 1; // Shift right and XOR 0xA001
			crc ^= 0xA001;
		} else
			// Else LSB is not set
			crc >>= 1; // Just shift right
	}
}

uint8_t replGetFlagRegister() {
	return replFlagsRegister;
}

void replSetFlagRegister(uint8_t value) {
	replFlagsRegister = value;
}

bool replGetFlag(uint8_t number) {
	if (replFlagsRegister & (1 << number)) {
		return true;
	} else {
		return false;
	}
}

void replSetFlag(uint8_t number, bool value) {
	if (value) {
		replFlagsRegister |= 1 << number;
	} else {
		replFlagsRegister &= ~(1 << number);
	}
}

uint32_t replBytesAwating() {
	switch (state) {
	case REPL_STATE_START_BYTE:
		return 6 + REPL_CRC_LENGTH + REPL_CRC_LENGTH;
	case REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE:
		return 5 + REPL_CRC_LENGTH + REPL_CRC_LENGTH; //4 bytes data, 1 byte command
	case REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE:
		return 4 + REPL_CRC_LENGTH + REPL_CRC_LENGTH;
	case REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE:
		return 3 + REPL_CRC_LENGTH + REPL_CRC_LENGTH;
	case REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE:
		return 2 + REPL_CRC_LENGTH + REPL_CRC_LENGTH;
	case REPL_STATE_READ_DATA_CRC_FIRST_BYTE:
		return 2 + REPL_CRC_LENGTH + 1; //don't add dataLength before checking that it's valid
	case REPL_STATE_READ_DATA_CRC_SECOND_BYTE:
		return 2 + REPL_CRC_LENGTH;
	case REPL_STATE_READ_COMMAND:
		return dataLength + 1 + REPL_CRC_LENGTH;
	case REPL_STATE_READ_DATA:
		return dataAwating + REPL_CRC_LENGTH;
	case REPL_STATE_READ_CRC_FIRST_BYTE:
		return 2;
	case REPL_STATE_READ_CRC_SECOND_BYTE:
		return 1;
	case REPL_STATE_PROCESSING:
		return 0;
	}
}

/*
 return false if wrong data, wrong protocol
 */
bool replSendByte(uint8_t b) {
	switch (state) {
	case REPL_STATE_START_BYTE:
		if (b != REPL_START_BYTE) {
			replReset();
			return false;
		}
		responseSendP = 0;
		state = REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE;
		crcRequest = REPL_CRC_START_VALUE;
		crc16(b, crcRequest);
		return true;
	case REPL_STATE_READ_DATA_LENGTH_FIRST_BYTE:
		dataLength = ((uint32_t) b) << 24;
		state = REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE;
		crc16(b, crcRequest);
		return true;
	case REPL_STATE_READ_DATA_LENGTH_SECOND_BYTE:
		dataLength += ((uint32_t) b) << 16;
		state = REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE;
		crc16(b, crcRequest);
		return true;
	case REPL_STATE_READ_DATA_LENGTH_THIRD_BYTE:
		dataLength += ((uint32_t) b) << 8;
		state = REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE;
		crc16(b, crcRequest);
		return true;
	case REPL_STATE_READ_DATA_LENGTH_FOURTH_BYTE:
		dataLength += (uint32_t) b;
		dataAwating = dataLength;
		state = REPL_STATE_READ_DATA_CRC_FIRST_BYTE;
		crc16(b, crcRequest);
		return true;
	case REPL_STATE_READ_DATA_CRC_FIRST_BYTE:
		if (b != (crcRequest >> 8)) {
			replReset();
			return false;
		}
		state = REPL_STATE_READ_DATA_CRC_SECOND_BYTE;
		return true;
	case REPL_STATE_READ_DATA_CRC_SECOND_BYTE:
		if (b != (crcRequest & 0xff)) {
			replReset();
			return false;
		}
		state = REPL_STATE_READ_COMMAND;
		return true;
	case REPL_STATE_READ_COMMAND:
		command = b;
		crc16(b, crcRequest);
		if (command <= REPL_LAST_COMMAND) {
			if (dataAwating > 0) {
				state = REPL_STATE_READ_DATA;
				return true;
			} else {
				state = REPL_STATE_READ_CRC_FIRST_BYTE;
				return true;
			}
		} else
			return false;
	case REPL_STATE_READ_DATA:
		crc16(b, crcRequest);
		if (dataAwating > 0) {
			flashBuff[dataLength - dataAwating] = b;
			--dataAwating;
			if (dataAwating > 0)
				return true;
			else {
				state = REPL_STATE_READ_CRC_FIRST_BYTE;
				return true;
			}
		} else
			return false;
	case REPL_STATE_READ_CRC_FIRST_BYTE:
		if (b != (crcRequest >> 8)) {
			replReset();
			return false;
		}
		state = REPL_STATE_READ_CRC_SECOND_BYTE;
		return true;
	case REPL_STATE_READ_CRC_SECOND_BYTE:
		if (b != (crcRequest & 0xff)) {
			replReset();
			return false;
		}
		state = REPL_STATE_PROCESSING;
		return true;
	}
	return false;
}
#define MAX_FUNCTIONS_READ 50
static uint32_t start;
static uint32_t end;
static uint32_t spReadStart = 0;
static uint32_t spReadEnd = 0;
static uint32_t stackFunctions[MAX_FUNCTIONS_READ * 2];
static uint32_t stackFunctionsStart = 0;
static uint32_t stackFunctionsEnd = 0;

uint8_t _replReadByte() {
	if (responseSendP < REPL_RESPONSE_BASE_PART_SIZE) {
		uint32_t reg;
		++responseSendP;
		if (responseSendP == 1) {
			crcResponse = REPL_CRC_START_VALUE;
			return ((replBytesAvalible() - REPL_CRC_LENGTH) >> 8) & 0xff;
		}
		if (responseSendP == 2) {
			return (replBytesAvalible() - REPL_CRC_LENGTH) & 0xff;
		}
		if (responseSendP == 3)
			return command;
		if (responseSendP == 4)
			return replFlagsRegister;
		if (responseSendP > 238) {
			reg = 0;
			uint8_t *p = (uint8_t*) &reg;
			p = (p + responseSendP - 239);
			return *p;
		}
		if (responseSendP > 234) {
//			__ASM
//			volatile ("VMOV %0, S31" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 235);
		}
		if (responseSendP > 230) {
//			__ASM
//			volatile ("VMOV %0, S30" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 231);
		}
		if (responseSendP > 226) {
//			__ASM
//			volatile ("VMOV %0, S29" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 227);
		}
		if (responseSendP > 222) {
//			__ASM
//			volatile ("VMOV %0, S28" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 223);
		}
		if (responseSendP > 218) {
//			__ASM
//			volatile ("VMOV %0, S27" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 219);
		}
		if (responseSendP > 214) {
//			__ASM
//			volatile ("VMOV %0, S26" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 215);
		}
		if (responseSendP > 210) {
//			__ASM
//			volatile ("VMOV %0, S25" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 211);
		}
		if (responseSendP > 206) {
//			__ASM
//			volatile ("VMOV %0, S24" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 207);
		}
		if (responseSendP > 202) {
//			__ASM
//			volatile ("VMOV %0, S23" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 203);
		}
		if (responseSendP > 198) {
//			__ASM
//			volatile ("VMOV %0, S22" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 199);
		}
		if (responseSendP > 194) {
//			__ASM
//			volatile ("VMOV %0, S21" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 195);
		}
		if (responseSendP > 190) {
//			__ASM
//			volatile ("VMOV %0, S20" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 191);
		}
		if (responseSendP > 186) {
//			__ASM
//			volatile ("VMOV %0, S19" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 187);
		}
		if (responseSendP > 182) {
//			__ASM
//			volatile ("VMOV %0, S18" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 183);
		}
		if (responseSendP > 178) {
//			__ASM
//			volatile ("VMOV %0, S17" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 179);
		}
		if (responseSendP > 174) {
//			__ASM
//			volatile ("VMOV %0, S16" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 175);
		}
		if (responseSendP > 170) {
//			__ASM
//			volatile ("VMOV %0, S15" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 171);
		}
		if (responseSendP > 166) {
//			__ASM
//			volatile ("VMOV %0, S14" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 167);
		}
		if (responseSendP > 162) {
//			__ASM
//			volatile ("VMOV %0, S13" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 163);
		}
		if (responseSendP > 158) {
//			__ASM
//			volatile ("VMOV %0, S12" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 159);
		}
		if (responseSendP > 154) {
//			__ASM
//			volatile ("VMOV %0, S11" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 155);
		}
		if (responseSendP > 150) {
//			__ASM
//			volatile ("VMOV %0, S10" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 151);
		}
		if (responseSendP > 146) {
//			__ASM
//			volatile ("VMOV %0, S9" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 147);
		}
		if (responseSendP > 142) {
//			__ASM
//			volatile ("VMOV %0, S8" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 143);
		}
		if (responseSendP > 138) {
//			__ASM
//			volatile ("VMOV %0, S7" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 139);
		}
		if (responseSendP > 134) {
//			__ASM
//			volatile ("VMOV %0, S6" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 135);
		}
		if (responseSendP > 130) {
//			__ASM
//			volatile ("VMOV %0, S5" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 131);
		}
		if (responseSendP > 126) {
//			__ASM
//			volatile ("VMOV %0, S4" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 127);
		}
		if (responseSendP > 122) {
//			__ASM
//			volatile ("VMOV %0, S3" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 123);
		}
		if (responseSendP > 118) {
//			__ASM
//			volatile ("VMOV %0, S2" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 119);
		}
		if (responseSendP > 114) {
//			__ASM
//			volatile ("VMOV %0, S1" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 115);
		}
		if (responseSendP > 110) {
//			__ASM
//			volatile ("VMOV %0, S0" : "=r" (reg) );
			uint8_t *p = (uint8_t*) &reg;
			return *(p + responseSendP - 111);
		}
		if (responseSendP > 109) {
			return (uint8_t)(__get_CONTROL() & 0xff);
		}
		if (responseSendP > 108) {
			return (uint8_t)(__get_FAULTMASK() & 0xff);
		}
		if (responseSendP > 107) {
			return (uint8_t)(__get_BASEPRI() & 0xff);
		}
		if (responseSendP > 106) {
			return (uint8_t)(__get_PRIMASK() & 0xff);
		}
//		if (responseSendP > 106) {//temporary only for test
//			return 0;
//		}
		if (responseSendP > 102) {
			uint32_t stack = (uint32_t) regs + 17 * 4;
			uint8_t *p = (uint8_t*) &stack;
			return p[responseSendP - 104];
		}
		if (responseSendP > 34)
			return *((uint8_t*) regs + responseSendP - 35);
		if (responseSendP > 4) {
			uint8_t *p = (uint8_t*) replInterrupts;
			return *(p + responseSendP - 5);
		}
	}

	switch (command) {
	case REPL_COMMAND_READ_RAM:
		if (start <= end) {
			++start;
			return replReadMemWithBreakPointCheck(start - 1);
		}
		break;
	case REPL_COMMAND_READ_EEPROM:
		if (start <= end) {
			++start;
			return replReadMemWithBreakPointCheck(start - 1);
		}
		break;
	default:
		if (start <= end) {
			++start;
			return replReadMemWithBreakPointCheck(start - 1);
		}
		break; //case REPL_COMMAND_READ_FLASH,REPL_COMMAND_STEP,REPL_COMMAND_PAUSE
	}
	if (spReadStart < spReadEnd) {
		++spReadStart;
		return replReadFlash(spReadStart - 1);
	}
	if (stackFunctionsStart <= stackFunctionsEnd) {
		++stackFunctionsStart;
		return replReadFlash(stackFunctionsStart - 1);
	}
	return 0;
}

uint8_t replReadByte() {
	if ((responseSendP < REPL_RESPONSE_BASE_PART_SIZE) || (start <= end)
			|| (spReadStart < spReadEnd)
			|| (stackFunctionsStart <= stackFunctionsEnd)) {
		uint8_t val = _replReadByte();
		crc16(val, crcResponse);
		return val;
	} else {
		if (responseSendP == REPL_RESPONSE_BASE_PART_SIZE) {
			++responseSendP;
			return (crcResponse >> 8) & 0xff;
		}
		if (responseSendP == (REPL_RESPONSE_BASE_PART_SIZE + 1)) {
			state = REPL_STATE_START_BYTE;
			++responseSendP;
			return crcResponse & 0xff;
		}
		return 0;
	}
}

uint32_t replBytesAvalible() {
//	if (state != REPL_STATE_PROCESSING)
//		return REPL_RESPONSE_BASE_PART_SIZE;
//	if (command == REPL_COMMAND_READ_FLASH || command == REPL_COMMAND_READ_RAM
//			|| command == REPL_COMMAND_READ_EEPROM
//			|| command == REPL_COMMAND_STEP || command == REPL_COMMAND_PAUSE
//			|| command == REPL_COMMAND_READ_NEAR_PC) {
	return end - start + 1 + REPL_RESPONSE_BASE_PART_SIZE + spReadEnd
			- spReadStart + stackFunctionsEnd - stackFunctionsStart + 1
			+ REPL_CRC_LENGTH;
//	} else
//		return REPL_RESPONSE_BASE_PART_SIZE;
}

/*
 Reset repl if you send wrong data
 */
void replReset() {
	state = REPL_STATE_START_BYTE;
	responseSendP = 0;
	start = 1;
	end = 0;
	spReadStart = 0;
	spReadEnd = 0;
	stackFunctionsStart = 1;
	stackFunctionsEnd = 0;
}

uint32_t replReadUint32(uint8_t *buff) {
	uint32_t out;
	out = ((uint32_t) (*buff)) << 24;
	out += ((uint32_t) (*(buff + 1))) << 16;
	out += ((uint32_t) (*(buff + 2))) << 8;
	out += ((uint32_t) (*(buff + 3)));
	return out;
}

extern int main(int argc, char *argv[]);

void replReadFlashNearProgramCounter() {
	uint32_t stackFunctionsCount = 0;
	spReadStart = (uint32_t) regs + 17 * 4 - 16; //-16 because gdb read that address I don't know why
	spReadEnd = spReadStart + 256;
	stackFunctionsStart = (uint32_t) &stackFunctions[0];
	stackFunctionsEnd = stackFunctionsStart;
	uint32_t address = regs->lr - 1;
	stackFunctions[0] = address;
	uint8_t *pointer = (uint8_t*) &stackFunctions[1];
	*pointer = replReadMemWithBreakPointCheck(address);
	*(pointer + 1) = replReadMemWithBreakPointCheck(address + 1);
	*(pointer + 2) = replReadMemWithBreakPointCheck(address + 2);
	*(pointer + 3) = replReadMemWithBreakPointCheck(address + 3);
	stackFunctionsEnd += 8;
	++stackFunctionsCount;
	address = ((uint32_t) main) - 1;
	stackFunctions[2] = address;
	pointer = (uint8_t*) &stackFunctions[3];
	*pointer = replReadMemWithBreakPointCheck(address);
	*(pointer + 1) = replReadMemWithBreakPointCheck(address + 1);
	*(pointer + 2) = replReadMemWithBreakPointCheck(address + 2);
	*(pointer + 3) = replReadMemWithBreakPointCheck(address + 3);
	stackFunctionsEnd += 8;
	++stackFunctionsCount;
	for (uint32_t i = spReadStart; i < spReadEnd; i += 4) {
		uint32_t address;
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadMemWithBreakPointCheck(i);
		*(pointer + 1) = replReadMemWithBreakPointCheck(i + 1);
		*(pointer + 2) = replReadMemWithBreakPointCheck(i + 2);
		*(pointer + 3) = replReadMemWithBreakPointCheck(i + 3);
		if ((address & 0xFF000000) == 0x08000000) {
			address -= 1;
			stackFunctions[stackFunctionsCount * 2] = address;
			uint8_t *pointer = (uint8_t*) &stackFunctions[stackFunctionsCount
					* 2 + 1];
			*pointer = replReadMemWithBreakPointCheck(address);
			*(pointer + 1) = replReadMemWithBreakPointCheck(address + 1);
			*(pointer + 2) = replReadMemWithBreakPointCheck(address + 2);
			*(pointer + 3) = replReadMemWithBreakPointCheck(address + 3);
			stackFunctionsEnd += 8;
			++stackFunctionsCount;
			if (stackFunctionsCount == MAX_FUNCTIONS_READ) {
				break;
			}
		}
	}
	start = regs->pc;
	if (start > 128) {
		start -= 128;
		end = start + 255;
	} else {
		start = 0;
		end = 255;
	}
}

bool replProcessing() {
	replTimerStop();
	switch (command) {
	case REPL_COMMAND_NOP:
		replSetFlag(REPL_IS_BREAKPOINT_SET, false);
		break;
	case REPL_COMMAND_WRITE_FLASH:
		replWriteFlash(flashBuff + 8, replReadUint32(flashBuff),
				replReadUint32(flashBuff + 4));
		break;
	case REPL_COMMAND_WRITE_RAM:
		replWriteRAM(flashBuff + 8, replReadUint32(flashBuff),
				replReadUint32(flashBuff + 4));
		break;
	case REPL_COMMAND_WRITE_EEPROM:
		replWriteEEPROM(flashBuff + 8, replReadUint32(flashBuff),
				replReadUint32(flashBuff + 4));
		break;
	case REPL_COMMAND_WRITE_AND_REWRITE_FLASH:
		replReWriteFlash(flashBuff + 8, replReadUint32(flashBuff),
				replReadUint32(flashBuff + 4));
		modemSetResetByRepl();
		NVIC_SystemReset();
		//cli();
		//wdt_enable (WDTO_15MS); /*reset mcu*/
		break;
	case REPL_COMMAND_READ_FLASH:
		start = replReadUint32(flashBuff);
		end = replReadUint32(flashBuff + 4);
		break;
	case REPL_COMMAND_READ_RAM:
		start = replReadUint32(flashBuff);
		end = replReadUint32(flashBuff + 4);
		break;
	case REPL_COMMAND_READ_EEPROM:
		start = replReadUint32(flashBuff);
		end = replReadUint32(flashBuff + 4);
		break;
	case REPL_COMMAND_PAUSE:
		replTimerPause();
		replReadFlashNearProgramCounter();
		break;
	case REPL_COMMAND_READ_NEAR_PC:
		replReadFlashNearProgramCounter();
		break;
	case REPL_COMMAND_RUN:
		replTimerSetRun();
		break;
	case REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT:
		if(!replGetFlag(REPL_IS_BREAK)) replTimerSetRunNotFromBreakPoint();
		break;
	case REPL_COMMAND_STEP:
		replTimerSetStep();/*replReadFlashNearProgramCounter();-call inside timer function*/
		break;
	case REPL_COMMAND_SET_BREAKPOINT:
		replSetBreakPoint(replReadUint32(flashBuff));
		break;
	case REPL_COMMAND_DELETE_ALL_BREAKPOINTS:
		replDeleteAllBreakPoints();
		break;
	case REPL_COMMAND_DELETE_BREAKPOINTS:
		replDeleteBreakPoints(flashBuff);
		break;
	}
	replTimerResume();
	if (command != REPL_COMMAND_READ_FLASH && command != REPL_COMMAND_READ_RAM
			&& command != REPL_COMMAND_READ_EEPROM
			&& command != REPL_COMMAND_STEP && command != REPL_COMMAND_PAUSE
			&& command != REPL_COMMAND_READ_NEAR_PC)
		state = REPL_STATE_START_BYTE;
	return true;
}

extern uint16_t __datz_start;
extern uint16_t __datz_end;
extern uint16_t __datz_load_start;
extern uint16_t __datz_load_end;

extern uint16_t __bsz_start;
extern uint16_t __bsz_end;

extern uint16_t __ctorz_start;
extern uint16_t __ctorz_end;

typedef void ctorz(void);

extern void
(*__init_arraz_start[])(void) __attribute__((weak));
extern void
(*__init_arraz_end[])(void) __attribute__((weak));

void replInitProgram() {
	__disable_irq();
	volatile uint32_t i = (uint32_t) &__datz_start;
	volatile uint32_t j = (uint32_t) &__datz_load_start;
	while (i < (uint32_t) &__datz_end) {
		*((uint8_t*) i) = *((uint8_t*) (j));
		i += 1UL;
		j += 1UL;
	}

	i = (uint32_t) &__bsz_start;
	while (i < (uint32_t) &__bsz_end) {
		*((uint8_t*) i) = 0x00;
		i += 1UL;
	}
	int count = __init_arraz_end - __init_arraz_start;
	for (i = 0; i < count; i++)
		__init_arraz_start[i]();
	__enable_irq();
}

static bool replIsInterruptAlreadyCall = false;

bool replIsInterrupt() {
	if (!replGetFlag(REPL_IS_BREAK)) {
		replIsInterruptAlreadyCall = false;
	} else if (!replIsInterruptAlreadyCall) {
		replIsInterruptAlreadyCall = true;
		replReset();
		return true;
	}
	return false;
}
