/*This file is provided under license: see license.txt*/
/*
 * Uart.cpp
 *
 * Created: 27-Aug-19 07:57:45 PM
 *  Author: root
 *
 * light version of Arduino HardwareSerial class made to make code easy portable
 */
#include "Uart.h"

volatile uint8_t _rx_buffer_head;
volatile uint8_t _rx_buffer_tail;
volatile uint8_t _tx_buffer_head;
volatile uint8_t _tx_buffer_tail;
unsigned char _rx_buffer[UART_RX_BUFFER_SIZE];
unsigned char _tx_buffer[UART_TX_BUFFER_SIZE];
bool _written;

void uartBegin(uint32_t baud) {
	_written = false;
	//tx
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	//rx
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
	USART_InitTypeDef USART_InitStructure;
	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl =
	USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1, &USART_InitStructure);

	// Enable the USART RX Interrupt
	NVIC_InitTypeDef nvicStructure;
	nvicStructure.NVIC_IRQChannel = USART1_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
	nvicStructure.NVIC_IRQChannelSubPriority = 0;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);
	// Enable the USART TX Interrupt
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);
	//USART_SendData(USART1, 'c');
	//USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
}

void uartSetBaud(uint32_t baud) {
	USART_DeInit(USART1);
	uartBegin(baud);
}

int uartAvailable(void) {
	return ((unsigned int) (UART_RX_BUFFER_SIZE + _rx_buffer_head
			- _rx_buffer_tail)) % UART_RX_BUFFER_SIZE;
}

int uartRead(void) {
	// if the head isn't ahead of the tail, we don't have any characters
	if (_rx_buffer_head == _rx_buffer_tail) {
		return -1;
	} else {
		unsigned char c = _rx_buffer[_rx_buffer_tail];
		_rx_buffer_tail = (uint8_t)(_rx_buffer_tail + 1) % UART_RX_BUFFER_SIZE;
		return c;
	}
}

void uartFlush() {
	while (_rx_buffer_head != _rx_buffer_tail);
}

void uartTx_udr_empty_irq(void) {
	// If interrupts are enabled, there must be more data in the output
	// buffer. Send the next byte
	unsigned char c = _tx_buffer[_tx_buffer_tail];
	_tx_buffer_tail = (_tx_buffer_tail + 1) % UART_TX_BUFFER_SIZE;

	USART_SendData(USART1, c);

	if (_tx_buffer_head == _tx_buffer_tail) {
		// Buffer empty, so disable interrupts
		USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
	}
}

void uartRx_complete_irq(void) {
	unsigned char c = (unsigned char) USART_ReceiveData(USART1);
	uint8_t i = (unsigned int) (_rx_buffer_head + 1) % UART_RX_BUFFER_SIZE;

	// if we should be storing the received character into the location
	// just before the tail (meaning that the head would advance to the
	// current location of the tail), we're about to overflow the buffer
	// and so we don't write the character or advance the head.
	if (i != _rx_buffer_tail) {
		_rx_buffer[_rx_buffer_head] = c;
		_rx_buffer_head = i;
	}
}

size_t uartWrite(uint8_t c) {
	_written = true;

	uint8_t i = (_tx_buffer_head + 1) % UART_TX_BUFFER_SIZE;

	// If the output buffer is full, there's nothing for it other than to
	// wait for the interrupt handler to empty it a bit
	while (i == _tx_buffer_tail) {
	}

	_tx_buffer[_tx_buffer_head] = c;
	_tx_buffer_head = i;

	USART_ITConfig(USART1, USART_IT_TXE, ENABLE);

	return 1;
}

size_t uartPrint(const char *str) {
	size_t n = 0;
	size_t size = strlen(str);
	if (size == 0)
		return 0;
	while (size--) {
		if (uartWrite(*str++))
			n++;
		else
			break;
	}
	return n;
}

size_t uartPrint(unsigned long n) {
  char buf[8 * sizeof(long) + 1]; // Assumes 8-bit chars plus zero byte.
  char *str = &buf[sizeof(buf) - 1];

  *str = '\0';
  uint8_t base = 10;

  do {
    unsigned long m = n;
    n /= base;
    char c = m - base * n;
    *--str = c < 10 ? c + '0' : c + 'A' - 10;
  } while(n);

  return uartPrint(str);
}

extern "C" void USART1_IRQHandler() {
	if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) {
		uartRx_complete_irq();
		USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	}
	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) {
		uartTx_udr_empty_irq();
	}
	//Check for overrun
	//Mistake in USART_GetITStatus USART_IT_ORE so I check it not like that
	//USART_GetITStatus(USART1, USART_IT_ORE) != RESET
	if (USART_GetFlagStatus(USART1, USART_FLAG_ORE)) {
		USART_ClearFlag(USART1, USART_FLAG_ORE);
	}
}
