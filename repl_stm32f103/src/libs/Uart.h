/*This file is provided under license: see license.txt*/
/*
 * Uart.h
 *
 * Created: 27-Aug-19 08:10:59 PM
 *  Author: root
 */

#ifndef UART_H_
#define UART_H_

#define UART_TX_BUFFER_SIZE 64
#define UART_RX_BUFFER_SIZE 128

#include "../config.h"

int uartAvailable(void);
int uartRead(void);
size_t uartWrite(uint8_t c);
void uartFlush(void);
void uartBegin(uint32_t baud);
void uartSetBaud(uint32_t baud);
size_t uartPrint(const char *str);
size_t uartPrint(unsigned long n);

#endif /* UART_H_ */
