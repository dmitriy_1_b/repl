#include "delay.h"

void delayUs(uint32_t us) {
	volatile uint32_t counter = 7*us;
	while(counter--);
}

void delayMs(uint32_t ms) {
	for(uint32_t i=0;i<ms;++i){
		delayUs(1000);
	}
}
