/*
 * workFunction.h
 *
 * Created: 12.02.2019 21:39:08
 *  Author: root
 */ 


#ifndef WORKFUNCTION_H_
#define WORKFUNCTION_H_
#include "../config.h"

void replWorkFunction();

class TestClass 
{
public:

TestClass(uint8_t b);// __attribute__ ((section (".work_function")));
~TestClass();// __attribute__ ((section (".work_function")));

void setB(uint8_t newB);// __attribute__ ((section (".work_function")));
protected:
uint8_t a=15;
uint8_t b=11;
private:
};// __attribute__ ((section (".work_function")));

#endif /* WORKFUNCTION_H_ */