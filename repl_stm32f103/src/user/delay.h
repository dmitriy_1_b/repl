#ifndef DELAY_H_
#define DELAY_H_
#include "../config.h"

void delayUs(uint32_t us);

void delayMs(uint32_t ms);

#endif /* MAIN_H_ */

