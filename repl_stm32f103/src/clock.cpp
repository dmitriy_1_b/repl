/*This file is provided under license: see license.txt*/
#include "clock.h"

void clockInit(){
	//RCC_DeInit();
	//RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_CFGR_PLLMULL16);
	RCC_HSEConfig(RCC_HSE_ON);
	while(RCC_WaitForHSEStartUp()==ERROR);
	RCC_PLLConfig(RCC_PLLSource_HSE_Div1, RCC_CFGR_PLLMULL9);
	RCC_PLLCmd(ENABLE);
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
	RCC_HCLKConfig(RCC_SYSCLK_Div1);
	RCC_PCLK1Config(RCC_HCLK_Div2); //36Mhz
	RCC_PCLK2Config(RCC_HCLK_Div1);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);

}
