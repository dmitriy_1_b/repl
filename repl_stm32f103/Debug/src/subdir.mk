################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/clock.cpp \
../src/main.cpp \
../src/modem_power.cpp \
../src/modem_routine.cpp \
../src/modem_sendData.cpp \
../src/usbExportFunctions.cpp 

OBJS += \
./src/clock.o \
./src/main.o \
./src/modem_power.o \
./src/modem_routine.o \
./src/modem_sendData.o \
./src/usbExportFunctions.o 

CPP_DEPS += \
./src/clock.d \
./src/main.d \
./src/modem_power.d \
./src/modem_routine.d \
./src/modem_sendData.d \
./src/usbExportFunctions.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DUSE_STM3210B_EVAL -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -include/home/user/Desktop/gcc-arm-none-eabi-9-2020-q1-update/arm-none-eabi/include/c++/9.2.1/stdlib.h -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


