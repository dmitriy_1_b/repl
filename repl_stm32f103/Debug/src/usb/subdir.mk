################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/usb/hw_config.c \
../src/usb/stm32_it.c \
../src/usb/usb_desc.c \
../src/usb/usb_endp.c \
../src/usb/usb_istr.c \
../src/usb/usb_main.c \
../src/usb/usb_prop.c \
../src/usb/usb_pwr.c 

OBJS += \
./src/usb/hw_config.o \
./src/usb/stm32_it.o \
./src/usb/usb_desc.o \
./src/usb/usb_endp.o \
./src/usb/usb_istr.o \
./src/usb/usb_main.o \
./src/usb/usb_prop.o \
./src/usb/usb_pwr.o 

C_DEPS += \
./src/usb/hw_config.d \
./src/usb/stm32_it.d \
./src/usb/usb_desc.d \
./src/usb/usb_endp.d \
./src/usb/usb_istr.d \
./src/usb/usb_main.d \
./src/usb/usb_prop.d \
./src/usb/usb_pwr.d 


# Each subdirectory must supply rules for building sources it contributes
src/usb/%.o: ../src/usb/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DUSE_STM3210B_EVAL -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


