################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/usb/Utilities/STM32_EVAL/STM3210B_EVAL/stm3210b_eval.c 

OBJS += \
./src/usb/Utilities/STM32_EVAL/STM3210B_EVAL/stm3210b_eval.o 

C_DEPS += \
./src/usb/Utilities/STM32_EVAL/STM3210B_EVAL/stm3210b_eval.d 


# Each subdirectory must supply rules for building sources it contributes
src/usb/Utilities/STM32_EVAL/STM3210B_EVAL/%.o: ../src/usb/Utilities/STM32_EVAL/STM3210B_EVAL/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -DUSE_STM3210B_EVAL -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


