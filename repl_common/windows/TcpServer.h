/*This file is provided under license: see license.txt*/

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
/* 
 * File:   Tcp.h
 * Author: user
 *
 * Created on September 16, 2020, 11:54 AM
 */

#ifndef TCP_SERVER_H
#define TCP_SERVER_H

class TcpServer{
public:
   void init(int portno); 
   void waitClient();
   void acceptClient();
   int  readClient(void *__buf, size_t __nbytes);
   int  writeClient(const void *__buf, size_t __n);
   void blockClient();
   void unblockClient();
   void closeClient();
private:    
    int _serverSockFd;
    int _clientSockFd;
};


#endif /* TCP_SERVER_H */

