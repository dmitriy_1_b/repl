/*This file is provided under license: see license.txt*/
#include "TcpServer.h"

void TcpServer::init(int portno) {
    WORD wVersionRequested = MAKEWORD(1, 1); // Stuff for WSA functions
    WSADATA wsaData; // Stuff for WSA functions
    WSAStartup(wVersionRequested, &wsaData);
    struct sockaddr_in serv_addr;
    /* First call to socket() function */
    _serverSockFd = socket(AF_INET, SOCK_STREAM, 0);

    if (_serverSockFd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    /* Initialize socket structure */
    ZeroMemory((char *) &serv_addr, sizeof (serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    /* Now bind the host address using bind() call.*/
    if (bind(_serverSockFd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }
}

void TcpServer::waitClient() {
    listen(_serverSockFd, 1);
}

void TcpServer::acceptClient() {
    int clilen;
    struct sockaddr_in cli_addr;
    clilen = sizeof (cli_addr);

    /* Accept actual connection from the client */
    _clientSockFd = accept(_serverSockFd, (struct sockaddr *) &cli_addr, &clilen);

    if (_clientSockFd < 0) {
        perror("ERROR on accept");
        exit(1);
    }
}

int TcpServer::readClient(void *__buf, size_t __nbytes) {
    int res = recv(_clientSockFd, (char*)__buf, __nbytes, 0);
    if(res<=0){
        if(WSAGetLastError() == WSAEWOULDBLOCK)
        errno = EWOULDBLOCK;
        else
        errno = 99999;//just random number    
    }
    return res;
}

int TcpServer::writeClient(const void *__buf, size_t __n) {
    int iSendResult = send(_clientSockFd, (const char*)__buf, __n, 0);

    if (iSendResult == -1) {
        printf("send failed with error: %d\n", errno);
        close(_clientSockFd);
        return iSendResult;
    }
    return iSendResult;
}

void TcpServer::blockClient() {
    u_long mode = 0; // 1 to enable non-blocking socket
    ioctlsocket(_clientSockFd, FIONBIO, &mode);
}

void TcpServer::unblockClient() {
    u_long mode = 1; // 1 to enable non-blocking socket
    ioctlsocket(_clientSockFd, FIONBIO, &mode);
}

void TcpServer::closeClient() {
    // shutdown the connection since we're done
    int iResult = shutdown(_clientSockFd, SD_SEND);
    if (iResult == -1) {
        printf("shutdown failed with error: %d\n", errno);
    }
    // cleanup
    close(_clientSockFd);
    WSACleanup();
}