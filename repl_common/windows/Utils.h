/*This file is provided under license: see license.txt*/


/* 
 * File:   Utils.h
 * Author: root
 *
 * Created on September 15, 2019, 9:57 PM
 */

#ifndef UTILS_H
#define UTILS_H

#include <stdint.h>

#include "string.h"
using namespace std;

class Utils {
public:
    Utils();
    uint64_t hex2int(const char* s);
    void byteToHex(char*buff, uint8_t byte);
    /*copy str immediately don't need free memory*/
    string substr(const char* str,const char* start,const char* end);
    string checkSum(const char* data);
    uint32_t replReadUint32(uint8_t * buff);
    void replWriteUint32(uint8_t * buff,uint32_t val);    
    void crc16(uint8_t addValue, uint16_t &crc);
    uint32_t swapU32(uint32_t val);
    void uint32ToHex(char* buff, uint32_t val);
    virtual ~Utils();
private:

};

void bzero(char* buff,int len);

#endif /* UTILS_H */

