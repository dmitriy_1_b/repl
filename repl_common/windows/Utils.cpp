/*This file is provided under license: see license.txt*/


/* 
 * File:   Utils.cpp
 * Author: root
 * 
 * Created on September 15, 2019, 9:57 PM
 */

#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <string>
#include <memory>
#include <unistd.h>

#include "Utils.h"

Utils::Utils() {
}

Utils::~Utils() {
}

uint64_t Utils::hex2int(const char *s) {
    int i;
    uint64_t val = 0;
    int len = strlen(s);
    for (i = 0; i < len; i++)
        if (s[i] <= 57)
            val += (s[i] - 48)*(1 << (4 * (len - 1 - i)));
        else if (s[i] >= 97)
            val += (s[i] - 87)*(1 << (4 * (len - 1 - i)));
        else
            val += (s[i] - 55)*(1 << (4 * (len - 1 - i)));

    return val;
}

string Utils::substr(const char* str, const char* start, const char* end) {
    string source = str;
    int s = source.find(start);
    if (s == string::npos)
        return "";
    int e = source.rfind(end);
    if (e == string::npos)
        return "";
    return source.substr(s + strlen(start), e - s - strlen(start));
}

void Utils::byteToHex(char* buff, uint8_t byte) {
    static const char* digits = "0123456789ABCDEF";
    buff[0] = digits[(byte / 16)];
    buff[1] = digits[(byte % 16)];
}

void Utils::uint32ToHex(char* buff, uint32_t val) {
    static const char* digits = "0123456789ABCDEF";
    uint8_t byte = uint8_t(val & 0xff);
    buff[0] = digits[(byte / 16)];
    buff[1] = digits[(byte % 16)];
    byte = uint8_t((val >> 8)&0xff);
    buff[2] = digits[(byte / 16)];
    buff[3] = digits[(byte % 16)];
    byte = uint8_t((val >> 16)&0xff);
    buff[4] = digits[(byte / 16)];
    buff[5] = digits[(byte % 16)];
    byte = uint8_t((val >> 24)&0xff);
    buff[6] = digits[(byte / 16)];
    buff[7] = digits[(byte % 16)];
}

string Utils::checkSum(const char* data) {
    uint8_t sum = 0;
    int len = strlen(data);
    for (int i = 0; i < len; ++i) {
        sum += (uint8_t) data[i];
    }
    char buff[3];
    byteToHex(buff, sum);
    buff[2] = 0x00;
    return buff;
}

uint32_t Utils::swapU32(uint32_t val) {
    uint8_t *p = (uint8_t *) & val;
    uint8_t temp;
    temp = p[0];
    p[0] = p[3];
    p[3] = temp;
    temp = p[1];
    p[1] = p[2];
    p[2] = temp;
    return val;
}

uint32_t Utils::replReadUint32(uint8_t * buff) {
    uint32_t out;
    out = ((uint32_t) (*buff)) << 24;
    out += ((uint32_t) (*(buff + 1))) << 16;
    out += ((uint32_t) (*(buff + 2))) << 8;
    out += ((uint32_t) (*(buff + 3)));
    return out;
}

void Utils::replWriteUint32(uint8_t * buff, uint32_t val) {
    buff[0] = (val >> 24)&0xff;
    buff[1] = (val >> 16)&0xff;
    buff[2] = (val >> 8)&0xff;
    buff[3] = (val)&0xff;
}

void Utils::crc16(uint8_t addValue, uint16_t &crc) {
    ////CRC-16/CCITT-FALSE
    //    uint8_t x;
    //    x = crc >> 8 ^ addValue;
    //    x ^= x >> 4;
    //    crc = (crc << 8) ^ ((uint16_t) (x << 12)) ^ ((uint16_t) (x << 5)) ^ ((uint16_t) x);
    //    return;
    crc ^= addValue; // XOR byte into least sig. byte of crc
    for (uint8_t i = 8; i != 0; i--) { // Loop over each bit
        if ((crc & 0x0001) != 0) { // If the LSB is set
            crc >>= 1; // Shift right and XOR 0xA001
            crc ^= 0xA001;
        } else // Else LSB is not set
            crc >>= 1; // Just shift right
    }
}

void bzero(char* buff,int len){
    ZeroMemory(buff,len);
}

