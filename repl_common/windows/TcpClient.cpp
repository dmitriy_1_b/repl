/*This file is provided under license: see license.txt*/
#include "TcpClient.h"

int TcpClient::connectToServer(const char * __name, const char * __service) {
    
    WORD wVersionRequested = MAKEWORD(1, 1); // Stuff for WSA functions
    WSADATA wsaData; // Stuff for WSA functions
    WSAStartup(wVersionRequested, &wsaData);

    struct addrinfo *results = NULL, *prt = NULL, hints;

    ZeroMemory(&hints, sizeof (hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    int res = getaddrinfo(__name, __service, &hints, &results);
    if (res != 0) {
        printf("getaddrinf failed: %d\n", res);
        return -1;
    } else {
        prt = results;
    }

    _socket = socket(results->ai_family, results->ai_socktype, results->ai_protocol);
    if (_socket == -1) {
        printf("Error at socket(): %d\n", errno);
        freeaddrinfo(results);
        return -1;
    }

    res = connect(_socket, prt->ai_addr, (int) prt->ai_addrlen);
    if (res == -1) {
        freeaddrinfo(results);
        close(_socket);
        return -1;
    }
    return 0;
}

int TcpClient::sendToServer(const void *__buf, size_t __n) {

    int res = send(_socket, (const char*)__buf, __n, 0);
    if (res == -1) {
        printf("send failed: %d\n", errno);
        close(_socket);
        return -1;
    }
    return res;
}

int TcpClient::recvFromServer(void *__buf, size_t __n) {
    return recv(_socket, (char*)__buf, __n, 0);
}

void TcpClient::closeSocket() {
    int res = shutdown(_socket, SD_SEND);
    if (res == -1) {
        printf("shutdown failed: %d\n", errno);
    }
    close(_socket);
    WSACleanup();
}
