/*This file is provided under license: see license.txt*/

#include <sys/ioctl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
/* 
 * File:   TcpClient.h
 * Author: user
 *
 * Created on September 16, 2020, 8:06 PM
 */

#ifndef TCPCLIENT_H
#define TCPCLIENT_H
class TcpClient{
public:
   int connectToServer(const char * __name, const char * __service);
   int sendToServer(const void *__buf, size_t __n);
   int recvFromServer(void *__buf, size_t __n);
   void closeSocket();
private:    
    int _socket;
};

#endif /* TCPCLIENT_H */

