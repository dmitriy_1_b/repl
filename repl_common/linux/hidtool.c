/* Name: hidtool.c
 * Project: hid-data example
 * Author: Christian Starkjohann
 * Creation Date: 2008-04-11
 * Tabsize: 4
 * Copyright: (c) 2008 by OBJECTIVE DEVELOPMENT Software GmbH
 * License: GNU GPL v2 (see License.txt), GNU GPL v3 or proprietary (CommercialLicense.txt)
 */
#include "hidtool.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "hiddata.h"
#include "usbconfig.h"  /* for device VID, PID, vendor name and product name */
#include "stdbool.h"
#define REPL_COMMAND_STEP 10
#define USB_PACKAGE_SIZE 128

/* ------------------------------------------------------------------------- */

char *usbErrorMessage(int errCode) {
    static char buffer[80];

    switch (errCode) {
        case USBOPEN_ERR_ACCESS: return "Access to device denied";
        case USBOPEN_ERR_NOTFOUND: return "The specified device was not found";
        case USBOPEN_ERR_IO: return "Communication error with device";
        default:
            sprintf(buffer, "Unknown USB error %d", errCode);
            return buffer;
    }
    return NULL; /* not reached */
}

usbDevice_t *openDevice(void) {
    usbDevice_t *dev = NULL;
    unsigned char rawVid[2] = {USB_CFG_VENDOR_ID}, rawPid[2] = {USB_CFG_DEVICE_ID};
    char vendorName[] = {USB_CFG_VENDOR_NAME, 0}, productName[] = {USB_CFG_DEVICE_NAME, 0};
    int vid = rawVid[0] + 256 * rawVid[1];
    int pid = rawPid[0] + 256 * rawPid[1];
    int err;

    if ((err = usbhidOpenDevice(&dev, vid, vendorName, pid, productName, 0)) != 0) {
        fprintf(stderr, "error finding %s: %s\n", productName, usbErrorMessage(err));
        return NULL;
    }
    return dev;
}

int usb_send(uint8_t* buffer, int len, uint8_t* outBuff) {
    usbDevice_t *dev;
    int err;
    if ((dev = openDevice()) == NULL){
        fprintf(stderr, "error open device");//exit(1);
        return 0;
    }
    uint8_t buff[USB_PACKAGE_SIZE+1];
    int pos = 0;
    bool pause = false;
    if (buffer[7] == REPL_COMMAND_STEP)
        pause = true;
    while (len > 0) {
        memset(buff, 0, sizeof (buff));
        /* room for dummy report ID(made as in the example) */
        if (len < USB_PACKAGE_SIZE) {
            memcpy(buff + 1, buffer + pos, len);
        } else {
            memcpy(buff + 1, buffer + pos, USB_PACKAGE_SIZE);
        }
        pos += USB_PACKAGE_SIZE;
        len -= USB_PACKAGE_SIZE;
        //buff[0]=1;
        if ((err = usbhidSetReport(dev, buff, sizeof (buff))) != 0){
            fprintf(stderr, "error writing data: %s\n", usbErrorMessage(err));
            return 0;
        }
    }
    if(pause){
        usleep(100000);
    }else{
        usleep(1000);
    }
    int size = sizeof (buff);
    memset(buff, 0, sizeof (buff));
    if ((err = usbhidGetReport(dev, 0, buff, &size)) != 0){
        fprintf(stderr, "error reading data: %s\n", usbErrorMessage(err));
        return 0;
    }
    memcpy(outBuff, buff + 1, USB_PACKAGE_SIZE); /* one byte for dummy report ID(made as in the example), 2 bytes for size */
    len = (buff[1] << 8) + buff[2];
    int retLen = len+2;
    len -= USB_PACKAGE_SIZE-2;
    pos = USB_PACKAGE_SIZE;
    while (len > 0) {
        size = sizeof (buff);
        memset(buff, 0, sizeof (buff));
        if ((err = usbhidGetReport(dev, 0, buff, &size)) != 0){
            fprintf(stderr, "error reading data: %s\n", usbErrorMessage(err));
            return 0;
        }
        memcpy(outBuff + pos, buff + 1, USB_PACKAGE_SIZE); /* one byte for dummy report ID(made as in the example), another size */
        pos += USB_PACKAGE_SIZE;
        len -= USB_PACKAGE_SIZE;
    }
    usbhidCloseDevice(dev);
    return retLen;
}

/* ------------------------------------------------------------------------- */
