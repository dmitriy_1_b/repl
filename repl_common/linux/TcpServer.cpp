/*This file is provided under license: see license.txt*/
#include "TcpServer.h"

void TcpServer::init(int portno) {
    struct sockaddr_in serv_addr;
    /* First call to socket() function */
    _serverSockFd = socket(AF_INET, SOCK_STREAM, 0);

    if (_serverSockFd < 0) {
        perror("ERROR opening socket");
        exit(1);
    }

    /* Initialize socket structure */
    bzero((char *) &serv_addr, sizeof (serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    /* Now bind the host address using bind() call.*/
    if (bind(_serverSockFd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        perror("ERROR on binding");
        exit(1);
    }
}

void TcpServer::waitClient() {
    listen(_serverSockFd, 1);
}

void TcpServer::acceptClient() {
    unsigned int clilen;
    struct sockaddr_in cli_addr;
    clilen = sizeof (cli_addr);

    /* Accept actual connection from the client */
    _clientSockFd = accept(_serverSockFd, (struct sockaddr *) &cli_addr, &clilen);

    if (_clientSockFd < 0) {
        perror("ERROR on accept");
        exit(1);
    }
}

int TcpServer::readClient(void *__buf, size_t __nbytes) {
    return read(_clientSockFd, __buf, __nbytes);
}

int TcpServer::writeClient(const void *__buf, size_t __n) {
    int iSendResult = write(_clientSockFd, __buf, __n);

    if (iSendResult == -1) {
        printf("send failed with error: %d\n", errno);
        close(_clientSockFd);
        return iSendResult;
    }
}

void TcpServer::blockClient() {
    u_long mode = 0; // 1 to enable non-blocking socket
    ioctl(_clientSockFd, FIONBIO, &mode);
}

void TcpServer::unblockClient() {
    u_long mode = 1; // 1 to enable non-blocking socket
    ioctl(_clientSockFd, FIONBIO, &mode);
}

void TcpServer::closeClient() {
    // shutdown the connection since we're done
    int iResult = shutdown(_clientSockFd, SHUT_RDWR);
    if (iResult == -1) {
        printf("shutdown failed with error: %d\n", errno);
        close(_clientSockFd);
        return;
    }
    // cleanup
    close(_clientSockFd);
}