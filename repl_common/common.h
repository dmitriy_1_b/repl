/*This file is provided under license: see license.txt*/

/* 
 * File:   common.h
 * Author: user
 *
 * Created on September 17, 2020, 2:49 PM
 */

#ifndef COMMON_H
#define COMMON_H
#ifdef _WIN32
#include "windows/TcpServer.h"
#include "windows/TcpClient.h"
#include "windows/Utils.h"
#else
#include "linux/TcpServer.h"
#include "linux/TcpClient.h"
#include "linux/Utils.h"
#endif
#endif /* COMMON_H */

