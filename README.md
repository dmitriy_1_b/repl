## That project let you to debug microcontroller(now stm32f303vg,atmega328) through any interface(now usb, gsm, lora soon), like through jtag.
## That project described at site eskimos.kz, also you can view some videos on my youtube channel EskimosBlog.
## For linux you can download already prepared virtual machine with ubuntu: https://drive.google.com/file/d/1Wvs7VIDFYjALSFWHMJkIxu44pBe9bpSc/view?usp=sharing
---

## Project has the following structure:
windows - netbeans project for windows(when open project in netbeans use that directory)  
linux - netbeans projects for linux(when open project in netbeans use that directory)  
repl_atmega - 		project for atmega328 mcu use atmel studio and eclipse ide.  
repl_stm32 - 		project for stm32f303vgt6 mcu use eclipse ide.  
repl_stm32f103 - 	project for stm32f103c8t6 mcu use eclipse ide.  
repl_gdbserver_avr - 	common gdbserver source code for atmega328 mcu use netbeans ide.  
repl_gdbserver_stm32 - 	common gdbserver source code for stm32f303vgt6 mcu use netbeans ide.  
repl_php - 		general source code for gsm server.  
repl_firmware_sender - 	common source code for program to send differences between old and new firmware files to the server.  
repl_object_worker - 	common source code for program to rename sections in object files that located at user program code folder.  
repl_linker - 		common source code for program to catch stdin, stdout, stderror pipes of any console program and   
			replace that program by itself to run before or after that program what you need,  
			used to run repl_firmware_sender and repl_object_worker automatically after you push build button at ide.  
			
---			

I also provided link: https://drive.google.com/file/d/1niuitO-ZU8p8rj0CvRi0Cd9_AXKaR8iq/view?usp=sharing  
to download two eclipse ide already configured to that project  
and two recompiled gdb client because I  change some behavior of that clients.  
After you first open eclipse ide you need to change workspace to workspace that I provided(File->Switch Workspace).  

---

## All programs are configurated by config.properties file that located at build folder of project
## It has the following structure for stm32:

ELF_NAME=repl_stm32  
AUTO_SEND=true  
COMPILER_DIR=C:\Program Files (x86)\GNU Tools ARM Embedded\dmitriy\bin\  
OBJ_COPY_PATH=C:\Program Files (x86)\GNU Tools ARM Embedded\dmitriy\bin\arm-none-eabi-objcopy.exe  
FLASH_OFFSET=0x08000000  
CONTROLLER_ID=564588878643469  
MIN_BYTES_BETWEEN_SEGMENTS=128  
OBJECTS_FOLDER=.\src\user\  
HOST=site.com  
LOAD_ADDRESS=134348800  
USE_GSM=true  
CONTROLLER=STM32F103

## and following  for atmega:

ELF_NAME=repl  
AUTO_SEND=true  
COMPILER_DIR=C:\Program Files (x86)\Atmel\Studio\7.0\toolchain\avr8\avr8-gnu-toolchain\bin\  
OBJ_COPY_PATH=C:\Program Files (x86)\Atmel\Studio\7.0\toolchain\avr8\avr8-gnu-toolchain\bin\avr-objcopy.exe  
FLASH_OFFSET=0x00000000  
CONTROLLER_ID=564588878643461  
MIN_BYTES_BETWEEN_SEGMENTS=128  
OBJECTS_FOLDER=.\user\  
HOST=site.com  
LOAD_ADDRESS=16384  
USE_GSM=true  

## It means following:

ELF_NAME=your output build file name  
AUTO_SEND=set true if you want to send differences between firmware automaticlly after build, only for gsm interface  
COMPILER_DIR=directory were located programs to compile your project  
OBJ_COPY_PATH=objcopy util path  
FLASH_OFFSET=start address of flash memory  
CONTROLLER_ID=unique id of your mcu, only for gsm interface  
MIN_BYTES_BETWEEN_SEGMENTS=part of repl_firmware_sender project(don't change), only for gsm interface  
OBJECTS_FOLDER=user code folder  
HOST=site with repl_php, it's very comfortable to make third level domain for this like something.site.com  
LOAD_ADDRESS=address of flash were firmware starts load, only for gsm interface  
USE_GSM=if true use gsm,else usb  
CONTROLLER=name of microcontroller default stm32f303

Also you need to change some constant at config.h files
and base.ini at repl_php project.  

---

## Schematics are simple you can see that at project folder.

---

And Atmega328 fuse bits: High 0xD9, Low 0xEE.
