/*This file is provided under license: see license.txt*/


/* 
 * File:   GDBDecoder.cpp
 * Author: root
 * 
 * Created on September 15, 2019, 8:40 PM
 */

#include "GDBDecoder.h"

GDBDecoder::GDBDecoder(Controller* controller) {
    _controller = controller;
}

void GDBDecoder::decode(string &input, string &output) {
    Utils ut;
    //            output="+#multiprocess+;qRelocInsn+$b9";
    //            if (input.find("$qTStatus")!=std::string::npos) {
    //                //                output="+$tnotrun:0#84";
    //                output="+$T1#85";
    //            } else 
    //                if (input.find("$qTsV")!=std::string::npos || input.find("$qC")!=std::string::npos) {
    //                output="+$#00";
    //            } 
    //                else if (input.find("$qTfV")!=std::string::npos) {
    //                output="+$#00";
    //            } else 

    if (input.find("$?") != std::string::npos) {
        _controller->pause();
        _controller->deleteAllBreakPoints();//execute when connected
        output = "+$S05#b8";
    } else if (input[0] == 0x03) {
        output = "+$";
        string data = _controller->pause();
        output += data;
        output += "#";
        output += ut.checkSum(data.c_str());
        output = "+$S05#b8";
    } else if (input.find("$vCont?") != std::string::npos) {
        output = "+$";
        string data = "vCont;c;s;t;r";
        output += data;
        output += "#";
        output += ut.checkSum(data.c_str());
    } else if (input.find("$c#63") != std::string::npos) {
        _controller->run();
        output = "+"; //continue
    } else if (input.find("$s#73") != std::string::npos) {
        _controller->step();
        output = "+$S05#b8";
        //output = "+";//continue
    } else if (input.find("$g#") != std::string::npos) {
        output = "+$";
        //string data = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000A000FFFFFFFF080002B8010000002000A0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        //string data = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000A000FFFFFFFF080002B8010000002000A00000000000000000000000000000000000";
        //string data = "ee0200000080000000000000de010000e90100000000000000000000000000000000000000000000000000000000000000000000d89f0020fb0700089807000800000021"; //_controller->readRegisters();
        string data = _controller->readRegisters();
        data = data.substr(0, 136); //176
        output += data;
        output += "#";
        output += ut.checkSum(data.c_str());
    } else if (input.find("$p") != std::string::npos) {
        output = "+$";
        uint8_t readCount = 0;
        int address = ut.hex2int(ut.substr(input.c_str(), "$p", "#").c_str());
        //string data = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000A000FFFFFFFF080002B8010000002000A0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
        //string data = "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000A000FFFFFFFF080002B8010000002000A00000000000000000000000000000000000";
        //string data = "ee0200000080000000000000de010000e90100000000000000000000000000000000000000000000000000000000000000000000d89f0020fb0700089807000800000021";//_controller->readRegisters();
        string data = _controller->readRegisters();
        string regData = "";
        if (address == 39) {
            address = 200 * 2;
            readCount = 8;
        }
        if (address > 22 && address < 39) {
            address = 80 * 2 + (address - 23)*16;
            readCount = 16;
        }
        if (address > 18 && address < 23) {
            address = 76 * 2 + (address - 19)*2;
            readCount = 2;
        }
        if (address < 19) {
            address = address * 8;
            readCount = 8;
        }
        for (int i = 0; i < readCount; ++i) {
            regData += data[address + i];
        }
        output += regData;
        output += "#";
        output += ut.checkSum(regData.c_str());
    } else if (input.find("$P") != std::string::npos) {
        string address = ut.substr(input.c_str(), "$P", "=");
        string value = ut.substr(input.c_str(), "=", "#");
        //_controller->writeRegister(ut.hex2int(address.c_str()), value);
        output = "+$OK#9a";
    } else if (input.find("$Hc-1") != std::string::npos || input.find("$Hc0") != std::string::npos /*|| input.find("$qSymbol") != std::string::npos*/) {
        //OK if running,vStopped if stopped//
        output = "+$OK#9a";
    }//            else if (input.find("#qsThreadInfo")!=std::string::npos) {
        //                output="+$l#6c";
        //            } else if (input.find("#qfThreadInfo")!=std::string::npos) {
        //                output="+$m-1#cb";
        //            } else if (input.find("#qAttached")!=std::string::npos) {
        //                //0 - new process 1 -attached
        //                output="+$0#30";
        //            } 
    else if (input.find("$qOffsets") != std::string::npos) {//if I relocated section
        //0 - new process 1 -attached
        output = "+$Text=0;Data=0;Bss=0#04";
        //                output="+$text=0;data=0;bss=0;texz=0;datz=0;bsz=0;boot=0#85";
        //                output="+$#00";
    } else if (input.find("$qSupported") != std::string::npos) {
        //                output="+$PacketSize=200;QStartNoAckMode;+multiprocess-;qRelocInsn-#d0";
        //                //output="+$PacketSize=200;qXfer:memory-map:read-;qXfer:features:read-;QStartNoAckMode+#0A";
        output = "+$";
        string data = "PacketSize=ffff;qXfer:features:read+";
        output += data;
        output += "#";
        output += ut.checkSum(data.c_str());
    } else if (input.find("$m") != std::string::npos) {
        string address = ut.substr(input.c_str(), "$m", ",");
        string len = ut.substr(input.c_str(), ",", "#");
        output = "+$";
        string data = _controller->readMemory(ut.hex2int(address.c_str()), ut.hex2int(len.c_str()));
        output += data;
        output += "#";
        output += ut.checkSum(data.c_str());
    } else if (input.find("$M") != std::string::npos) {
        string address = ut.substr(input.c_str(), "$M", ",");
        string len = ut.substr(input.c_str(), ",", ":");
        string data = ut.substr(input.c_str(), ":", "#");
        output = "+$OK#9a";
        _controller->writeMemory(ut.hex2int(address.c_str()), ut.hex2int(len.c_str()), data);
    } else if (input.find("$Z0") != std::string::npos) {
        string address = ut.substr(input.c_str(), "$Z0,", ",");
        output = "+$OK#9a";
        _controller->setBreakPoint(ut.hex2int(address.c_str()));
//        output = "+$E01#";
//        string data ="E01";
//        output += ut.checkSum(data.c_str());
//         output = "+$#00";
    } else if (input.find("$z0") != std::string::npos) {
        string address = ut.substr(input.c_str(), "$z0,", ",");
        output = "+$OK#9a";
        _controller->deleteBreakPoint(ut.hex2int(address.c_str()));
//        output = "+$E01#";
//        string data ="E01";
//        output += ut.checkSum(data.c_str());
//         output = "+$#00";
    } else if (input.find("$qXfer:features:read:target.xml") != std::string::npos) {
        output = "+$";
        string data=_controller->getTargetXML();
        output += data;
        output += "#";
        output += ut.checkSum(data.c_str());
    } else {//qL1160000000000000000
        output = "+$#00";
    }
}

bool GDBDecoder::isLastCommandRun() {
    return _controller->isLastCommandRun();
}

string GDBDecoder::isCheckIsBreak() {
//    if (!_controller->isLastCommandRun()) {
//        return "";
//    }
    if (_controller->isPause()) {
        return "$S05#b8";
    }
    if (_controller->isCheckIsBreak()) {
        return "$S05#b8";
    } else {
        return "";
    }
}

GDBDecoder::~GDBDecoder() {
}

