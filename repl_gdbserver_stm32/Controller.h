/*This file is provided under license: see license.txt*/


/* 
 * File:   Controller.h
 * Author: root
 *
 * Created on September 8, 2019, 2:54 AM
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H
#define _WIN32_WINNT 0x0501

#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string> 
#include <stdint.h>
#include <sstream>
#include <algorithm>   
#include <stdio.h> 
//#include <sys/socket.h> 
//#include <arpa/inet.h> 
//#include <netdb.h>
//#include <windows.h>      // Needed for all Winsock stuff
//#include <ws2tcpip.h>
//#include <winsock2.h>
#include <time.h>
#include "Buffer.h"
#include <iostream> 
#include <map> 
#include <iterator> 
#include "../repl_common/usb.h"
#include "../repl_common/common.h"

using namespace std;

#define REPL_IS_BREAK 0//return from break point
#define REPL_IS_STEP 1//return after step;
#define REPL_STEP 2
#define REPL_IS_STEP_CALL_AFTER_BREAK 3
#define REPL_BREAK_POINT_FIRST_INSTRUCTION 4
#define REPL_BREAK_POINT_SECOND_INSTRUCTION 5
#define REPL_IS_PAUSE 6//pause by any reason;
#define REPL_IS_BREAKPOINT_SET 7

#define REPL_COMMAND_NOP 0
#define REPL_COMMAND_RUN 1
#define REPL_COMMAND_WRITE_FLASH 2
#define REPL_COMMAND_WRITE_RAM 3
#define REPL_COMMAND_WRITE_EEPROM 4
#define REPL_COMMAND_READ_FLASH 5
#define REPL_COMMAND_READ_RAM 6
#define REPL_COMMAND_READ_EEPROM 7
#define REPL_COMMAND_PAUSE 8
#define REPL_COMMAND_WRITE_AND_REWRITE_FLASH 9
#define REPL_COMMAND_STEP 10
#define REPL_COMMAND_SET_BREAKPOINT 11
#define REPL_COMMAND_DELETE_ALL_BREAKPOINTS 12
#define REPL_COMMAND_DELETE_BREAKPOINTS 13
#define REPL_COMMAND_READ_NEAR_PC 14//call if somebody try to read flash where address=pc
#define REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT 15

#define REPL_START_BYTE 0xb7//just random number
#define FLASH_START 0x08000000
#define FLASH_END 0x08040000
#define MIN_BYTES_BETWEEN_SEGMENTS 128
#define RECEIVE_BUFF_LENGTH 10*1024//10kByte
#define MAX_SEGMENT_SIZE 1024*1024*1024//big number
#define BUFFER_TIME_OUT_S 30
#define REPL_INFO_SIZE 241
#define REPL_NEAR_PC_READ_SIZE 256
#define REPL_STACK_READ_SIZE 256
#define PORTION_SIZE 2048
#define PORTION_MIN_SIZE 128
#define REPL_COMMAND_PAUSE_DELAY_MS 250

class Controller {

    typedef struct __attribute__ ((__packed__)) replStructRegisters {
        uint32_t r8;
        uint32_t r9;
        uint32_t r10;
        uint32_t r11;
        uint32_t r4;
        uint32_t r5;
        uint32_t r6;
        uint32_t r7;
        uint32_t interruptLr;
        uint32_t r0;
        uint32_t r1;
        uint32_t r2;
        uint32_t r3;
        uint32_t r12;
        uint32_t lr;
        uint32_t pc;
        uint32_t psr;
        uint32_t stack;
        uint8_t primask;
        uint8_t basepri;
        uint8_t faultmask;
        uint8_t control;
        uint32_t s0;
        uint32_t s1;
        uint32_t s2;
        uint32_t s3;
        uint32_t s4;
        uint32_t s5;
        uint32_t s6;
        uint32_t s7;
        uint32_t s8;
        uint32_t s9;
        uint32_t s10;
        uint32_t s11;
        uint32_t s12;
        uint32_t s13;
        uint32_t s14;
        uint32_t s15;
        uint32_t s16;
        uint32_t s17;
        uint32_t s18;
        uint32_t s19;
        uint32_t s20;
        uint32_t s21;
        uint32_t s22;
        uint32_t s23;
        uint32_t s24;
        uint32_t s25;
        uint32_t s26;
        uint32_t s27;
        uint32_t s28;
        uint32_t s29;
        uint32_t s30;
        uint32_t s31;
        uint32_t fpscr;
    };
public:
    Controller();
    virtual string readMemory(uint32_t address, uint32_t length);
    virtual void writeMemory(uint32_t address, uint32_t length, string data);
    virtual void setBreakPoint(uint32_t address);
    virtual void deleteBreakPoint(uint32_t address);
    virtual string readRegisters();
    virtual void writeRegister(uint32_t address, string value);
    virtual bool isPause();
    virtual string pause();
    virtual void run();
    virtual void step();
    virtual bool isCheckIsBreak();
    virtual ~Controller();
    virtual bool isLastCommandRun();
    virtual string getTargetXML();
    virtual void deleteAllBreakPoints();
private:
    char HOST[1024];
    char CONTROLLER_ID[1024];
    string TARGET_XML;
    bool _useGSM;
    bool _withFpu;
    void _readNearProgramCounter(int  len);
    void _updateRegisters();
    void _deleteUnSetBreakPoints();
    uint32_t _lastReadTime;
    Buffer _buffer;
    uint8_t _recvbuf[RECEIVE_BUFF_LENGTH];
    int _sendData(uint8_t* data, uint32_t size, bool check=false);
    int _sendPost(uint8_t* data, uint32_t size, bool check=false, int timeOutSec=30);
    uint8_t _replFlagsRegister;
    uint32_t _stackPointer;
    uint8_t _replInterrupts[30];
    replStructRegisters _regs;
    bool _lastCommandRun;
    bool _lastCommandStep;
    map <uint32_t, bool> _breakPoints;
};

#endif /* CONTROLLER_H */

