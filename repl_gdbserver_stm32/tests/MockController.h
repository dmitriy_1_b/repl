/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MockController.h
 * Author: root
 *
 * Created on September 16, 2019, 12:01 AM
 */

#ifndef MOCKCONTROLLER_H
#define MOCKCONTROLLER_H
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../Controller.h"
using namespace std;

class MockController : public Controller {
 public:
  MOCK_METHOD2(readMemory, std::string(uint32_t address, uint32_t length));
    //MOCK_METHOD((std::string),readMemory, (uint32_t address, uint32_t length));
};


#endif /* MOCKCONTROLLER_H */

