#include <stdlib.h>
#include <iostream>
#include "gtest/gtest.h"
#include "../Buffer.h"

TEST(BufferTest, All) {
    Buffer b;
    uint8_t arr[] = {'A', '1', '2', '3', 0x25, 0x00, 0x98};
    b.set(arr, 897, sizeof ( arr));
    uint8_t* res = b.get(897, sizeof ( arr));
    EXPECT_TRUE(0 == memcmp(arr, res, sizeof ( arr)));
    res = b.get(898, sizeof ( arr) - 1);
    EXPECT_TRUE(0 == memcmp(arr + 1, res, sizeof ( arr) - 1));
    b.clear();
    EXPECT_TRUE(b.get(897, sizeof ( arr)) == NULL);
}

TEST(BufferTest, outside) {
    Buffer b;
    uint8_t* arr1 = (uint8_t*)"Hello World";
    uint8_t* arr2 = (uint8_t*)"125Hell8 World8";
    b.set(arr1, 1000, strlen((char*)arr1));
    b.set(arr2, 997, strlen((char*)arr2));
    uint8_t* res = b.get(997, strlen((char*)arr2));
    EXPECT_TRUE(0 == memcmp(arr2, res, strlen((char*)arr2)));
    b.clear();
    EXPECT_TRUE(b.get(1000, 1) == NULL);
}

TEST(BufferTest, inside) {
    Buffer b;
    uint8_t* arr1 = (uint8_t*)"Hello World";
    uint8_t* arr2 = (uint8_t*)"125Hell8 World8";
    uint8_t* arr3 = (uint8_t*)"125Hello World8";
    b.set(arr2, 997, strlen((char*)arr2));
    b.set(arr1, 1000, strlen((char*)arr1));
    uint8_t* res = b.get(997, strlen((char*)arr3));
    EXPECT_TRUE(0 == memcmp(arr3, res, strlen((char*)arr3)));
    b.clear();
    EXPECT_TRUE(b.get(1000, 1) == NULL);
}

TEST(BufferTest, crossRight) {
    Buffer b;
    uint8_t* arr1 = (uint8_t*)"Hello 59rld";
    uint8_t* arr2 = (uint8_t*)"World8897";
    uint8_t* arr3 = (uint8_t*)"Hello World8897";
    b.set(arr1, 1000, strlen((char*)arr1));
    b.set(arr2, 1006, strlen((char*)arr2));
    uint8_t* res = b.get(1000, strlen((char*)arr3));
    EXPECT_TRUE(0 == memcmp(arr3, res, strlen((char*)arr3)));
    b.clear();
    EXPECT_TRUE(b.get(1000, 1) == NULL);
}

TEST(BufferTest, crossLeft) {
    Buffer b;
    uint8_t* arr1 = (uint8_t*)"Hello 59rld";
    uint8_t* arr2 = (uint8_t*)"World8897";
    uint8_t* arr3 = (uint8_t*)"Hello 59rld8897";
    b.set(arr2, 1006, strlen((char*)arr2));
    b.set(arr1, 1000, strlen((char*)arr1));
    uint8_t* res = b.get(1000, strlen((char*)arr3));
    EXPECT_TRUE(0 == memcmp(arr3, res, strlen((char*)arr3)));
    b.clear();
    EXPECT_TRUE(b.get(1000, 1) == NULL);
}

TEST(BufferTest, multi) {
    Buffer b;
    uint8_t* arr1 = (uint8_t*)"Hello world";
    uint8_t* arr2 = (uint8_t*)"1324Hello world1";
    uint8_t* arr3 = (uint8_t*)"ewqeHello world2";
    uint8_t* arr4 = (uint8_t*)"qweHello world3";
    uint8_t* arr5 = (uint8_t*)"wqeqwHello world4";
    uint8_t* arr6 = (uint8_t*)"9qeqwHell4 world9";
    b.set(arr1, 1000, strlen((char*)arr1));
    b.set(arr2, 2000, strlen((char*)arr2));
    b.set(arr3, 3000, strlen((char*)arr3));
    b.set(arr4, 4000, strlen((char*)arr4));
    b.set(arr5, 5000, strlen((char*)arr5));
    b.set(arr6, 5000, strlen((char*)arr6));
    uint8_t* res = b.get(1000, strlen((char*)arr1));
    EXPECT_TRUE(0 == memcmp(arr1, res, strlen((char*)arr1)));
    res = b.get(2000, strlen((char*)arr2));
    EXPECT_TRUE(0 == memcmp(arr2, res, strlen((char*)arr2)));
    res = b.get(3000, strlen((char*)arr3));
    EXPECT_TRUE(0 == memcmp(arr3, res, strlen((char*)arr3)));
    res = b.get(4000, strlen((char*)arr4));
    EXPECT_TRUE(0 == memcmp(arr4, res, strlen((char*)arr4)));
    res = b.get(5000, strlen((char*)arr5));
    EXPECT_FALSE(0 == memcmp(arr5, res, strlen((char*)arr5)));
    res = b.get(5000, strlen((char*)arr6));
    EXPECT_TRUE(0 == memcmp(arr6, res, strlen((char*)arr6)));
    b.clear();
}

TEST(BufferTest, multiCross) {
    Buffer b;
    uint8_t* arr1 = (uint8_t*)"Hell9 9orld";
    uint8_t* arr2 = (uint8_t*)"125Hello";
    uint8_t* arr3 = (uint8_t*)"World89888784";
    uint8_t* arr4 = (uint8_t*)"125Hell9 9orld89888784";
    b.set(arr2, 997, strlen((char*)arr2));
    b.set(arr3, 1006, strlen((char*)arr3));
    b.set(arr1, 1000, strlen((char*)arr1));
    uint8_t* res = b.get(997, strlen((char*)arr4));
    EXPECT_TRUE(0 == memcmp(arr4, res, strlen((char*)arr4)));
    b.clear();
    EXPECT_TRUE(b.get(1000, 1) == NULL);
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
