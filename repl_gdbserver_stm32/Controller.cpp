/*This file is provided under license: see license.txt*/


/* 
 * File:   Controller.cpp
 * Author: root
 * 
 * Created on September 8, 2019, 2:54 AM
 */

#include <valarray>

#include "Controller.h"
#include "Buffer.h"
#define CONFIG "./config.properties"

static void replWriteUint32(uint8_t * buff, uint32_t i) {
    buff[0] = i >> 24;
    buff[1] = (i >> 16)&0xff;
    buff[2] = (i >> 8)&0xff;
    buff[3] = i & 0xff;
}

void strTrim(char* str) {
    int pos = strlen(str) - 1;
    while ((str[pos] == '\n') || (str[pos] == '\r') || (str[pos] == '\t') || (str[pos] == ' ')) {
        str[pos] = 0x00;
        if (pos > 0) {
            --pos;
        } else {
            break;
        }
    }
}

Controller::Controller() {
    _lastReadTime = 0;
    _lastCommandRun = false;
    _lastCommandStep = false;
    _useGSM = false;
    FILE * config = fopen(CONFIG, "r");
    char str[1024];
    while (!feof(config)) {
        if (fgets(str, 1023, config) != NULL) {
            char* p = strchr(str, '=');
            if(p==NULL)
                break;
            *p = 0x00;
            if (strcmp(str, "CONTROLLER_ID") == 0) {
                ++p;
                strcpy(CONTROLLER_ID, p);
                strTrim(CONTROLLER_ID);
            } else if (strcmp(str, "HOST") == 0) {
                ++p;
                strcpy(HOST, p);
                strTrim(HOST);
            } else if (strcmp(str, "USE_GSM") == 0) {
                ++p;
                char USE_GSM[10];
                strcpy(USE_GSM, p);
                strTrim(USE_GSM);
                if (strcmp(USE_GSM, "true") == 0) {
                    _useGSM = true;
                }
            } else if (strcmp(str, "CONTROLLER") == 0) {
                ++p;
                char CONTROLLER[20];
                strcpy(CONTROLLER, p);
                strTrim(CONTROLLER);
                if (strcmp(CONTROLLER, "STM32F103") == 0) {
                    TARGET_XML = "target_without_fpu.xml";
                    _withFpu = false;
                } else {
                    TARGET_XML = "target.xml";
                    _withFpu = true;
                }
            }
        } else {
            break;
        }
    }
    fclose(config);
}

Controller::~Controller() {
}

string Controller::readMemory(uint32_t address, uint32_t length) {
    Utils ut;
    _lastCommandRun = false;
    //    if (address > DATA_END && address < EEPROM_START) {
    //        string zero = "";
    //        for (int i = 0; i < length; ++i) {
    //            zero += "00";
    //        }
    //        return zero;
    //    }
    //if ((time(NULL) - _lastReadTime) < BUFFER_TIME_OUT_S) {
    {
        uint8_t* buff = _buffer.get(address, length);
        if (buff != NULL) {
            char out[2 * length + 1];
            for (int i = 0; i < length; ++i) {
                ut.byteToHex(&out[i * 2], (uint8_t) buff[i]);
            }
            out[2 * length] = 0x00;
            string ret(out, 2 * length);
            _lastReadTime = time(NULL);
            return ret;
        }
    }
    if (((address == _regs.pc) || (address == (_regs.pc + 0))) && (length == 4)) {
        uint8_t buff_[1];
        buff_[0] = REPL_COMMAND_READ_NEAR_PC;
        int len = _sendData((uint8_t*) buff_, 1);
        _readNearProgramCounter(len);
        uint8_t* buff = _buffer.get(address, length);
        if (buff != NULL) {
            char out[2 * length + 1];
            for (int i = 0; i < length; ++i) {
                ut.byteToHex(&out[i * 2], (uint8_t) buff[i]);
            }
            out[2 * length] = 0x00;
            string ret(out, 2 * length);
            _lastReadTime = time(NULL);
            return ret;
        } else {
            return "E01"; //error
        }
    }
    //    } else {
    //        _buffer.clear();
    //    }
    uint8_t buff[9];
    uint32_t readAddress;
    readAddress = address;
    buff[0] = REPL_COMMAND_READ_FLASH;
    replWriteUint32(&buff[1], readAddress);
    if (length > PORTION_MIN_SIZE) {
        replWriteUint32(&buff[5], readAddress + length - 1); //PORTION_SIZE
    } else {
        replWriteUint32(&buff[5], readAddress + PORTION_MIN_SIZE - 1); //PORTION_SIZE   
    }
    int len = _sendData((uint8_t*) buff, 9);
    if (len > 0)
        _updateRegisters();
    len -= REPL_INFO_SIZE;
    if (len > 0) {
        _buffer.set(_recvbuf + REPL_INFO_SIZE, address, len);
        char out[2 * length + 1];
        for (int i = 0; i < length; ++i) {
            ut.byteToHex(&out[i * 2], (uint8_t) _recvbuf[i + REPL_INFO_SIZE]);
        }
        out[2 * length] = 0x00;
        string ret(out, 2 * length);
        _lastReadTime = time(NULL);
        return ret;
    }
    return "E01"; //error
}

void Controller::writeMemory(uint32_t address, uint32_t length, string data) {
    _lastCommandRun = false;
    _buffer.clear();
    Utils ut;
    uint8_t* buff = (uint8_t*) malloc(9 + length);
    //uint8_t buff[9+length]; 
    uint32_t writeAddress;
    writeAddress = address;
    if (address >= FLASH_START && address <= FLASH_END) {
        buff[0] = REPL_COMMAND_WRITE_FLASH;
    } else {
        buff[0] = REPL_COMMAND_WRITE_RAM;
    }
    replWriteUint32(&buff[1], writeAddress);
    replWriteUint32(&buff[5], writeAddress + length - 1);
    for (int i = 0; i < length; i++) {
        buff[9 + i] = (uint8_t) ut.hex2int(data.substr(2 * i, 2).c_str());
    }
    int len = _sendData((uint8_t*) buff, 9 + length);
    if (len > 0)
        _updateRegisters();
    free(buff);
}

void Controller::writeRegister(uint32_t address, string value) {
    _lastCommandRun = false;
    _buffer.clear();
    // do not rewrite stackPointer
    Utils ut;
    //32-sreg
    uint32_t offset = _stackPointer - sizeof (replStructRegisters) + 1;
    if (address < 32) {
        offset += 1 + 31 - address;
        writeMemory(offset, value.length() / 2, value);
    } else if (address == 32) {
        writeMemory(offset, value.length() / 2, value);
    } else if (address == 34) {
        uint32_t val = ut.hex2int(value.c_str()) / 2;
        char buff[5];
        buff[4] = 0x00;
        //        buff[0]=val>>8&0xff;
        //        buff[1]=val&0xff;
        offset += 33;
        ut.byteToHex(buff, (uint8_t) (val >> 8 & 0xff));
        ut.byteToHex(buff, (uint8_t) (val & 0xff));
        string wr(buff, 5);
        writeMemory(offset, wr.length() / 2, wr);
    }

}

void Controller::_updateRegisters() {
    _replFlagsRegister = _recvbuf[1];
    memcpy(_replInterrupts, _recvbuf + 2, sizeof (_replInterrupts));
    memcpy(&_regs, _recvbuf + 32, sizeof (replStructRegisters));
    _stackPointer = _regs.stack;
}

bool Controller::isCheckIsBreak() {
    uint8_t buff[1];
    buff[0] = REPL_COMMAND_NOP;
    int len = _sendData((uint8_t*) buff, 1, true);
    _readNearProgramCounter(len);
    if (_replFlagsRegister & (1 << REPL_IS_PAUSE)) {
        return true;
    } else {
        return false;
    }
}

string Controller::readRegisters() {
    Utils ut;
    //never been the first command so all registers already read
    //    uint8_t buff[1];
    //    buff[0] = REPL_COMMAND_NOP;
    //    int len = sendPost((uint8_t*) buff, 1, 30);
    //    if (len > 0) {
    //        _updateRegisters();
    string ret = "";
    char buff[9];
    buff[8] = 0;
    ut.uint32ToHex(buff, _regs.r0);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r1);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r2);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r3);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r4);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r5);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r6);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r7);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r8);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r9);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r10);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r11);
    ret += buff;
    ut.uint32ToHex(buff, _regs.r12);
    ret += buff;
    ut.uint32ToHex(buff, _regs.stack);
    ret += buff;
    ut.uint32ToHex(buff, _regs.lr);
    ret += buff;
    if (!_replFlagsRegister & (1 << REPL_IS_BREAK)) {
        ut.uint32ToHex(buff, _regs.pc);
    } else {
        ut.uint32ToHex(buff, _regs.pc + 0);
    }
    ret += buff;
    ut.uint32ToHex(buff, _regs.psr);
    ret += buff;
    ut.uint32ToHex(buff, _regs.stack); //msp
    ret += buff;
    ut.uint32ToHex(buff, 0); //psp
    ret += buff;
    buff[3] = 0x00;
    ut.byteToHex(buff, _regs.primask);
    ret += buff;
    buff[3] = 0x00;
    ut.byteToHex(buff, _regs.basepri);
    ret += buff;
    buff[3] = 0x00;
    ut.byteToHex(buff, _regs.faultmask);
    ret += buff;
    buff[3] = 0x00;
    ut.byteToHex(buff, _regs.control);
    ret += buff;
    if (_withFpu) {
        ut.uint32ToHex(buff, _regs.s0);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s1);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s2);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s3);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s4);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s5);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s6);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s7);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s8);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s9);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s10);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s11);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s12);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s13);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s14);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s15);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s16);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s17);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s18);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s19);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s20);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s21);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s22);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s23);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s24);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s25);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s26);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s27);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s28);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s29);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s30);
        ret += buff;
        ut.uint32ToHex(buff, _regs.s31);
        ret += buff;
        ut.uint32ToHex(buff, _regs.fpscr);
        ret += buff;
    }
    return ret;
    //    }
    //    return NULL;
}

string Controller::pause() {
    Utils ut;
    if (_lastCommandStep) {
        return "E01"; //error
    }
    _lastCommandRun = false;
    _buffer.clear();
    Sleep(REPL_COMMAND_PAUSE_DELAY_MS);
    int len;
    for(int i=0;i<3;++i){
        uint8_t buff[1];
        buff[0] = REPL_COMMAND_PAUSE;
        len = _sendData((uint8_t*) buff, 1);
        if(len>0){
            break;
        }else{
            Sleep(REPL_COMMAND_PAUSE_DELAY_MS);
        }
    }
    _readNearProgramCounter(len);
    return "05";
}

void Controller::setBreakPoint(uint32_t address) {
    if (_breakPoints.find(address) == _breakPoints.end()) {
        _breakPoints[address] = true;
        _lastCommandRun = false;
        //_buffer.clear();
        uint8_t buff[5];
        buff[0] = REPL_COMMAND_SET_BREAKPOINT;
        replWriteUint32(buff + 1, address);
        int len = _sendData((uint8_t*) buff, 5);
        if (len > 0) {
            _updateRegisters();
        }
    } else {
        _breakPoints[address] = true;
    }
}

void Controller::deleteBreakPoint(uint32_t address) {
    if (_breakPoints.find(address) != _breakPoints.end()) {
        _breakPoints[address] = false;
    }
}

/*gdbclient always delete all breakpoints after pause program*/
void Controller::_deleteUnSetBreakPoints() {
    map<uint32_t, bool>::iterator it = _breakPoints.begin();
    uint8_t buff[1000];
    uint8_t count = 0;
    while (it != _breakPoints.end()) {
        if (!it->second) {
            replWriteUint32(buff + 2 + count * 4, it->first);
            ++count;
            map<uint32_t, bool>::iterator del = it;
            ++it;
            _breakPoints.erase(del);
        } else {
            ++it;
        }
    }
    if (count > 0) {
        buff[0] = REPL_COMMAND_DELETE_BREAKPOINTS;
        buff[1] = count;
        _lastCommandRun = false;
        _buffer.clear();
        int len = _sendData((uint8_t*) buff, 2 + count * 4);
        if (len > 0) {
            _updateRegisters();
        }
    }
}

void Controller::deleteAllBreakPoints() {
    uint8_t buff[1];
    buff[0] = REPL_COMMAND_DELETE_ALL_BREAKPOINTS;
    int len = _sendData((uint8_t*) buff, 1);
    _readNearProgramCounter(len);
}

void Controller::run() {
    _lastCommandStep = false;
    _deleteUnSetBreakPoints();
    _lastCommandRun = true;
    _buffer.clear();
    uint8_t buff[1];
    buff[0] = REPL_COMMAND_RUN;
    int len = _sendData((uint8_t*) buff, 1);
    if (len > 0) {
        _updateRegisters();
    }
}

void Controller::step() {
    //to optimize breakpoints execution    
    //    _lastCommandStep = true;
    //    _lastCommandRun = false;
    //    _regs.pc+=2;

    Utils ut;
    _lastCommandStep = true;
    _deleteUnSetBreakPoints();
    _lastCommandRun = false;
    _buffer.clear();
    uint8_t buff[1];
    buff[0] = REPL_COMMAND_STEP;
    uint32_t prevPC = _regs.pc;
    int len = _sendData((uint8_t*) buff, 1);
    _readNearProgramCounter(len);
    if (prevPC == _regs.pc) {
        _buffer.clear();
    }
}

void Controller::_readNearProgramCounter(int len) {
    if (len > 0) {
        _updateRegisters();
    }
    len -= REPL_INFO_SIZE;
    if (len > 0) {
        uint32_t start = _regs.pc;
        if (start > 128) {
            start -= 128;
        } else {
            start = 0;
        }
        _buffer.set(_recvbuf + REPL_INFO_SIZE, start, REPL_NEAR_PC_READ_SIZE);
        _lastReadTime = time(NULL);
    }
    len -= REPL_NEAR_PC_READ_SIZE;
    if (len > 0) {
        uint32_t start = _regs.stack - 16; //-16 because gdb read that address I don't know why
        _buffer.set(_recvbuf + REPL_INFO_SIZE + REPL_NEAR_PC_READ_SIZE, start, REPL_STACK_READ_SIZE);
        _lastReadTime = time(NULL);
    }
    len -= REPL_STACK_READ_SIZE;
    if (len > 0) {
        uint8_t *start = _recvbuf + REPL_INFO_SIZE + REPL_NEAR_PC_READ_SIZE + REPL_STACK_READ_SIZE;
        for (int i = 0; i < len; i += 8) {
            _buffer.set((uint8_t*) (start + 4), *((uint32_t*) (start)), 4);
            start += 8;
        }
        _lastReadTime = time(NULL);
    }
}

int _usb_send(uint8_t* buffer, int len, uint8_t* outBuff) {
    Utils ut;
    int size = usb_send(buffer, len, outBuff);
    uint16_t crc = 0xffff;
    for (int i = 0; i < size - 2; ++i) {
        ut.crc16(outBuff[i], crc);
    }
    uint16_t responseCrc = ((uint16_t) outBuff[size - 2]) << 8;
    responseCrc += ((uint8_t) outBuff[size - 1]);
    if (crc == responseCrc) {
        size -= 4; //2 length, 2 crc
        memcpy(outBuff, outBuff + 2, size); //only length, because crc in the end
        return size;
    } else {
        return 0;
    }
}

int Controller::_sendData(uint8_t* data, uint32_t size, bool check) {
    if(_useGSM)
    return _sendPost(data,size,check);
    uint8_t buff[1024];
    Utils ut;
    buff[0] = REPL_START_BYTE;
    ut.replWriteUint32(buff + 1, size - 1);
    memcpy(buff + 7, data, size);
//    if (buff[7] == REPL_COMMAND_PAUSE)//to guaranty than command run accomplished
//        Sleep(1000);
    uint16_t crc = 0xffff;
    for (uint32_t i = 0; i < 5; ++i) {
        ut.crc16(buff[i], crc);
    }
    buff[5] = (crc >> 8)&0xff;
    buff[6] = crc & 0xff;
    size += 7;
    for (uint32_t i = 7; i < size; ++i) {
        ut.crc16(buff[i], crc);
    }
    buff[size] = (crc >> 8)&0xff;
    buff[size + 1] = crc & 0xff;
    size += 2;
    if (!check) {
        if (buff[7] == REPL_COMMAND_RUN) {
            int len = _usb_send(buff, size, _recvbuf);
            Sleep(10);
            return len;
        } else if ((buff[7] == REPL_COMMAND_DELETE_BREAKPOINTS)
                || (buff[7] == REPL_COMMAND_SET_BREAKPOINT)) {
            int len = _usb_send(buff, size, _recvbuf);
            Sleep(100);
            return len;
        } else {
            return _usb_send(buff, size, _recvbuf);
        }
    } else {
        int len = _usb_send(buff, size, _recvbuf);
        if ((!(_recvbuf[1] & (1 << REPL_IS_BREAK))) || (len == 0)) {
            buff[0] = REPL_START_BYTE;
            ut.replWriteUint32(buff + 1, 0);
            uint16_t crc = 0xffff;
            for (uint32_t i = 0; i < 5; ++i) {
                ut.crc16(buff[i], crc);
            }
            buff[5] = (crc >> 8)&0xff;
            buff[6] = crc & 0xff;
            buff[7] = REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT;
            ut.crc16(buff[7], crc);
            buff[8] = (crc >> 8)&0xff;
            buff[9] = crc & 0xff;
            len = _usb_send(buff, 10, _recvbuf);
            _recvbuf[1] = 0x00;
        }
        return len;
    }
}

int Controller::_sendPost(uint8_t* data, uint32_t size, bool check, int timeOutSec) {
    string page = "CommandSaver.php";
    if (check) {
        page = "GetLastResponse.php";
    }
    Utils ut;
    uint8_t buff[1024];
    buff[0] = REPL_START_BYTE;
    ut.replWriteUint32(buff + 1, size - 1);
    memcpy(buff + 7, data, size);
    uint16_t crc = 0xffff;
    for (uint32_t i = 0; i < 5; ++i) {
        ut.crc16(buff[i], crc);
    }
    buff[5] = (crc >> 8)&0xff;
    buff[6] = crc & 0xff;
    size += 7;
    for (uint32_t i = 7; i < size; ++i) {
        ut.crc16(buff[i], crc);
    }
    buff[size] = (crc >> 8)&0xff;
    buff[size + 1] = crc & 0xff;
    size += 2;
    TcpClient client;
    int res = client.connectToServer(HOST,"80");
    if (res == -1) {
        return false;
    }

    string header;
    header = "POST /";
    header += page;
    header += "?controller_id=";
    header += CONTROLLER_ID;
    header += "&data_length_awaiting=";
    if (data[0] == REPL_COMMAND_READ_FLASH ||
            data[0] == REPL_COMMAND_READ_RAM ||
            data[0] == REPL_COMMAND_READ_EEPROM) {
        header += "246"; //245-base part
    } else {
        header += "245"; //245-base part   
    }
    //header += "&XDEBUG_SESSION_START=netbeans-xdebug";
    header += " HTTP/1.1\r\n";
    header += "Host: ";
    header += HOST;
    //header += "repl_test:8009";//HOST;
    header += "\r\n";
    header += "Content-Type: binary/octet-stream\r\n";
    //header += "Connection: Keep-Alive\r\n"
    header += "Content-Length: ";
    //mingw glitch https://stackoverflow.com/questions/12975341/to-string-is-not-a-member-of-std-says-g-mingw
    std::ostringstream os;
    os << size;
    header += os.str();
    header += "\r\n";
    header += "\r\n";

    res = client.sendToServer(header.c_str(), header.size());
    if (res == -1) {
        return false;
    }

    res = client.sendToServer((char*) buff, size);
    if (res == -1) {
        return false;
    }
    //Sleep(1);   
    int length = 0;
    res = 0;
    int portion = 0;
    char outBuff[1024 * 10];
    for (int i = 0; i < timeOutSec; ++i) {
        //char* point = recvbuf;
        portion = client.recvFromServer(outBuff + res, RECEIVE_BUFF_LENGTH);
        if (portion < 0) {
            res = 0;
            break;
        }
        res += portion;
        outBuff[res] = 0x00;
        int statusLen = strlen("HTTP/1.1 200");
        if (res < statusLen) {
            Sleep(1000);
            continue;
        }
        if (memcmp(outBuff, "HTTP/1.1 200", statusLen) != 0) {
            res = 0;
            break;
        }
        char* cLenStart;
        if ((cLenStart = strstr(outBuff, "Content-Length: ")) == 0) {
            Sleep(1000);
            continue;
        }
        char* cLenEnd;
        if ((cLenEnd = strstr(cLenStart, "\r\n")) == 0) {
            Sleep(1000);
            continue;
        }
        *cLenEnd = 0x00;
        int cLength = atoi(cLenStart + strlen("Content-Length: "));
        *cLenEnd = '\r';
        if (cLength <= 0) {
            res = 0;
            break;
        }
        if (res < cLength) {
            Sleep(1000);
            continue;
        } else {
            break;
        }
    }
    if (res != 0) {
        char* dataStart;
        if ((dataStart = strstr(outBuff, "\r\n\r\n")) != 0) {
            dataStart += 4;
            length = res + outBuff - dataStart;
            memcpy(_recvbuf, dataStart, length);
        }
    }
    client.closeSocket();
    crc = 0xffff;
    for (int i = 0; i < length - 2; ++i) {
        ut.crc16(_recvbuf[i], crc);
    }
    uint16_t responseCrc = ((uint16_t) _recvbuf[length - 2]) << 8;
    responseCrc += ((uint8_t) _recvbuf[length - 1]);
    if (crc == responseCrc) {
        length -= 4; //2 length, 2 crc
        memcpy(_recvbuf, _recvbuf + 2, length); //only length, because crc in the end
        return length;
    } else {
        return 0;
    }
    return 0;
}

bool Controller::isPause() {
    return _replFlagsRegister & (1 << REPL_IS_PAUSE);
}

bool Controller::isLastCommandRun() {
    return _lastCommandRun;
}

string Controller::getTargetXML() {
    std::ifstream t(TARGET_XML);
    std::string data((std::istreambuf_iterator<char>(t)),
            std::istreambuf_iterator<char>());
    return data;
}