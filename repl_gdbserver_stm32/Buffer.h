/*This file is provided under license: see license.txt*/


/* 
 * File:   Buffer.h
 * Author: root
 *
 * Created on September 18, 2019, 11:21 AM
 */

#ifndef BUFFER_H
#define BUFFER_H
#include <stdint.h>
#include <malloc.h>
#include <memory.h>

typedef  uint8_t* void_pointer;

class Buffer {
public:
    Buffer();
    void set(uint8_t* buff,uint32_t address, uint32_t length);
    uint8_t* get(uint32_t address, uint32_t length);
    void clear(void);    
    virtual ~Buffer();
private:
    void_pointer* _pointers;
    int _len;
};

#endif /* BUFFER_H */

