################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/usb/hw_config.c \
../src/usb/stm32_it.c \
../src/usb/usb_desc.c \
../src/usb/usb_endp.c \
../src/usb/usb_istr.c \
../src/usb/usb_main.c \
../src/usb/usb_prop.c \
../src/usb/usb_pwr.c 

OBJS += \
./src/usb/hw_config.o \
./src/usb/stm32_it.o \
./src/usb/usb_desc.o \
./src/usb/usb_endp.o \
./src/usb/usb_istr.o \
./src/usb/usb_main.o \
./src/usb/usb_prop.o \
./src/usb/usb_pwr.o 

C_DEPS += \
./src/usb/hw_config.d \
./src/usb/stm32_it.d \
./src/usb/usb_desc.d \
./src/usb/usb_endp.d \
./src/usb/usb_istr.d \
./src/usb/usb_main.d \
./src/usb/usb_prop.d \
./src/usb/usb_pwr.d 


# Each subdirectory must supply rules for building sources it contributes
src/usb/%.o: ../src/usb/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32F303xC -DUSE_STM32303C_EVAL -DUSE_FULL_ASSERT -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -D__GNUC__=8 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -std=gnu17 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


