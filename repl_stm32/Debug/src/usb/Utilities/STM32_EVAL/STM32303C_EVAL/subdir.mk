################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/usb/Utilities/STM32_EVAL/STM32303C_EVAL/stm32303c_eval.c 

OBJS += \
./src/usb/Utilities/STM32_EVAL/STM32303C_EVAL/stm32303c_eval.o 

C_DEPS += \
./src/usb/Utilities/STM32_EVAL/STM32303C_EVAL/stm32303c_eval.d 


# Each subdirectory must supply rules for building sources it contributes
src/usb/Utilities/STM32_EVAL/STM32303C_EVAL/%.o: ../src/usb/Utilities/STM32_EVAL/STM32303C_EVAL/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DSTM32F303xC -DUSE_STM32303C_EVAL -DUSE_FULL_ASSERT -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -D__GNUC__=8 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -std=gnu17 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


