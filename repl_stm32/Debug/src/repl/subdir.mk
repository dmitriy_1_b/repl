################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/repl/assembler.asm 

CPP_SRCS += \
../src/repl/breakPoints.cpp \
../src/repl/delay.cpp \
../src/repl/emulator.cpp \
../src/repl/memory.cpp \
../src/repl/repl.cpp \
../src/repl/step.cpp \
../src/repl/timer.cpp 

OBJS += \
./src/repl/assembler.o \
./src/repl/breakPoints.o \
./src/repl/delay.o \
./src/repl/emulator.o \
./src/repl/memory.o \
./src/repl/repl.o \
./src/repl/step.o \
./src/repl/timer.o 

ASM_DEPS += \
./src/repl/assembler.d 

CPP_DEPS += \
./src/repl/breakPoints.d \
./src/repl/delay.d \
./src/repl/emulator.d \
./src/repl/memory.d \
./src/repl/repl.d \
./src/repl/step.d \
./src/repl/timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/repl/%.o: ../src/repl/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross Assembler'
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -x assembler-with-cpp -DDEBUG -DSTM32F303xC -DUSE_STM32303C_EVAL -DUSE_FULL_ASSERT -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/repl/%.o: ../src/repl/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 -Og -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -fno-move-loop-invariants -Wall -Wextra  -g3 -DDEBUG -DUSE_FULL_ASSERT -DSTM32F30X -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -D__GNUC__=8 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f3-stdperiph" -std=gnu++17 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


