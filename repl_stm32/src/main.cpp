/*This file is provided under license: see license.txt*/
#include "main.h"

volatile uint32_t controlReg;

uint32_t seconds = 0;

extern uint32_t Delay;

volatile struct a {
	uint32_t a1;
	uint32_t a2;
	uint32_t a3;
	uint32_t a4;
	uint32_t a5;
	uint32_t a6;
	uint32_t a7;
	uint32_t a8;
	uint32_t a9;
	uint32_t a10;
	uint32_t a11;
	uint32_t a12;
	uint32_t a13;
	uint32_t a14;
	uint32_t a15;
	uint32_t a16;
	uint32_t a17;
	uint32_t a18;
	uint32_t a19;
	uint32_t a20;
	uint32_t a21;
	uint32_t a22;
	uint32_t a23;
	uint32_t a24;
	uint32_t a25;
	uint32_t a26;
	uint32_t a27;
	uint32_t a28;
	uint32_t a29;
	uint32_t a30;
	uint32_t a31;
	uint32_t a32;
	uint32_t a33;
	uint32_t a34;
	uint32_t a35;
	uint32_t a36;
	uint32_t a37;
	uint32_t a38;
	uint32_t a39;
	uint32_t a40;
} *point;

#define NOF  64
volatile uint32_t samples[NOF];
volatile float Fsamples[NOF];
float fZeroCurrent = 8.0;

static void ProcessSamples(void) {
	int i;
	for (i = 0; i < NOF; i++) {
		Fsamples[i] = samples[i] * 3.3 / 4096.0 - fZeroCurrent;
	}
}
#define CONTROL_SPSEL 1
extern  volatile bool replCallFromISR;
extern  volatile uint32_t replBStack,replDStack;

extern volatile void replThread(void);

volatile uint32_t reg11;

int main(int argc, char *argv[]) {

	//usb_main();
	clockInit();
//	volatile uint32_t FP_CTRL = *((uint32_t*)0xE0002000);
//	volatile uint32_t FP_REMAP = *((uint32_t*)0xE0002004);
//	volatile uint32_t FP_COMP0 = *((uint32_t*)0xE0002008);
//	volatile uint32_t FP_COMP1 = *((uint32_t*)0xE000200C);
//	volatile uint32_t FP_COMP2 = *((uint32_t*)0xE0002010);
//	volatile uint32_t FP_COMP3 = *((uint32_t*)0xE0002014);
//	volatile uint32_t FP_COMP4 = *((uint32_t*)0xE0002018);
//	volatile uint32_t FP_COMP5 = *((uint32_t*)0xE000201C);

	//FLASH->ACR&=~(1<<4);//PRFTEN
	//DBGMCU_APB1PeriphConfig(DBGMCU_TIM2_STOP,ENABLE);
	//DBGMCU_TIM2_STOP;
	//replThread();
	point = (struct a*) 0x20009F54;
//	if (__get_CONTROL() & (1 << CONTROL_SPSEL)) {
//			if (replCallFromISR) {
//				replDStack=__get_PSP();
//				__set_PSP(replBStack);
//			} else {
//				replBStack=__get_PSP();
//				__set_PSP(replDStack);
//			}
//		} else {
//			if (replCallFromISR) {
//				replDStack=__get_MSP();
//				__set_MSP(replBStack);
//			} else {
//				replBStack=__get_MSP();
//				__set_MSP(replDStack);
//			}
//		}
	//__ASM volatile ("VMOV %0, S0" : "=r" (reg11) );
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	NVIC_SetPriorityGrouping(5);//watch en.DM00046982.pdf Priority grouping
	modemReadResetByRepl();
	replInitProgram();
	replTimerInit();
	while (1) {
		replWorkFunction();
	}

//	SCB->CPACR |= ((3UL << 10 * 2) | (3UL << 11 * 2)); /* set CP10 and CP11 Full Access */
//	SCB->CCR |= SCB_CCR_STKALIGN_Msk;
//	controlReg = __get_CONTROL();
//	while (1) {
//	}
	// Infinite loop, never return.
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
