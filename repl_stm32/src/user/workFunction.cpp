/*
 * workFunction.cpp
 *
 * Created: 12.02.2019 21:43:23
 *  Author: root
 */
#include "workFunction.h"

void newF();

void doSomething();
inline void doSwitch(uint8_t var);

uint8_t var = 0;
uint8_t var2 = 69;
uint64_t var64 = 55;
uint64_t varB64 = 33;

TestClass::TestClass(uint8_t b) {
	a = 15;
	this->b = b + 15;
	++var;
}

TestClass::~TestClass() {
	a = 5;
	b = 5;
	--var;
}

void TestClass::setB(uint8_t newB) {
	b = 0x25;
	++var;
}

TestClass myClass(1); // __attribute__ ((section (".work_data")));
TestClass myClass2(2); // __attribute__ ((section (".work_data")));
uint8_t testData = 10; // __attribute__ ((section (".work_data")))=10;

void replWorkFunction(); //__attribute__ ((section (".work_function")));

extern uint32_t TestFunction;

void timer3Init(){
	NVIC_InitTypeDef nvicStructure;
		nvicStructure.NVIC_IRQChannel = TIM3_IRQn;
		nvicStructure.NVIC_IRQChannelPreemptionPriority = 1;//must be low than tim2, depend on SCB->AIRCR
		nvicStructure.NVIC_IRQChannelSubPriority = 1;//depend on SCB->AIRCR
		nvicStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&nvicStructure);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

		TIM_TimeBaseInitTypeDef timerInitStructure;
		timerInitStructure.TIM_Prescaler = 1;
		timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
		timerInitStructure.TIM_Period = 10000;
		timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
		timerInitStructure.TIM_RepetitionCounter = 0;
		TIM_TimeBaseInit(TIM3, &timerInitStructure);
		TIM_Cmd(TIM3, ENABLE);
		//NVIC_EnableIRQ (TIM2_IRQn);
		TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
}

extern "C" void TIM3_IRQHandler() {

	//TIM3->CR1 &= (uint16_t) ~TIM_CR1_CEN;
	//TIM3->CNT = 0;
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET) {
			TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		}
	//TIM3->CR1 |= TIM_CR1_CEN;
}

void replWorkFunction() {
	//delayMs(1000);
	//timer3Init();
	//setPinOut(PIN_LIMIT_SWITCH);
	//setPinOut(PIN_BUTTON_SEND);
//	void (*fun_ptr)(void) =(void (*)())&TestFunction;
//	(*fun_ptr)();
	while (1) {
		GPIO_SetBits(GPIOE,GPIO_Pin_11);
		delayMs(1);
		GPIO_ResetBits(GPIOE,GPIO_Pin_11);
	    delayMs(1);
		//setPin(PIN_BUTTON_SEND);
		//_delay_us(1);
		//doSomething();
		//clearPin(PIN_BUTTON_SEND);
		//replBreakPoint();
		//setPin(PIN_LIMIT_SWITCH);
		//_delay_us(1);
		//clearPin(PIN_LIMIT_SWITCH);
		/*doSomething();
		myClass2.setB(0xdd);
		if (myString == "Test String") {
			myString = "Test String2";
		} else {
			myString = F("&csq=");
		}
		TestClass myClass3(4);
		myClass3.setB(0x55);
		testData = 123;*/
	}
	//myClass = *new TestClass(3);
	//myClass.~TestClass();
	//clearPin(PIN_METER_RELAY2);
}

inline void doSwitch(uint8_t var) {
	switch (var) {
	case 1:
		doSomething();
	case 2:
		--var;
	case 3:
		testData = 68;
	case 5:
		newF();
	}
}

void notCallFunction() {
	++var;
	doSwitch(var);
}

void newF() {
	++var64;
	--varB64;
	--var;
	--var2;
}

void doSomething() {
	++var64;
	--varB64;
	--var;
	TestClass myClass4(5);
	myClass4.setB(0x55);
}

