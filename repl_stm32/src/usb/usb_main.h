#ifndef USB_MAIN_H_
#define USB_MAIN_H_

#ifdef __cplusplus
	extern "C" int usb_main(void);
#else
	int usb_main(void);
#endif

#endif
