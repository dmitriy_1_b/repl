/*This file is provided under license: see license.txt*/
/*
 * usbExportFunctions.cpp
 *
 * Created: 07-Apr-20 15:59:33
 *  Author: root
 */
#include "usbExportFunctions.h"

static uint16_t bytesAvalible;
static bool afterProcessing = true;
static bool process = false;
extern bool replUSBRunFlag;
extern bool replTimerPauseFlag;
extern uint8_t command;

//connect throw linker script to usbFunctionRead
uint8_t usbExportFunctionRead(uint8_t *data, uint8_t len) {
	//need to connect parts of response between
	//so at first call it read size of response
	//at second, third .. etc. calls read other parts of response
	//afterProcessing flag reloads after processing request at the usbExportFunctionWrite function
	//read zeros if response end
	if (afterProcessing) {
		afterProcessing = false;
		bytesAvalible = (uint16_t) replBytesAvalible();
	}
	for (uint8_t i = 0; i < len; ++i) {
		if (bytesAvalible != 0) {
			data[i] = replReadByte();
			--bytesAvalible;
		} else {
			data[i] = 0x00;
		}
	}
	#ifdef REPL_USE_USB
	if((bytesAvalible==0) && replUSBRunFlag &&
			(command==REPL_COMMAND_RUN || command==REPL_COMMAND_RUN_NOT_FROM_BREAKPOINT)) {
		replUSBRunFlag=false;
		replTimerPauseFlag=false;
		TIM2->ARR=3600; //need to complete usb transaction
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	}
	#endif
	return len;
}

//connect throw linker script to usbFunctionWrite
uint8_t usbExportFunctionWrite(uint8_t *data, uint8_t len) {
	for (uint8_t i = 0; i < len; ++i) {
		if (replBytesAwating() > 0) {
			if (!replSendByte(data[i])) {
				replReset();
			}
		} else {
			break;
		}
	}
	if ((replBytesAwating() == 0)/*&&(!afterProcessing)*/) {
		process = true;	//need to call not from interrupt
		/* return 1 if this was the last chunk */
		return 1;
	} else {
		return 0;
	}
}

void usbProcessing() {
	while (1) {
		if (process) {
			process = false;
			__disable_irq();
			if (!replProcessing()) {
				replReset();
			}
			__enable_irq();
			afterProcessing = true;	//watch usbExportFunctionRead
		}
	}
}

