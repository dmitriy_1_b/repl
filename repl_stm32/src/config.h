/*This file is provided under license: see license.txt*/
#ifndef CONFIG_H
#define CONFIG_H
#define REPL_USE_USB
//#define REPL_USE_GSM
#include <stdio.h>
//eclipse file parser patch
/*#ifndef __GNUC__
#define __GNUC__
#endif
#ifndef _UINT8_T_DECLARED
typedef unsigned char uint8_t;
#endif
#ifndef _UINT16_T_DECLARED
typedef unsigned short uint16_t;
#endif
#ifndef _UINT32_T_DECLARED
typedef unsigned  int uint32_t;
#endif
#ifndef _INT8_T_DECLARED
typedef signed char int8_t;
#endif
#ifndef _INT16_T_DECLARED
typedef signed short int16_t;
#endif
#ifndef _INT32_T_DECLARED
typedef signed  int int32_t;
#endif*/
#define AT_COMMAND_APN "AT+CSTT=\"internet\""
#define APN_ONLY "internet"
#define AT_COMMAND_CIPSTART "AT+CIPSTART=\"TCP\",\"your site\",\"80\""
#define WEBSITE "your site"
#define CONTROLLER_ID "564588878643469"//must be 15 symbols

#define F(string_literal) (string_literal)
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include "stm32f30x_rcc.h"
#include "stm32f30x_gpio.h"
#include "stm32f30x_exti.h"
#include "stm32f30x_misc.h"
#include "stm32f30x_comp.h"
#include "stm32f30x_tim.h"
#include "stm32f30x_dac.h"
#include "stm32f30x_usart.h"
#include "stm32f30x_rtc.h"
#include "stm32f30x_pwr.h"
#include "stm32f30x_dbgmcu.h"
#include "stm32f30x_flash.h"
#include "stm32f30x.h"
#include "clock.h"
#define boolean bool
#include "libs/Uart.h"
#include "usb/usb_main.h"
#include "usbExportFunctions.h"


/*my files*/
#include "repl/delay.h"
#include "modem.h"
#include "main.h"
#include "repl/repl.h"
#include "user/workFunction.h"
#include "repl/memory.h"
#include "repl/timer.h"
#include "repl/breakPoints.h"
#include "repl/step.h"
#include "repl/emulator.h"
#include "user/delay.h"
#define REPL_STEP_EXECUTE_BY_EMULATOR 1//stepi or step to one instruction forward can be made by using timer or by using  emulator
#define BAUDRATE_START    115200//19200//115200//57600
#define BAUDRATE_END    230400//230400//460800
#define ONE_BYTE_SEND_DELAY_US 41//10/19200*1000000us+1us (for guarantee)
#define MODEM_MAX_TURN_TIME_MS 6000
#define MODEM_SWITCH_TIME_MS 1500
#define MODEM_REBOOT_TIME_MS 10000
#define MODEM_COMMAND_MAX_TIME_MS 10000
#define MODEM_AT_MAX_TIMES 3
#define MODEM_TURN_ON_MAX_TIME_MS 30000
#define MODEM_SEND_DATA_MAX_TIME_MS 30000
#define MODEM_RECEIVE_FIRWARE_MAX_TIME_MS 5000
#define MODEM_TIME_BETWEEN_ESTABLISH_CONNECTIONS 5000
#define MODEM_MAX_NTP_ERROR 3
#define MODEM_MAX_PRINT_LENGTH 1800
#define REPL_BREAK_POINTS_COUNT 20//1 breakpoint = 6 bytes = 3 nop
#define REPL_BUFFER_SIZE 4104UL//used in writeflash


#endif
