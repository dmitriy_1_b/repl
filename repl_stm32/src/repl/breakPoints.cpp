/*This file is provided under license: see license.txt*/
#include "breakPoints.h"
#define REPL_BREAKPOINT_NULL_INDEX 0xff

extern uint8_t flashBuff[REPL_BUFFER_SIZE];

volatile uint32_t breakPointReturnPC;
volatile uint32_t stepPointReturnPC;
volatile uint32_t breakPointFirstInstructionPC;
extern volatile replStructRegisters *regs;
extern volatile uint16_t mainThreadTime;
extern uint32_t replInstructionTable;
//address 4 bytes instruction 2-4 bytes 2 bytes nop
//(some instruction can execute in one cycle with next)

bool replCheck32Bit(uint8_t b) {
	if (((b & 0b11100000) == 0b11100000) && ((b & 0b11111000) != 0b11100000)) {
		return true;
	} else {
		return false;
	}
}

void replSetBreakPoint(uint32_t address) {

	volatile uint32_t table = (uint32_t) &replInstructionTable;
	if (address == 0) {
		return;
	}
	address = address & 0xFFFFFFFE; //you cannot add breakpoint to odd address, sometimes gdb tries to do this
	uint8_t freeCell = REPL_BREAK_POINTS_COUNT;
	uint32_t a;
	for (int16_t i = REPL_BREAK_POINTS_COUNT - 1; i >= 0; --i) {
		uint8_t *pointer = (uint8_t*) &a;
		*pointer = replReadFlash(table + i * 10);
		*(pointer + 1) = replReadFlash(table + i * 10 + 1);
		*(pointer + 2) = replReadFlash(table + i * 10 + 2);
		*(pointer + 3) = replReadFlash(table + i * 10 + 3);
		if (a == 0x00) {
			freeCell = i;
		} else if (a == address) {
			replSetFlag(REPL_IS_BREAKPOINT_SET, true);
			return;
		}
	}
	if (freeCell == REPL_BREAK_POINTS_COUNT) {
		return;
	}
	uint8_t *pointer = (uint8_t*) &address;
	flashBuff[0] = *pointer;
	flashBuff[1] = *(pointer + 1);
	flashBuff[2] = *(pointer + 2);
	flashBuff[3] = *(pointer + 3);
	flashBuff[4] = replReadFlash(address);
	flashBuff[5] = replReadFlash(address + 1);
	if (replCheck32Bit(flashBuff[5])) {
		flashBuff[6] = replReadFlash(address + 2);
		flashBuff[7] = replReadFlash(address + 3);
		//0xFEE7 rjmp PC+0
		flashBuff[8] = 0xfe;
		flashBuff[9] = 0xe7;
		replWriteFlash((uint8_t*) flashBuff, table + freeCell * 10,
				table + freeCell * 10 + 9);
	} else {
		//0xFEE7 rjmp PC+0
		flashBuff[6] = 0xfe;
		flashBuff[7] = 0xe7;
		replWriteFlash((uint8_t*) flashBuff, table + freeCell * 10,
				table + freeCell * 10 + 7);
	}

	//0xFEE7 rjmp PC+0
	flashBuff[0] = 0xfe;
	flashBuff[1] = 0xe7;
	replWriteFlash((uint8_t*) flashBuff, address, address + 1);
	replSetFlag(REPL_IS_BREAKPOINT_SET, true);
}

uint8_t replBreakPointAddressToIndex(uint32_t address) {
	address = address & 0xFFFFFFFE;
	if (address == regs->pc) {
		replSetFlag(REPL_IS_BREAK, false);
	}
	volatile uint32_t table = (uint32_t) &replInstructionTable;
	if (address == 0) {
		return REPL_BREAKPOINT_NULL_INDEX;
	}
	uint32_t a;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
		uint8_t *pointer = (uint8_t*) &a;
		*pointer = replReadFlash(table + i * 10);
		*(pointer + 1) = replReadFlash(table + i * 10 + 1);
		*(pointer + 2) = replReadFlash(table + i * 10 + 2);
		*(pointer + 3) = replReadFlash(table + i * 10 + 3);
		if (a == address) {
			return i;
		}
	}
}

void replDeleteBreakPointByIndex(uint8_t i) {
	uint32_t address;
	volatile uint32_t table = (uint32_t) &replInstructionTable;
	uint8_t *pointer = (uint8_t*) &address;
	*pointer = replReadFlash(table + i * 10);
	*(pointer + 1) = replReadFlash(table + i * 10 + 1);
	*(pointer + 2) = replReadFlash(table + i * 10 + 2);
	*(pointer + 3) = replReadFlash(table + i * 10 + 3);
	flashBuff[0] = replReadFlash(table + i * 10 + 4);
	flashBuff[1] = replReadFlash(table + i * 10 + 5);
	if (replCheck32Bit(flashBuff[1])) {
		flashBuff[2] = replReadFlash(table + i * 10 + 6);
		flashBuff[3] = replReadFlash(table + i * 10 + 7);
		replWriteFlash((uint8_t*) flashBuff, address, address + 3);
	} else {
		replWriteFlash((uint8_t*) flashBuff, address, address + 1);
	}
	flashBuff[0] = 0x00;
	flashBuff[1] = 0x00;
	flashBuff[2] = 0x00;
	flashBuff[3] = 0x00;
	replWriteFlash((uint8_t*) flashBuff, table + i * 10, table + i * 10 + 3);
	return;
}

void replDeleteBreakPoints(uint8_t *buff) {
	uint8_t breakPoints[20];
	uint8_t count = buff[0];
	++buff;
	for (uint8_t i = 0; i < count; ++i) {
		breakPoints[i] = replBreakPointAddressToIndex(
				replReadUint32(buff + i * 4));
	}
	for (uint8_t i = 0; i < count; ++i) {
		replDeleteBreakPointByIndex(breakPoints[i]);
	}
}

void replDeleteAllBreakPoints(void) {
	volatile uint32_t table = (uint32_t) &replInstructionTable;
	volatile uint32_t address;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) {
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadFlash(table + i * 10);
		*(pointer + 1) = replReadFlash(table + i * 10 + 1);
		*(pointer + 2) = replReadFlash(table + i * 10 + 2);
		*(pointer + 3) = replReadFlash(table + i * 10 + 3);
		if (address == regs->pc) {
			replSetFlag(REPL_IS_BREAK, false);
		}
		if (address != 0x00) {
			flashBuff[0] = replReadFlash(table + i * 10 + 4);
			flashBuff[1] = replReadFlash(table + i * 10 + 5);
			if (replCheck32Bit(flashBuff[1])) {
				flashBuff[2] = replReadFlash(table + i * 10 + 6);
				flashBuff[3] = replReadFlash(table + i * 10 + 7);
				replWriteFlash((uint8_t*) flashBuff, address, address + 3);
			} else {
				replWriteFlash((uint8_t*) flashBuff, address, address + 1);
			}
			flashBuff[0] = 0x00;
			flashBuff[1] = 0x00;
			flashBuff[2] = 0x00;
			flashBuff[3] = 0x00;
			replWriteFlash((uint8_t*) flashBuff, table + i * 10,
					table + i * 10 + 3);
		}
	}
}

bool replCheckBreakPoint(void) {
	volatile uint32_t table = (uint32_t) &replInstructionTable;
	volatile uint32_t address;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) { //check if breakpoint function call instruction made by repl not by user like replBreakPoint();
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadFlash(table + i * 10);
		*(pointer + 1) = replReadFlash(table + i * 10 + 1);
		*(pointer + 2) = replReadFlash(table + i * 10 + 2);
		*(pointer + 3) = replReadFlash(table + i * 10 + 3);
		if (regs->pc == address) {
			return true;
		}
	}
	return false;
}

void replBreakPointExecute(void) {
	volatile uint32_t table = (uint32_t) &replInstructionTable;
	volatile uint32_t address;
	for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) { //check if breakpoint function call instruction made by repl not by user like replBreakPoint();
		uint8_t *pointer = (uint8_t*) &address;
		*pointer = replReadFlash(table + i * 10);
		*(pointer + 1) = replReadFlash(table + i * 10 + 1);
		*(pointer + 2) = replReadFlash(table + i * 10 + 2);
		*(pointer + 3) = replReadFlash(table + i * 10 + 3);
		if (regs->pc == address) {
			if (!replEmulate((table + i * 10 + 4))) {
				//mainThreadTime = 1;
				//TIM2->ARR = 1;//reduce debug flow time
				if (replCheck32Bit(replReadFlash(table + i * 10 + 5))) {
					breakPointReturnPC = regs->pc + 4;
				} else {
					breakPointReturnPC = regs->pc + 2;
				}
				regs->pc = (table + i * 10 + 4);
				breakPointFirstInstructionPC = regs->pc;
				replSetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION, true);
			} else {
				replSetFlag(REPL_IS_BREAK, false);
				if (replGetFlag(REPL_IS_STEP_CALL_AFTER_BREAK)) {
					replSetFlag(REPL_IS_STEP, true);
					replSetFlag(REPL_IS_STEP_CALL_AFTER_BREAK, false);
					replTimerPause();
				}
			}
			return;
		}
	}
}

uint8_t replReadMemWithBreakPointCheck(uint32_t pointer) {
	uint8_t outM = replReadFlash(pointer - 1);
	uint8_t out = replReadFlash(pointer);
	uint8_t outP = replReadFlash(pointer + 1);
	uint32_t breakPointAddress = 0;
	if ((out == 0xfe) && (outP == 0xe7) && ((pointer & 0x01) == 0)) {
		breakPointAddress = pointer;
	}
	if ((out == 0xe7) && (outM == 0xfe) && ((pointer & 0x01) == 1)) {
		breakPointAddress = pointer - 1;
	}
	if (breakPointAddress != 0) {
		volatile uint32_t address;
		volatile uint32_t table = (uint32_t) &replInstructionTable;
		for (uint8_t i = 0; i < REPL_BREAK_POINTS_COUNT; ++i) { //check if breakpoint function call instruction made by repl not by user like replBreakPoint();
			uint8_t *p = (uint8_t*) &address;
			*p = replReadFlash(table + i * 10);
			*(p + 1) = replReadFlash(table + i * 10 + 1);
			*(p + 2) = replReadFlash(table + i * 10 + 2);
			*(p + 3) = replReadFlash(table + i * 10 + 3);
			if (breakPointAddress == address) {
				if ((pointer & 0x01) == 0) {
					out = replReadFlash(table + i * 10 + 4);
				} else {
					out = replReadFlash(table + i * 10 + 5);
				}
				return out;
			}
		}
	}
	return out; //pgm_read_byte(pointer);
}

////uses for 4 bytes breakpoints
//void replBreakPoint(void){
//cli();
//TCCR1B=0;
//OCR1A=REPL_DEFAULT_PROCESSOR_CYCLES;
//TCNT1=REPL_DEFAULT_PROCESSOR_CYCLES-1;
//TCCR1B=1<<CS10;
//sei();
//while(1);
//}
