.global Delay
.global TestFunction
				.syntax unified
DelayMy:
				PUSH	{R0, R2, LR}
DELAY1:
				cbnz     R2,DELAY2@ to test emulator
				LDR		R2, =40000
DELAY2:			SUBS	R2, #1
				BNE		DELAY2
				SUBS	R0, R0, #1
				BNE		DELAY1
				@MOV R1, #100
				LDR R1, =0x3FDD70A4 @ 1.73
    			VMOV S2, R1
    			@VCVT.F32.U32 S2,S2
    			@MOV R1, #98
    			LDR R1, =0x403D70A4 @ 2.96
    			VMOV S3, R1
    			@VCVT.F32.U32 S3,S3
    			VDIV.F32 S1, S2, S3
    			VMOV R1, S1 @ R1 = 0x3F159F23  ~0.584459
				POP		{R0, R2, PC}






TestFunction:
TBH [R7, R8, LSL #1]
@ITTEE   EQ        @ IT can be omitted
@ADDEQ r1,r1,#1    @ 16-bit AND, not ANDS
@ADDEQ r2,r2,#1    @ 32-bit ADDS (16-bit ADDS does not set flags in
@ADDNE r3,r3,#1     @ 16-bit MOV
@BNE TestFunction @ADDEQ r4,r4,#1     @ 16-bit MOV
b.n TestFunction


.extern __replTimerISR__
.extern __replThread__
.extern replBStack
.extern replDStack
.extern replCallFromISR
.extern replFirstStart
.extern replInterruptLr
.extern replIPSR

.global TIM2_IRQHandler

TIM2_IRQHandler1:
					push    {r4-r7,lr}
					MOV 	R0,R8
					MOV 	R1,R9
					MOV 	R2,R10
					MOV 	R3,R11
					push 	{r0-r3}
@if (__get_CONTROL() & (1 << CONTROL_SPSEL)) {
@		if (replCallFromISR) {
@			__set_PSP(replBStack);
@		} else {
@			__set_PSP(replDStack);
@		}
@	} else {
@		if (replCallFromISR) {
@			__set_MSP(replBStack);
@		} else {
@			__set_MSP(replDStack);
@		}
@	}
                   	mrs     r3, CONTROL
                   	tst.w   r3, #2@CONTROL SPSEL,
								  @0: MSP is the current stack pointer
								  @1: PSP is the current stack pointer
                   	beq.n   MSP_Stack
PSP_Stack:  	   	ldr     r3, =replCallFromISR
                   	ldrb    r3, [r3, #0]
                   	cbz     r3, label1
                   	ldr     r3, =replBStack
                   	ldr     r3, [r3, #0]
                   	mrs		r1, PSP
                    ldr     r2, =replDStack
					str     r1, [r2, #0]
                   	msr     PSP, r3
                   	b.n     END_StackManipulatuon
label1:            	ldr     r3, =replDStack
                   	ldr     r3, [r3, #0]
                   	mrs		r1, PSP
                    ldr     r2, =replBStack
					str     r1, [r2, #0]
                   	msr     PSP, r3
                   	b.n     END_StackManipulatuon
MSP_Stack:         	ldr     r3, =replCallFromISR
                   	ldrb    r3, [r3, #0]
                   	cbz     r3, label2
                   	ldr     r3, =replBStack
                   	ldr     r3, [r3, #0]
                   	mrs		r1, MSP
                    ldr     r2, =replDStack
					str     r1, [r2, #0]
                   	msr     MSP, r3
                   	b.n     END_StackManipulatuon
label2:            	ldr     r3, =replDStack
                   	ldr     r3, [r3, #0]
                   	mrs		r1, MSP
                    ldr     r2, =replBStack
					str     r1, [r2, #0]
                   	msr     MSP, r3
END_StackManipulatuon:	BL __replTimerISR__
					ldr     r3, =replFirstStart
					ldrb    r2, [r3, #0]
                   	cbnz    r2, firstCall
					pop 	{r0-r3}
					MOV 	R8,R0
					MOV 	R9,R1
					MOV 	R10,R2
					MOV 	R11,R3
					pop     {r4-r7,pc}
firstCall:			EOR 	r2,r2// load zero
					strb    r2, [r3, #0]
					ldr     r2, =replDStack
                   	ldr     r2, [r2, #0]
					mrs     r3, CONTROL
                   	tst.w   r3, #2@CONTROL SPSEL,
								  @0: MSP is the current stack pointer
								  @1: PSP is the current stack pointer
                   	beq.n   _MSP_Stack
_PSP_Stack:         msr     PSP, r2
					b.n     _END_StackManipulatuon
_MSP_Stack:         msr     MSP, r2
_END_StackManipulatuon:
					ldr 	r3,=replIPSR@xPSR
					ldr     r3, [r3, #0]
					push 	{r3}@xPSR
					ldr 	r3,=__replThread__
					push 	{r3}@pc
					push 	{r3}@lr
					EOR 	r3,r3// load zero
					push 	{r3}@r0-r3,r12
					push 	{r3}
					push 	{r3}
					push 	{r3}
					push 	{r3}
					ldr 	r3,=replInterruptLr
					ldr 	r3, [r3, #0]
					mov     lr,r3
					push 	{lr}
					pop		{pc}
					@mov 	pc,r3


.set	TIM2_IRQHandler, TIM2_IRQHandler1+1@

.global replInstructionTable@ 20 breakpoints, 1 breakpoint = 10 bytes = 4 - address, (2 or 4) instruction,2 - branch instruction PC+0

		replInstructionTable:
		.space 0xc8

				.syntax divided
