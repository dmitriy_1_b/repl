/*This file is provided under license: see license.txt*/
/*
 * emulator.cpp
 *
 *  Created on: Feb 17, 2020
 *      Author: root
 */

#include "emulator.h"

extern volatile replStructRegisters *regs;
extern volatile uint32_t replInterrupts;
extern volatile uint32_t replBStack;

volatile uint32_t instruction;

volatile uint32_t stackCopy;

static void stackChangeApply() {
	uint32_t oldStack = replBStack + 17 * 4;
	if (stackCopy == oldStack)
		return;
	//pop
	if (stackCopy > oldStack) {
		uint8_t *p;
		for (int32_t i = 0; i < 17 * 4; ++i) {
			p = (uint8_t*) (stackCopy - i);
			*p = *((uint8_t*) (oldStack - i));
		}
	} else { //push
		uint8_t *p;
		for (int32_t i = 17 * 4 - 1; i >= 0; --i) {
			p = (uint8_t*) (stackCopy - i);
			*p = *((uint8_t*) (oldStack - i));
		}
	}
	replBStack = stackCopy - 17 * 4;
	regs = (volatile replStructRegisters*) (replBStack);

}

static volatile uint32_t* numberToRegister(uint8_t n) {
	switch (n) {
	case 0:
		return &regs->r0;
	case 1:
		return &regs->r1;
	case 2:
		return &regs->r2;
	case 3:
		return &regs->r3;
	case 4:
		return &regs->r4;
	case 5:
		return &regs->r5;
	case 6:
		return &regs->r6;
	case 7:
		return &regs->r7;
	case 8:
		return &regs->r8;
	case 9:
		return &regs->r9;
	case 10:
		return &regs->r10;
	case 11:
		return &regs->r11;
	case 12:
		return &regs->r12;
	case 13:
		return &stackCopy;
	case 14:
		return &regs->lr;
	case 15:
		return &regs->pc;
	}
	abort();
}

static uint32_t align(uint32_t x, uint8_t y) {
	return y * (x / y);
}

static bool conditionHolds(uint8_t cond) {
	bool result;
	// Evaluate base condition.
	switch (cond >> 1) {
	case 0b000:
		result = (regs->psr >> 30) & 0x01;
		break; //(APSR.Z == '1'); // EQ or NE
	case 0b001:
		result = (regs->psr >> 29) & 0x01;
		break; //(APSR.C == '1'); // CS or CC
	case 0b010:
		result = (regs->psr >> 31) & 0x01;
		break; //(APSR.N == '1'); // MI or PL
	case 0b011:
		result = (regs->psr >> 28) & 0x01;
		break; //(APSR.V == '1'); // VS or VC
	case 0b100:
		result = ((regs->psr >> 29) & 0x01)
				&& ((regs->psr >> 30) & 0x01 == 0x00);
		break; //(APSR.C == '1') && (APSR.Z == '0'); // HI or LS
	case 0b101:
		result = (regs->psr >> 31) & 0x01 == (regs->psr >> 28) & 0x01;
		break; //(APSR.N == APSR.V); // GE or LT
	case 0b110:
		result = ((regs->psr >> 31) & 0x01 == (regs->psr >> 28) & 0x01)
				&& ((regs->psr >> 30) & 0x01 == 0x00);
		break; //(APSR.N == APSR.V) && (APSR.Z == '0'); // GT or LE
	case 0b111:
		result = true; // AL
	}
	// Condition flag values in the set '111x' indicate the instruction is always executed.
	// Otherwise, invert condition if necessary.
	if ((cond & 0x01) == 0x01 && cond != 0b1111) {
		result = !result;
	}
	return result;
}

static bool conditionPass() {
	volatile uint8_t itCond;
	volatile uint8_t itState = (((regs->psr >> 10) & 0x3ff) << 2)
			+ ((regs->psr >> 25) & 0x3);
	if (itState != 0x00) {
		itCond = (itState & 0xF0) >> 4;
		itState = (itState & 0xe0) + ((itState << 1) & 0x1f);
		if ((itState & 0x0f) == 0x00) {	 //last instruction
			itState = 0x00;
		}
		regs->psr = (regs->psr & 0xF9FF03FF)
				+ (((uint32_t) itState << 25) & 0x6000000)
				+ (((uint32_t) itState << (10 - 2)) & 0xFC00);
		return conditionHolds(itCond);
	} else {
		return true;
	}
}

static uint8_t bitCount(uint32_t value) {
	uint8_t count = 0;
	for (uint8_t i = 0; i < 32; ++i) {
		count += (value & (1 << i)) >> i;
	}
	return count;
}

bool replInstructionIs32bit(){
	instruction = *((uint32_t*) (regs->pc));
	if (((instruction & 0xf800) == 0xf800) || ((instruction & 0xf800) == 0xE800)
				|| ((instruction & 0xf800) == 0xf000)) {
		return true;
	}else{
		return false;
	}
}

bool replEmulate(uint32_t instructionPc) {
//	if (regs->pc != 0x8001c9a)
//		return false;
	stackCopy = replBStack + 17 * 4;
	instruction = *((uint32_t*) (instructionPc));
	volatile uint32_t *rd;
	volatile uint32_t imm;
	volatile int32_t signImm;
	volatile uint8_t cond;
	volatile uint8_t op;
	if (((instruction & 0xf800) == 0xf800) || ((instruction & 0xf800) == 0xE800)
			|| ((instruction & 0xf800) == 0xf000)) {
		regs->pc += 4;
		instruction = (instruction >> 16) + (instruction << 16);
		//32 bit instruction
		//ADR T2
		if ((instruction & 0xFBFF8000) == 0xF2AF0000) {
			if (!conditionPass())
				return true;
			imm = (((instruction >> 26) & 0x01) << 11)
					+ (((instruction >> 12) & 0x07) << 8)
					+ (instruction & 0xff);
			rd = numberToRegister((uint8_t)((instruction >> 8) & 0x0F));
			*rd = align(regs->pc, 4) - imm;
			return true;
		}
		//ADR T3
		if ((instruction & 0xFBFF8000) == 0xF20F0000) {
			if (!conditionPass())
				return true;
			imm = (((instruction >> 26) & 0x01) << 11)
					+ (((instruction >> 12) & 0x07) << 8)
					+ (instruction & 0xff);
			rd = numberToRegister((uint8_t)((instruction >> 8) & 0x0F));
			*rd = align(regs->pc, 4) + imm;
			return true;
		}
		//B T3
		if ((instruction & 0xF800D000) == 0xF0008000) {
			cond = (instruction >> 22) & 0x0f;
			if (!conditionHolds(cond))
				return true;
			signImm = (((instruction >> 26) & 0x01) << 19)
					+ (((instruction >> 11) & 0x01) << 18)
					+ (((instruction >> 13) & 0x01) << 17)
					+ (((instruction >> 16) & 0x3f) << 11)
					+ (instruction & 0x7ff);
			if (signImm & 0x80000) {
				signImm = -(0x100000 - signImm);
			}
			signImm = (signImm << 1);
			regs->pc = regs->pc + signImm;
			return true;
		}
		//B T4
		if ((instruction & 0xF800D000) == 0xF0009000) {
			if (!conditionPass())
				return true;
			signImm = (((instruction >> 26) & 0x01) << 23)
					+ (((instruction >> 13) & 0x01) << 22)
					+ (((instruction >> 11) & 0x01) << 21)
					+ (((instruction >> 16) & 0x3ff) << 11)
					+ (instruction & 0x7ff);
			if (signImm & 0x800000) {
				signImm = -(0x1000000 - signImm);
			} else {
				signImm = signImm ^ ((1 << 22) | (1 << 21));
			}
			signImm = (signImm << 1);
			regs->pc = regs->pc + signImm;
			return true;
		}
		//BL T1
		if ((instruction & 0xF800D000) == 0xF000D000) {
			if (!conditionPass())
				return true;
			signImm = (((instruction >> 26) & 0x01) << 23)
					+ (((instruction >> 13) & 0x01) << 22)
					+ (((instruction >> 11) & 0x01) << 21)
					+ (((instruction >> 16) & 0x3ff) << 11)
					+ (instruction & 0x7ff);
			if (signImm & 0x800000) {
				signImm = -(0x1000000 - signImm);
			} else {
				signImm = signImm ^ ((1 << 22) | (1 << 21));
			}
			signImm = (signImm << 1);
			regs->lr = regs->pc | 0x01;
			regs->pc = regs->pc + signImm;
			return true;
		}
		//LDM T2 splimit not checked
		if ((instruction & 0xFFD02000) == 0xE8900000) {
			if (!conditionPass())
				return true;
			uint16_t registers = (uint16_t)(instruction & 0x0000FFFF);
			uint8_t regNumber = (uint8_t)((instruction >> 16) & 0x0F);
			rd = numberToRegister(regNumber);
			bool wback = (instruction & 0x200000) >> (16 + 5);
			uint32_t address = *rd;
			for (uint8_t i = 0; i < 16; ++i) {
				if (registers & (1 << i)) {
					volatile uint32_t *reg = numberToRegister(i);
					*reg = *((uint32_t*) (address));
					address += 4;
				}
			}
			if (registers & (1 << 15)) {
				regs->pc = regs->pc & 0xfffffffe;
			}
			if ((!(registers & (1 << regNumber))) && wback) {
				*rd = address;
			}
			stackChangeApply();
			return true;
		}
		//LDMDB T1 splimit not checked
		if ((instruction & 0xFFD02000) == 0xE9100000) {
			if (!conditionPass())
				return true;
			uint16_t registers = (uint16_t)(instruction & 0x0000FFFF);
			uint8_t regNumber = (uint8_t)((instruction >> 16) & 0x0F);
			rd = numberToRegister(regNumber);
			bool wback = (instruction & 0x200000) >> (16 + 5);
			uint32_t address = *rd - 4 * bitCount(registers);
			for (uint8_t i = 0; i < 16; ++i) {
				if (registers & (1 << i)) {
					volatile uint32_t *reg = numberToRegister(i);
					*reg = *((uint32_t*) (address));
					address += 4;
				}
			}
			if (registers & (1 << 15)) {
				regs->pc = regs->pc & 0xfffffffe;
			}
			if ((!(registers & (1 << regNumber))) && wback) {
				*rd = *rd - 4 * bitCount(registers);
			}
			stackChangeApply();
			return true;
		}
		//LDR (immediate) T3,T4,LDR (literal) T2 (B,SB,H,SH,T postfix)
		if ((instruction & 0xFE100000) == 0xF8100000) {
			if (!conditionPass())
				return true;
			uint8_t n = (uint8_t)((instruction >> 16) & 0x0F);
			bool add = (instruction & (1 << (7 + 16))) >> (7 + 16);
			uint8_t t = (uint8_t)((instruction >> 12) & 0x0F);
			volatile uint32_t *rn = numberToRegister(n);
			volatile uint32_t *rt = numberToRegister(t);
			uint8_t size = (instruction>>(5+16))&0x03;
			bool sign = (instruction>>(8+16))&0x01;
			uint32_t address;
			if (n != 15) {
				bool index;
				bool wback;
				if(instruction&(1<<11)){
					//LDR (immediate) T3
					if (add == true) {
						imm = instruction & 0x0FFF;
						index = true;
						wback = false;
					} else {//LDR (immediate) T4
						imm = instruction & 0xFF;
						add = (instruction & (1 << 9)) >> 9;
						index = (instruction & (1 << 10)) >> 10;
						wback = (instruction & (1 << 8)) >> 8;
					}
					uint32_t offsetAddr;
					if (add) {
						offsetAddr = *rn + imm;
					} else {
						offsetAddr = *rn - imm;
					}
					if (index) {
						address = offsetAddr;
					} else {
						address = *rn;
					}
					if (wback) {
						*rn = offsetAddr;
					}
				}else{
					//LDR (register) T2
					uint8_t m = (uint8_t)(instruction & 0x0F);
					volatile uint32_t *rm = numberToRegister(m);
					imm=(instruction>>4)&0x03;
//					if((imm>0)&&(*rm&(1<<(32-imm))))
//						regs->psr=regs->psr|(1<<29);// set carry flag
//					else
//						regs->psr=regs->psr&(~(1<<29));//reset carry flag
					address=*rn+(*rm<<imm);
				}
				//LDR (literal) T2
			} else {
				imm = instruction & 0x0FFF;
				uint32_t base = align(regs->pc, 4);
				if (add) {
					address = base + imm;
				} else {
					address = base - imm;
				}
			}
			if(!sign){
				if(size==2){
					*rt = *((uint32_t*) address);
				}else if(size==1){
					*rt = *((uint16_t*) address);
				}else if(size==0){
					*rt = *((uint8_t*) address);
				}
			}else{
				if(size==1){
					*rt = *((int16_t*) address);
				}else if(size==0){
					*rt = *((int8_t*) address);
				}
			}
			stackChangeApply();
			return true;
		}
		//LDRD//unalign not checked
		if ((instruction & 0xFE500000) == 0xE8500000) {
			bool wback = (instruction & (1 << (5 + 16))) >> (5 + 16);
			bool index = (instruction & (1 << (8 + 16))) >> (8 + 16);
			bool add = (instruction & (1 << (7 + 16))) >> (7 + 16);
			uint8_t n = (uint8_t)((instruction >> 16) & 0x0F);
			uint8_t t = (uint8_t)((instruction >> 12) & 0x0F);
			uint8_t t2 = (uint8_t)((instruction >> 8) & 0x0F);
			volatile uint32_t *rn = numberToRegister(n);
			volatile uint32_t *rt = numberToRegister(t);
			volatile uint32_t *rt2 = numberToRegister(t2);
			uint32_t address;
			imm=(instruction&0xff)<<2;
			if(n!=15){//LDRD(immediate)
				if(!((wback==0) && (index==0))){
					if (!conditionPass())
						return true;
				uint32_t offsetAddr;
				if (add) {
					offsetAddr = *rn + imm;
				} else {
					offsetAddr = *rn - imm;
				}
				if (index) {
					address = offsetAddr;
				} else {
					address = *rn;
				}
				if (wback) {
					*rn = offsetAddr;
				}
				*rt = *((uint32_t*) address);
				*rt2 = *((uint32_t*) (address+4));
				stackChangeApply();
				return true;
				}
			}else{
				//LDR literal
				if(!(((wback==0) && (index==0))||((wback==1) && (index==1) && (add==0)))){
					if (!conditionPass())
						return true;
					if (add) {
						address = *rn + imm;
					} else {
						address = *rn - imm;
					}
					*rt = *((uint32_t*) address);
					*rt2 = *((uint32_t*) (address+4));
					stackChangeApply();
					return true;
				}
			}
		}//LDREX
		if ((instruction & 0xFFF00F00) == 0xE8500F00) {
			if (!conditionPass())
				return true;
			uint8_t n = (uint8_t)((instruction >> 16) & 0x0F);
			uint8_t t = (uint8_t)((instruction >> 12) & 0x0F);
			volatile uint32_t *rn = numberToRegister(n);
			volatile uint32_t *rt = numberToRegister(t);
			imm=(instruction&0xff)<<2;
			uint32_t address=*rn+imm;
			*rt = *((uint32_t*) address);
			//stackChangeApply();
			return true;
		}
		//LDREXH,LDREXB
		if ((instruction & 0xFFF00FEF) == 0xE8D00F4F) {
			if (!conditionPass())
				return true;
			bool halfWord=(instruction & (1 << 4)) >> 4;
			uint8_t n = (uint8_t)((instruction >> 16) & 0x0F);
			uint8_t t = (uint8_t)((instruction >> 12) & 0x0F);
			volatile uint32_t *rn = numberToRegister(n);
			volatile uint32_t *rt = numberToRegister(t);
			uint32_t address=*rn;
			if(halfWord)
				*rt = *((uint16_t*) address);
			else
				*rt = *((uint8_t*) address);
			//stackChangeApply();
			return true;
		}
		// TBB, TBH
		if ((instruction & 0xFFF0FFE0) == 0xE8D0F000) {
			if (!conditionPass())
				return true;
			bool isTbh = (instruction & (1 << 4)) >> 4;
			uint8_t n = (uint8_t)((instruction >> 16) & 0x0F);
			uint8_t m = (uint8_t)(instruction  & 0x0F);
			volatile uint32_t *rn = numberToRegister(n);
			volatile uint32_t *rm = numberToRegister(m);
			uint16_t halfwords;
			if(isTbh){
				halfwords = *((uint16_t*) (*rn+(*rm<<1)));
			}else{
				halfwords = *((uint8_t*) (*rn+*rm));
			}
			regs->pc = regs->pc+2*halfwords;
			regs->pc = regs->pc & 0xfffffffe;
			return true;
		}
		regs->pc-= 4;
	} else {
		regs->pc += 2;
		instruction = instruction & 0xFFFF;
		//16 bit instruction
		//ADR
		if ((instruction & 0xf800) == 0xA000) {
			if (!conditionPass())
				return true;
			rd = numberToRegister((uint8_t)((instruction >> 8) & 0x07));
			imm = (instruction & 0xff) << 2;
			*rd = align(regs->pc + 2, 4) + imm;
			return true;
		}
		//B T1
		if ((instruction & 0xf000) == 0xD000) {
			cond = (instruction >> 8) & 0x0f;
			if (!conditionHolds(cond))
				return true;
			signImm = instruction & 0xff;
			if (signImm & 0x80) {
				signImm = -(0x100 - signImm);
			}
			signImm = (signImm << 1);
			regs->pc = regs->pc + signImm + 2;
			return true;
		}
		//B T2
		if ((instruction & 0xf800) == 0xE000) {
			if (!conditionPass())
				return true;
			signImm = instruction & 0x7ff;
			if (signImm & 0x400) {
				signImm = -(0x800 - signImm);
			}
			signImm = (signImm << 1);
			regs->pc = regs->pc + signImm + 2;
			return true;
		}
		//BLX T1 without ns bit
		if ((instruction & 0xff80) == 0x4780) {
			if (!conditionPass())
				return true;
			rd = numberToRegister((uint8_t)((instruction >> 3) & 0x0F));
			regs->lr = regs->pc | 0x01;
			regs->pc = (*rd) & 0xfffffffe;
			return true;
		}
		//BX T1 without ns bit
		if ((instruction & 0xff80) == 0x4700) {
			if (!conditionPass())
				return true;
			rd = numberToRegister((uint8_t)((instruction >> 3) & 0x0F));
			regs->pc = (*rd) & 0xfffffffe;
			return true;
		}
		//CBNZ, CBZ T1
		if ((instruction & 0xf500) == 0xB100) {
			rd = numberToRegister((uint8_t)(instruction & 0x07));
			imm = +(((instruction >> 9) & 0x01) << 6)
					+ (((instruction >> 3) & 0x1f) << 1);
			op = (instruction >> 11) & 0x01;
			if (((op == 0) && (*rd == 0)) || ((op == 1) && (*rd != 0))) {
				regs->pc = regs->pc + imm + 2;
			}
			return true;
		}
		//IT T1
		if (((instruction & 0xFF00) == 0xBF00)
				&& ((instruction & 0x000F) != 0x00)) {
			regs->psr = (regs->psr & 0xF9FF03FF)
					+ (((instruction & 0x00FF) << 25) & 0x6000000)
					+ (((instruction & 0x00FF) << (10 - 2)) & 0xFC00);
			return true;
		}
		//LDM T3/POP.n splimit not checked
		if ((instruction & 0xFE00) == 0xBC00) {
			if (!conditionPass())
				return true;
			uint16_t registers = (uint16_t)(instruction & 0x00FF)
					+ (((instruction >> 8) & 0x01) << 15);
			uint8_t regNumber = 13;
			rd = numberToRegister(regNumber);
			uint32_t address = *rd;
			for (uint8_t i = 0; i < 16; ++i) {
				if (registers & (1 << i)) {
					volatile uint32_t *reg = numberToRegister(i);
					*reg = *((uint32_t*) (address));
					address += 4;
				}
			}
			if (registers & (1 << 15)) {
				regs->pc = regs->pc & 0xfffffffe;
			}
			if (!(registers & (1 << regNumber))) {
				*rd = address;
			}
			stackChangeApply();
			return true;
		}
//		//LDR (literal)
		if ((instruction & 0xF800) == 0x4800) {
			if (!conditionPass())
				return true;
			uint8_t t = (uint8_t)((instruction >> 8) & 0x07);
			volatile uint32_t *rt = numberToRegister(t);
			imm=(instruction&0xFF)<<2;
			uint32_t address=align(regs->pc + 2, 4)+imm;
			*rt = *((uint32_t*) address);
			return true;
		}
		//MOV (register) T1
		if ((instruction & 0xFF00) == 0x4600) {
			if (!conditionPass())
				return true;
			uint8_t m = (uint8_t)((instruction >> 3) & 0x0F);
			volatile uint32_t *rm = numberToRegister(m);
			uint8_t d = (uint8_t)((instruction & 0x07)+((instruction>>4) & 0x08));
			volatile uint32_t *rd = numberToRegister(d);
			*rd=*rm;
			stackChangeApply();
			return true;
		}
		regs->pc-= 2;
	}
	return false;
}

