/*This file is provided under license: see license.txt*/
/*
 * timer.cpp
 *
 * Created: 29-Aug-19 10:25:01 PM
 *  Author: root
 */
#include "timer.h"

volatile uint8_t replInterrupts[30];
volatile uint32_t replInterruptLr;
volatile uint32_t replIPSR;
volatile uint32_t mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
extern volatile uint32_t breakPointReturnPC;
extern volatile uint32_t stepPointReturnPC;
extern volatile uint32_t breakPointFirstInstructionPC;
volatile replStructRegisters *regs;
volatile replStructRegisters *debugRegs;


static bool replTimerStopFlag = false;
bool replTimerPauseFlag = false;
bool replUSBRunFlag = false;

void replTimerSetStep(void) {
	replTimerPauseFlag = false;
	if (replGetFlag(REPL_IS_BREAK)) {
		replSetFlag(REPL_IS_STEP_CALL_AFTER_BREAK, true);
		mainThreadTime = REPL_STEP_END_TIMER_ARR;
	} else {
#ifndef REPL_STEP_EXECUTE_BY_EMULATOR
		replSetFlag(REPL_STEP, true);
		mainThreadTime = REPL_STEP_START_TIMER_ARR;
#else
		replStepExecute();
		mainThreadTime = REPL_STEP_END_TIMER_ARR;
#endif
	}
	if (replGetFlag(REPL_IS_BREAK)) {
		replBreakPointExecute();	//can call replTimerPause
	}
	if (!replTimerPauseFlag) {
		replSetFlag(REPL_IS_PAUSE, false);
	}
	if ((!replTimerStopFlag) && (!replTimerPauseFlag)) {
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	}
}

void replTimerSetRun(void) {
#ifdef REPL_USE_GSM
	mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
#endif
#ifdef REPL_USE_USB
	mainThreadTime = REPL_RUN_PROCESSOR_CYCLES;
#endif
	replTimerPauseFlag = false;
	if (replGetFlag(REPL_IS_BREAK)) {
		replBreakPointExecute();	//can call replTimerPause
	}
	if (!replTimerPauseFlag) {
		replSetFlag(REPL_IS_PAUSE, false);
#ifdef REPL_USE_USB
		//TIM2->ARR=72000; //need to complete usb transaction
		replUSBRunFlag=true;
		replTimerPauseFlag=true;
#endif
	}
	if ((!replTimerStopFlag) && (!replTimerPauseFlag)) {
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	}
}

void replTimerSetRunNotFromBreakPoint(void) {
#ifdef REPL_USE_GSM
	mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
#endif
#ifdef REPL_USE_USB
	mainThreadTime = REPL_RUN_PROCESSOR_CYCLES;
#endif
	replTimerPauseFlag = false;
	replSetFlag(REPL_IS_PAUSE, false);
	if ((!replTimerStopFlag) && (!replTimerPauseFlag)) {
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	}
}
volatile bool isAlreadySave = false;
volatile uint8_t usbInterrupt = 0x00;
volatile uint8_t usbInterrupt2 = 0x00;

inline void saveInterruptsEnableBits(void) {
	if (!isAlreadySave) {
		memcpy((void*) replInterrupts, (const void*) NVIC->ISER, 30);
		//en.DM00043574.pdf
		uint8_t *pointer = (uint8_t*) NVIC->ICER;
		for (uint8_t i = 0; i < 30; ++i) {
			if (i == 2) {
#ifdef REPL_USE_USB
				*(pointer + i) = ~(replInterrupts[i] & 0x18);//19,20 usb interrupts
#endif
			} else if (i == 3) {
				*(pointer + i) = ~(replInterrupts[i] & 0x10);//Tim2//position in bits 28
			} else if (i == 4) {
#ifdef REPL_USE_GSM
				*(pointer + i) = ~(replInterrupts[i] & 0x20);//Usart1//position in bits 37
#endif
			} else if (i == 5) {
#ifdef REPL_USE_USB
				*(pointer + i) = ~(replInterrupts[i] & 0x04);//usbWakeup interrupt 42
#endif
			} else {
				*(pointer + i) = 0xff;
			}
		}
		isAlreadySave = true;
	}
}

inline void loadInterruptsEnableBits(void) {
	isAlreadySave = false;
	memcpy((void*) NVIC->ISER, (const void*) replInterrupts, 30);
}

inline void saveUsbInterrupt(void) {
	uint8_t *pointer = (uint8_t*) NVIC->ICER;
	usbInterrupt = (*(pointer + 2))&0x18;
	*(pointer + 2) = 0x18;		//off 19,20 usb interrupts
	usbInterrupt2 = (*(pointer + 5))&0x04;
	*(pointer + 5) = 0x04;		//off 42 usbWakeup interrupt
}

inline void loadUsbInterrupt(void) {
	uint8_t *pointer = (uint8_t*) NVIC->ISER;
	*(pointer + 2) = usbInterrupt;//on 19,20 usb interrupts
	*(pointer + 5) = usbInterrupt2;	//on 42 usbWakeup interrupt
}

//volatile uint16_t a;

volatile void replThread(void) {
#ifdef  REPL_USE_GSM
	modemConfig();
	modemEstablishConnection();
#endif
#ifdef  REPL_USE_USB
	usb_main();
	usbProcessing();
#endif
}

volatile bool replFirstStart = true;
volatile bool replCallFromISR = false;
volatile uint8_t debugStack[1000];	//start size from something
volatile uint32_t replBStack, replDStack;

//#define SP  _SFR_IO16(0x3D)

void replTimerInit(void) {
	replDStack = (uint32_t) &debugStack + 999;
//replSetFlag(REPL_ISBREAK,false);
	NVIC_InitTypeDef nvicStructure;
	nvicStructure.NVIC_IRQChannel = TIM2_IRQn;
	nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
	nvicStructure.NVIC_IRQChannelSubPriority = 0;
	nvicStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvicStructure);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	TIM_TimeBaseInitTypeDef timerInitStructure;
	timerInitStructure.TIM_Prescaler = 1;
	timerInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	timerInitStructure.TIM_Period = REPL_DEFAULT_PROCESSOR_CYCLES;
	timerInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	timerInitStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM2, &timerInitStructure);
	TIM_Cmd(TIM2, ENABLE);
//NVIC_EnableIRQ (TIM2_IRQn);
   TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void replTimerStop(void) {
	TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
	replTimerStopFlag = true;
}

void replTimerResume(void) {
	replTimerStopFlag = false;
	if (!replTimerPauseFlag)
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
}

void replTimerPause(void) {
	TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
	replSetFlag(REPL_IS_PAUSE, true);
	replTimerPauseFlag = true;
}

volatile uint32_t prevPc = 0;
static uint32_t usartCR1;

extern uint8_t flashBuff[REPL_BUFFER_SIZE];

//void test(){
//	static bool first =true;
//	if(first){
//	replSetBreakPoint(0x80056ba);
//	first=false;
//	}
//	if(replGetFlag(REPL_IS_BREAK)){
//		//replTimerSetStep();
//		replTimerResumeFromPause();
//	}
//}
volatile bool handleMode = false;	//If cpu isn't threadMode
/*call from asm linked by ld script*/
void replTimerISR(void) {
	TIM2->CR1 &= (uint16_t) ~TIM_CR1_CEN;
	TIM2->CNT = 0;
#ifdef REPL_USE_GSM
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	//TIM2->DIER|=0x01;//Set it update, some times it resets, but interrupt manages to work
#endif
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	}
//	setPinInput (PIN_MODEM_RTS);
//	setPin(PIN_MODEM_RTS);
//	TCCR1B = 0;
//	TCNT1 = 0;
//	TIFR1 |= 1 << OCF1A;
	if (replCallFromISR) {
#ifdef REPL_USE_USB
		TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
		//TIM2->DIER&=~0x01;
		saveUsbInterrupt();
#endif
		replCallFromISR = false;
		//cli();
		//replDStack=SP;
		//SP=replBStack;
		//restartTimerToCallRepl;
		//mainThreadTime
		if (replGetFlag(REPL_STEP)
				|| replGetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION)
				|| replGetFlag(REPL_STEP_POINT_FIRST_INSTRUCTION)) {
#ifdef REPL_USE_GSM
			usartCR1 = USART1->CR1;
			USART1->CR1 &= ~((1 << USART_CR1_IDLEIE) | (1 << USART_CR1_RXNEIE)
					| (1 << USART_CR1_TCIE) | (1 << USART_CR1_TXEIE)
					| (1 << USART_CR1_PEIE));
			#endif
		} else {
			loadInterruptsEnableBits();
		}
		TIM2->ARR = mainThreadTime;		//17-19
//		OCR1A = mainThreadTime;	//75 and below - 1 processor clock  76 - 2 clocks .....
	} else {
#ifdef REPL_USE_USB
		TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
		//TIM2->DIER&=~0x01;
		loadUsbInterrupt();
#endif
		replCallFromISR = true;
		saveInterruptsEnableBits();
		//replBStack=SP;
		//SP=replDStack;
		regs = (volatile replStructRegisters*) (replBStack);
		replInterruptLr = regs->interruptLr;
		replIPSR = regs->psr & 0x1ff;
		if (!replFirstStart) {
			debugRegs = (volatile replStructRegisters*) (replDStack);
			if ((replInterruptLr & 0x08) == 0) {
				handleMode = true;
				debugRegs->psr = (debugRegs->psr & 0xFFFFFE00) + replIPSR;
				//debugRegs->psr|=0x1000000;
				////set_xPSR(replBStack);
				//regs->interruptLr=debugRegs->interruptLr;
				debugRegs->interruptLr &= 0xFFFFFFF7;
			} else if (handleMode) {
				handleMode = false;
				debugRegs->interruptLr |= 0x08;
				debugRegs->psr = (debugRegs->psr & 0xFFFFFE00);
			}
		}
		//pc not change after step increase timer arr value
		if (replGetFlag(REPL_STEP) && regs->pc == prevPc) {
			if (mainThreadTime < REPL_STEP_END_TIMER_ARR) {
				++mainThreadTime;
				TIM2->ARR = 1;
				TIM2->CR1 |= TIM_CR1_CEN;
				return;
			}
		}
		prevPc = regs->pc;
		if ((!replGetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION))
				&& (!replGetFlag(REPL_STEP_POINT_FIRST_INSTRUCTION))) {
			if (replReadFlash(regs->pc) == 0xfe
					&& replReadFlash(regs->pc + 1) == 0xe7
					&& replCheckBreakPoint()) {
				replSetFlag(REPL_IS_BREAK, true);
				mainThreadTime = REPL_STEP_END_TIMER_ARR;
				if (replGetFlag(REPL_STEP)) {
					USART1->CR1 = usartCR1;
					replSetFlag(REPL_IS_STEP, true);
					replSetFlag(REPL_STEP, false);
				}
				replTimerPause();
			} else {
				replSetFlag(REPL_IS_BREAK, false);
			}
		} else {
			replSetFlag(REPL_IS_BREAK, false);
		}

		if (replGetFlag(REPL_STEP)) {
			USART1->CR1 = usartCR1;
			replSetFlag(REPL_IS_STEP, true);
			mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
			replSetFlag(REPL_STEP, false);
			replTimerPause();
		} else {
			replSetFlag(REPL_IS_STEP, false);
		}
		if (replGetFlag(REPL_IS_STEP)) {
			replReadFlashNearProgramCounter();
		}
		TIM2->ARR = REPL_DEFAULT_PROCESSOR_CYCLES;
//		OCR1A = REPL_DEFAULT_PROCESSOR_CYCLES;		//0xffff;
		if (replGetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION)) {
			USART1->CR1 = usartCR1;
			replSetFlag(REPL_BREAK_POINT_FIRST_INSTRUCTION, false);
			mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
			regs->pc = breakPointReturnPC;
			//OCR1A=1;
			//TIM2->ARR = 1;		//reduce debug flow time
			if (replGetFlag(REPL_IS_STEP_CALL_AFTER_BREAK)) {
				replSetFlag(REPL_IS_STEP, true);
				replSetFlag(REPL_IS_STEP_CALL_AFTER_BREAK, false);
				replTimerPause();
			}
		}
		if (replGetFlag(REPL_STEP_POINT_FIRST_INSTRUCTION)) {
			USART1->CR1 = usartCR1;
			replSetFlag(REPL_STEP_POINT_FIRST_INSTRUCTION, false);
			mainThreadTime = REPL_DEFAULT_PROCESSOR_CYCLES;
			regs->pc = stepPointReturnPC;
			replSetFlag(REPL_IS_STEP, true);
			replTimerPause();
			replReadFlashNearProgramCounter();
		}
		//test();
		//replEmulate();
		//restartTimerToPauseRepl;
		if (replFirstStart) {
			replIPSR |= 0x1000000;
//			clearPin(PIN_MODEM_RTS);
//			setPinOut(PIN_MODEM_RTS);
//			TCCR1B = 1 << CS10;		//no prescales
//			sei();
			//repl();		//never return
		}
	}
	TIM2->CR1 |= TIM_CR1_CEN;
//	clearPin(PIN_MODEM_RTS);
//	setPinOut(PIN_MODEM_RTS);
//	TCCR1B = 1 << CS10;		//no prescales
	return;
}
