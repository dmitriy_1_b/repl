/*This file is provided under license: see license.txt*/
/*
 * step.h
 *
 *  Created on: Mar 9, 2020
 *      Author: root
 */

#ifndef REPL_STEP_H_
#define REPL_STEP_H_
#include "../config.h"

void replStepExecute(void);


#endif /* REPL_STEP_H_ */
