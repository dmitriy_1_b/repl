/*This file is provided under license: see license.txt*/
/*
 * buffer.h
 *
 * Created: 06-Sep-19 11:53:25 PM
 *  Author: root
 */ 


#ifndef BUFFER_H_
#define BUFFER_H_
#include "../config.h"
uint8_t flashBuff[REPL_BUFFER_SIZE];

inline void replBufferWriteByte(uint16_t address,uint8_t byte){
	flashBuff[address]=byte;
}

inline uint8_t replBufferReadByte(uint16_t address){
	return flashBuff[address];
}



#endif /* BUFFER_H_ */