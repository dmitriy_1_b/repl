/*This file is provided under license: see license.txt*/
#include "modem.h"

volatile unsigned char csqLevel = 0;
volatile boolean resetByRepl = false;
volatile uint8_t cipStatus = 0;

#define BASE_DELAY_MS 500

boolean modemConnect() {

	if (!modemSendATCommand(F("AT+CSQ"), F("CSQ:"), F(",0"))) {
		return false;
	}
//	if (!modemSendATCommand(F("AT+IFC=2,2"),F("OK"),F(""))) {
//		return false;
//	}
	char baud[15];
	baud[0] = 0x00;
	strcpy(baud, "AT+IPR=");
	utoa(BAUDRATE_END, baud + 7, 10);
	if (!modemSendATCommand(baud, F("OK"), F(""))) {
		return false;
	}
	uartSetBaud(BAUDRATE_END);
	//_delay_ms(BASE_DELAY_MS);
	if (!modemSendATCommand(F("AT+CREG?"), F("+CREG:"), F("0,1"))) {
		return false;
	}
	replDelayMs(BASE_DELAY_MS);
	if (!modemSendATCommand(F("AT+CSTT?"), F("OK"), F(APN_ONLY))) {
		if (!modemSendATCommand(AT_COMMAND_APN, "OK", "")) {
			return false;
		}
		replDelayMs(BASE_DELAY_MS);
	}
	if (modemSendATCommand(F("AT+CIPSTATUS"), "", "")) {
		if ((cipStatus == CIPSTATUS_PDP_DEACT)
				|| (cipStatus == CIPSTATUS_TCP_CLOSED)
				|| (cipStatus == CIPSTATUS_TCP_CLOSING)) {
			modemSendATCommand(F("AT+CIPSHUT"), F("SHUT OK"), F(""));
			replDelayMs(BASE_DELAY_MS);
			if (!modemSendATCommand(AT_COMMAND_APN, "OK", "")) {
				return false;
			}
			replDelayMs(BASE_DELAY_MS);
			if (!modemSendATCommand(F("AT+CIICR"), F("OK"), F(""))) {
				return false;
			}
		} else if (cipStatus == CIPSTATUS_IP_GPRSACT) {

		} else if (cipStatus == CIPSTATUS_CONNECT_OK) {
			return true;
		} else if (cipStatus == CIPSTATUS_IP_INITIAL) {
			if (!modemSendATCommand(F(AT_COMMAND_APN),F( "OK"),F( ""))) {
				return false;
			}
			if (!modemSendATCommand(F("AT+CIICR"), F("OK"), F(""))) {
				return false;
			}
		} else {
			if (!modemSendATCommand(F("AT+CIICR"), F("OK"), F(""))) {
				return false;
			}
		}
	}
	replDelayMs(BASE_DELAY_MS);
	if (!modemSendATCommand(F("AT+CIFSR"), F("."), F(""))) {
		return false;
	}
	//_delay_ms(BASE_DELAY_MS);
	if (!modemSendATCommand(F(AT_COMMAND_CIPSTART),
			"", "")) {
		return false;
	}
	replDelayMs(BASE_DELAY_MS);
	return true;
}

/**
 *
 */
boolean modemSendATCommand(const char *atcommand, const char *mustInResponse1,
		const char *mustInResponse2) {
	while (uartAvailable()) {
		uartRead();
	}
	uartPrint(atcommand);
	uartPrint("\r\n");
	if ((strstr(atcommand, "AT+CIPSTATUS") != NULL)||
			(strstr(atcommand, "AT+CIPSTART") != NULL)||
			(strstr(atcommand, "AT+CIICR") != NULL)) {
					replDelayMs(BASE_DELAY_MS);
	}
	unsigned long int delayCounter = 0;
	while (true) {
		if (delayCounter >= MODEM_COMMAND_MAX_TIME_MS) {
			return false;
		}
		if (uartAvailable()) {
			char response[100];
			response[0] = 0x00;
			int resLeng = 0;
			while (uartAvailable()) {
				response[resLeng] = (char) uartRead();
				if (resLeng < 98) {
					++resLeng;
					response[resLeng] = 0x00;
				}
			}

			if (strstr(response, "ERROR") != NULL) {
				if ((strstr(response, "AT+CIPSTART") != NULL)
						&& (strstr(response, "ALREADY CONNECT") != NULL)) {
					return true;
				}
				return false;
			}

			if (strstr(response, "AT+CSQ") != NULL) {
				char *stStart = strstr(response, "+CSQ: ");
				char *stEnd = NULL;
				if (stStart != NULL) {
					stEnd = strstr(stStart, ",");
				}
				if (stStart == NULL || stEnd == NULL) {
					return false;
				}
				stStart += 6;
				stEnd[0] = 0x00;
				csqLevel = atol(stStart);
				return true;
			}

			if (strstr(response, "AT+CIPSTATUS") != NULL) {
				if (strstr(response, "IP START") != NULL) {
					cipStatus = CIPSTATUS_IP_START;
				} else if (strstr(response, "PDP DEACT") != NULL) {
					cipStatus = CIPSTATUS_PDP_DEACT;
				} else if (strstr(response, "IP INITIAL") != NULL) {
					cipStatus = CIPSTATUS_IP_INITIAL;
				} else if (strstr(response, "IP GPRSACT") != NULL) {
					cipStatus = CIPSTATUS_IP_GPRSACT;
				} else if (strstr(response, "CONNECT OK") != NULL) {
					cipStatus = CIPSTATUS_CONNECT_OK;
				} else if (strstr(response, "TCP CLOSED") != NULL) {
					cipStatus = CIPSTATUS_TCP_CLOSED;
				} else if (strstr(response, "TCP CLOSING") != NULL) {
					cipStatus = CIPSTATUS_TCP_CLOSING;
				} else if (strstr(response, "TCP CONNECTING") != NULL) {
					replDelayMs(1000);
					cipStatus = CIPSTATUS_CONNECT_OK;
				} else if (strstr(response, "IP CONFIG") != NULL) {
					cipStatus = CIPSTATUS_IP_CONFIG;
				} else {
					cipStatus = 0;
				}
				return true;
			}

			if (strstr(response, "AT+CIPSTART") != NULL) {
				if (strstr(response, "CONNECT OK") != NULL) {
					return true;
				} else if (strstr(response, "ALREADY CONNECT") != NULL) {
					return true;
				} else {
					return false;
				}
			}

			if (strlen(mustInResponse1) != 0) {
				if (strstr(response, mustInResponse1) == NULL)
					return false;
			}

			if (strlen(mustInResponse2) != 0) {
				if (strstr(response, mustInResponse2) == NULL)
					return false;
				else
					return true;
			} else {
				return true;
			}
		}
		replDelayMs(100);
		delayCounter += 100;
	}
}

/**
 *
 */
void modemCloseConnection()
{
	modemSendATCommand(F("AT+CIPCLOSE=1"), F("CLOSE OK"), F(""));
	modemSendATCommand(F("AT+CIPSHUT"), F("SHUT OK"), F(""));
}

/**
 *
 */
boolean modemEstablishConnection()
{
	boolean first = true;
	while (true) {
		if (!resetByRepl) {
			if (!first) {
				first = false;
				modemCloseConnection();
				modemTurnOff();
			}
			modemTurnOn();
			if (!modemWaitNetworkConnection()) {
				continue;
			}
			if (!modemConnect()) {
				continue;
			}
		} else {
			resetByRepl = false;
		}
		while (true) {
			if (!modemSendData()) {
				modemSendATCommand(F("AT+CIPCLOSE=1"), F("CLOSE OK"), F(""));
				replDelayMs(BASE_DELAY_MS);
				if (!modemSendATCommand(
						"AT+CIPSTART=\"TCP\",\"repl.dmtests.ru\",\"80\"",
						"CONNECT OK", "")) {
					break;
				}
				replDelayMs(BASE_DELAY_MS);
			}
			replDelayMs(10);
		}
	}
	//closeConnection();
	//turnOffModem();
	return true;
}

void modemConfig() {
	uartBegin(BAUDRATE_START);
//	#ifndef FIRMWARE_WITHOUT_TRANSISTOR
//	//pinMode(pwSwGPRSPin, OUTPUT);       // Выход power switch pin PD7
//	setPinOut(PIN_POWER_SWITCH);
//	#else
//	//pinMode(pwSwGPRSPin, INPUT);       // Выход power switch pin PD7
//	setPinInput(PIN_POWER_SWITCH);
//	#endif
//	//digitalWrite(pwSwGPRSPin,LOW);
//	clearPin(PIN_POWER_SWITCH);
//	//pinMode(pwStatusGPRSPin, INPUT);      // Status inpit from SIM900
//	setPinInput(PIN_POWER_STATUS);
}

boolean modemWaitNetworkConnection() {
//	setPinOut(PIN_MODEM_RTS);
	unsigned long int counter = 0;
	bool changeBaud = true;
	while (!modemSendATCommand(F("AT"), F("OK"), F(""))) {
		if (counter > MODEM_AT_MAX_TIMES) {
//			errorPush(ERROR_NOT_RESPONSE_TO_AT);
			return false;
		}
		if (changeBaud) {
			uartSetBaud(BAUDRATE_END);
			changeBaud = false;
		} else {
			uartSetBaud(BAUDRATE_START);
			changeBaud = true;
		}
		counter += 1;
		replDelayMs(BASE_DELAY_MS);
	}
	if (!modemSendATCommand("AT+CPIN?", "+CPIN:", "READY")) {
		return false;
	}
	counter = 0;
	while (!modemSendATCommand(F("AT+CGATT?"), F("+CGATT:"), F("1"))) {
		if (counter > MODEM_TURN_ON_MAX_TIME_MS) {
//			errorPush(ERROR_NOT_CONNECT_TO_NETWORK);
			return false;
		}
		counter += 1000;
		replDelayMs(BASE_DELAY_MS);
	}
	return true;
}

void modemTurnOffPins() {
	////Destroy GSM TX
	//DDRD&=~(1<PIND5);
	//PORTD&=~(1<PIND5);
	////Destroy GSM Rx
	//PORTD&=~(1<PIND6);
	//_delay_us(1);
	//digitalWrite(txGsmPin,LOW);
//	UCSR0B&=~((1<<RXEN0)|(1<<TXEN0));
//	clearPin(PIN_TX_GSM);
//	//pinMode(txGsmPin, INPUT);
//	setPinInput(PIN_TX_GSM);
//	//digitalWrite(rxGsmPin,LOW);
//	clearPin(PIN_RX_GSM);
}

void modemTurnOnPins() {
	////Destroy GSM TX
	//DDRD|=(1<PIND5);
	//PORTD|=(1<PIND5);
	////Destroy GSM Rx
	//PORTD|=(1<PIND6);
	//_delay_us(1);
	//digitalWrite(txGsmPin,HIGH);
//	setPin(PIN_TX_GSM);
//	//pinMode(txGsmPin, OUTPUT);
//	setPinOut(PIN_TX_GSM);
//	//digitalWrite(rxGsmPin,HIGH);
//	setPin(PIN_RX_GSM);
//	UCSR0B|=(1<<RXEN0)|(1<<TXEN0);
}

void modemReadResetByRepl() {
//	uint8_t b = eepromReadByte(EEPROM_RESET_BY_REPL_OFFSET);
//	if(b==EEPROM_RESET_BY_REPL_VALUE){
//		resetByRepl=true;
//		eepromWriteByte(EEPROM_RESET_BY_REPL_OFFSET,EEPROM_NOT_RESET_BY_REPL_VALUE);
//		setPinOut(PIN_TRANSISTOR);
//		modemTurnOnPins();
//	}
	resetByRepl = false;
}

void modemSetResetByRepl() {
	//eepromWriteByte(EEPROM_RESET_BY_REPL_OFFSET,EEPROM_RESET_BY_REPL_VALUE);
}
