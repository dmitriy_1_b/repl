/*This file is provided under license: see license.txt*/

/* 
 * File:   main.cpp
 * Author: root
 *
 * Created on June 25, 2019, 9:23 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cstring>
#include <algorithm>    
#include <vector>       
#include <dirent.h>
#include <stdio.h>

using namespace std;
#define ELFDATA2LSB 1
#define ELFDATA2MSB 2
#define ELFCLASS32 1 
#define ELFCLASS64 2
#define CONFIG "./config.properties"
char OBJECTS_FOLDER[1024];
//#define OBJECTS_FOLDER "C:\\projects\\repl_stm32\\Debug\\src\\user\\"
//#define OBJECTS_FOLDER "./user/"
const uint8_t ELF_MAGIC[4] = {0x7f, 'E', 'L', 'F'};
#define _A_SUBDIR 0x10
/*
 * 
 */
void swap4(uint8_t* bytes)
{
    uint8_t b = bytes[0];
    bytes[0] = bytes[3];
    bytes[3] = b;
    b = bytes[1];
    bytes[1] = bytes[2];
    bytes[2] = b;
}

void swap2(uint8_t* bytes)
{
    uint8_t b = bytes[0];
    bytes[0] = bytes[1];
    bytes[1] = b;
}

uint8_t strings[394];

void replaceAll(uint8_t* source, uint32_t sourceSize, uint8_t* search, uint8_t* replace, uint8_t size)
{
    uint8_t* p = std::search(source, source + sourceSize, search, search + size);
    while (p < source + sourceSize)
    {
        memcpy(p, replace, size);
        p = std::search(source, source + sourceSize, search, search + size);
    }
}

void replaceAllStrings(uint8_t* source, uint32_t sourceSize, string search, string replace)
{
    replaceAll(source, sourceSize, (uint8_t*) search.c_str(), (uint8_t*) replace.c_str(), search.length());
}

void modifyElf(const char* file)
{
    fstream elfFile;
    uint32_t sectionHeaderOffset;
    uint8_t dataEncoding;
    uint8_t fileClass;
    uint8_t elfMagic[4];
    uint16_t sectionHeaderEntitySize;
    uint16_t sectionHeaderIndexForStrings;
    uint32_t sectionStringsOffset;
    uint32_t sectionStringsSize;
    elfFile.open(file, ios::in | ios::out | ios::binary);
    elfFile.seekg(0, ios::beg);
    elfFile.read((char*) &elfMagic, 4);
    if (memcmp(elfMagic, ELF_MAGIC, 4) != 0)
    {
        printf("FILE isn't elf!");
        exit(EXIT_FAILURE);
    }
    elfFile.seekg(4, ios::beg);
    elfFile.read((char*) &fileClass, 1);
    if (fileClass == ELFCLASS64)
    {
        printf("ELF FILE CLASS 64 not supported!");
        exit(EXIT_FAILURE);
    }
    elfFile.seekg(5, ios::beg);
    elfFile.read((char*) &dataEncoding, 1);
    elfFile.seekg(32, ios::beg);
    elfFile.read((char*) &sectionHeaderOffset, 4);
    if (dataEncoding == ELFDATA2MSB)
        swap4((uint8_t*) & sectionHeaderOffset);
    elfFile.seekg(46, ios::beg);
    elfFile.read((char*) &sectionHeaderEntitySize, 2);
    if (dataEncoding == ELFDATA2MSB)
        swap2((uint8_t*) & sectionHeaderEntitySize);
    elfFile.seekg(50, ios::beg);
    elfFile.read((char*) &sectionHeaderIndexForStrings, 2);
    if (dataEncoding == ELFDATA2MSB)
        swap2((uint8_t*) & sectionHeaderIndexForStrings);
    elfFile.seekg(sectionHeaderOffset + ((uint32_t) sectionHeaderEntitySize) * sectionHeaderIndexForStrings + 16, ios::beg);
    elfFile.read((char*) &sectionStringsOffset, 4);
    if (dataEncoding == ELFDATA2MSB)
        swap4((uint8_t*) & sectionStringsOffset);
    elfFile.seekg(sectionHeaderOffset + ((uint32_t) sectionHeaderEntitySize) * sectionHeaderIndexForStrings + 20, ios::beg);
    elfFile.read((char*) &sectionStringsSize, 4);
    if (dataEncoding == ELFDATA2MSB)
        swap4((uint8_t*) & sectionStringsSize);
    //uint8_t strings[sectionStringsSize];
    elfFile.seekg(sectionStringsOffset, ios::beg);
    elfFile.read((char*) &strings, sectionStringsSize);

    replaceAllStrings(strings, sectionStringsSize, ".text", ".texz");
    replaceAllStrings(strings, sectionStringsSize, ".data", ".datz");
    replaceAllStrings(strings, sectionStringsSize, ".rodata", ".rodatz");
    replaceAllStrings(strings, sectionStringsSize, ".progmem", ".progmez");
    replaceAllStrings(strings, sectionStringsSize, ".bss", ".bsz");
    replaceAllStrings(strings, sectionStringsSize, ".ctors", ".ctorz");
    replaceAllStrings(strings, sectionStringsSize, ".dtors", ".dtorz");
    replaceAllStrings(strings, sectionStringsSize, ".init_array", ".init_arraz");
    replaceAllStrings(strings, sectionStringsSize, ".fini_array", ".fini_arraz");

    elfFile.seekp(sectionStringsOffset, ios::beg);
    elfFile.write((char*) strings, sectionStringsSize);
    elfFile.close();
}

void strTrim(char* str) {
    int pos = strlen(str) - 1;
    while ((str[pos] == '\n') || (str[pos] == '\r') || (str[pos] == '\t') || (str[pos] == ' ')) {
        str[pos] = 0x00;
        if (pos > 0) {
            --pos;
        } else {
            break;
        }
    }
}

void modifyFolder(const char *folder){
    DIR *d;
    struct dirent *dir;
    d = opendir(folder);
    if (d)
    {
        while ((dir = readdir(d)) != NULL)
        {            
            int len = strlen(dir->d_name);
            string str(dir->d_name,len);
            if (len > 2 && str[len - 1] == 'o' && str[len - 2] == '.')
            {
                str=folder+str;
                modifyElf(str.c_str());
            }else if((str.rfind('.')==std::string::npos) && 
                    ((dir->d_type&_A_SUBDIR)==_A_SUBDIR)){
                str=folder+str+"/";
                modifyFolder(str.c_str());
            }
        }
        closedir(d);
    }
}

int main(int argc, char** argv)
{
    FILE * config = fopen(CONFIG, "r");
    char str[1024];
    while (!feof(config)) {
        if (fgets(str, 1023, config) != NULL) {
            char* p = strchr(str, '=');
            if(p==NULL)
                break;
            *p = 0x00;
            if (strcmp(str, "OBJECTS_FOLDER") == 0) {
                ++p;
                strcpy(OBJECTS_FOLDER, p);
                strTrim(OBJECTS_FOLDER);
            }
        } else {
            break;
        }
    }
    fclose(config);
    modifyFolder(OBJECTS_FOLDER);
    return 0;
}

