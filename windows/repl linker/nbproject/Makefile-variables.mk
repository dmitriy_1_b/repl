#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=MinGW64-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/MinGW64-Windows
CND_ARTIFACT_NAME_Debug=repl_linker
CND_ARTIFACT_PATH_Debug=dist/Debug/MinGW64-Windows/repl_linker
CND_PACKAGE_DIR_Debug=dist/Debug/MinGW64-Windows/package
CND_PACKAGE_NAME_Debug=repllinker.tar
CND_PACKAGE_PATH_Debug=dist/Debug/MinGW64-Windows/package/repllinker.tar
# Release configuration
CND_PLATFORM_Release=MinGW-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-Windows
CND_ARTIFACT_NAME_Release=repl_linker
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-Windows/repl_linker
CND_PACKAGE_DIR_Release=dist/Release/MinGW-Windows/package
CND_PACKAGE_NAME_Release=repllinker.tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-Windows/package/repllinker.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
