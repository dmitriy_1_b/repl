#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=i686-w64-mingw32-gcc
CCC=i686-w64-mingw32-g++
CXX=i686-w64-mingw32-g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=MinGW64-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/19e7b8f/TcpClient.o \
	${OBJECTDIR}/_ext/19e7b8f/TcpServer.o \
	${OBJECTDIR}/_ext/19e7b8f/Utils.o \
	${OBJECTDIR}/_ext/19e7b8f/hiddata.o \
	${OBJECTDIR}/_ext/19e7b8f/hidtool.o \
	${OBJECTDIR}/_ext/451a3196/Buffer.o \
	${OBJECTDIR}/_ext/451a3196/Controller.o \
	${OBJECTDIR}/_ext/451a3196/GDBDecoder.o \
	${OBJECTDIR}/_ext/451a3196/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-Wno-pointer-arith
CXXFLAGS=-Wno-pointer-arith

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-lhid -lsetupapi -lws2_32

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_gdbserver_avr_usb.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_gdbserver_avr_usb.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	g++ -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_gdbserver_avr_usb ${OBJECTFILES} ${LDLIBSOPTIONS} -static -static-libgcc -static-libstdc++

${OBJECTDIR}/_ext/19e7b8f/TcpClient.o: ../../repl_common/windows/TcpClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/19e7b8f
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/19e7b8f/TcpClient.o ../../repl_common/windows/TcpClient.cpp

${OBJECTDIR}/_ext/19e7b8f/TcpServer.o: ../../repl_common/windows/TcpServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/19e7b8f
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/19e7b8f/TcpServer.o ../../repl_common/windows/TcpServer.cpp

${OBJECTDIR}/_ext/19e7b8f/Utils.o: ../../repl_common/windows/Utils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/19e7b8f
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/19e7b8f/Utils.o ../../repl_common/windows/Utils.cpp

${OBJECTDIR}/_ext/19e7b8f/hiddata.o: ../../repl_common/windows/hiddata.c
	${MKDIR} -p ${OBJECTDIR}/_ext/19e7b8f
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/19e7b8f/hiddata.o ../../repl_common/windows/hiddata.c

${OBJECTDIR}/_ext/19e7b8f/hidtool.o: ../../repl_common/windows/hidtool.c
	${MKDIR} -p ${OBJECTDIR}/_ext/19e7b8f
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/19e7b8f/hidtool.o ../../repl_common/windows/hidtool.c

${OBJECTDIR}/_ext/451a3196/Buffer.o: ../../repl_gdbserver_avr/Buffer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/451a3196
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/451a3196/Buffer.o ../../repl_gdbserver_avr/Buffer.cpp

${OBJECTDIR}/_ext/451a3196/Controller.o: ../../repl_gdbserver_avr/Controller.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/451a3196
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/451a3196/Controller.o ../../repl_gdbserver_avr/Controller.cpp

${OBJECTDIR}/_ext/451a3196/GDBDecoder.o: ../../repl_gdbserver_avr/GDBDecoder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/451a3196
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/451a3196/GDBDecoder.o ../../repl_gdbserver_avr/GDBDecoder.cpp

${OBJECTDIR}/_ext/451a3196/main.o: ../../repl_gdbserver_avr/main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/451a3196
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/451a3196/main.o ../../repl_gdbserver_avr/main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
