#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/4b0cfd00/TcpClient.o \
	${OBJECTDIR}/_ext/4b0cfd00/TcpServer.o \
	${OBJECTDIR}/_ext/4b0cfd00/Utils.o \
	${OBJECTDIR}/_ext/4b0cfd00/core.o \
	${OBJECTDIR}/_ext/4b0cfd00/hiddata.o \
	${OBJECTDIR}/_ext/4b0cfd00/hidtool.o \
	${OBJECTDIR}/_ext/6850d604/Buffer.o \
	${OBJECTDIR}/_ext/6850d604/Controller.o \
	${OBJECTDIR}/_ext/6850d604/GDBDecoder.o \
	${OBJECTDIR}/_ext/6850d604/main.o


# C Compiler Flags
CFLAGS=`lpkg-config libusb-1.0 --cflags` 

# CC Compiler Flags
CCFLAGS=-Wno-pointer-arith -pedantic -pthread `pkg-config libusb-1.0 --cflags` 
CXXFLAGS=-Wno-pointer-arith -pedantic -pthread `pkg-config libusb-1.0 --cflags` 

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_gdbserver_stm32

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_gdbserver_stm32: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_gdbserver_stm32 ${OBJECTFILES} ${LDLIBSOPTIONS} -L/usr/lib/x86_64-linux-gnu/ -ludev -static-libgcc -static-libstdc++ `pkg-config libusb-1.0 --libs` -lpthread

${OBJECTDIR}/_ext/4b0cfd00/TcpClient.o: ../../repl_common/linux/TcpClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/TcpClient.o ../../repl_common/linux/TcpClient.cpp

${OBJECTDIR}/_ext/4b0cfd00/TcpServer.o: ../../repl_common/linux/TcpServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/TcpServer.o ../../repl_common/linux/TcpServer.cpp

${OBJECTDIR}/_ext/4b0cfd00/Utils.o: ../../repl_common/linux/Utils.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/Utils.o ../../repl_common/linux/Utils.cpp

${OBJECTDIR}/_ext/4b0cfd00/core.o: ../../repl_common/linux/core.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/core.o ../../repl_common/linux/core.c

${OBJECTDIR}/_ext/4b0cfd00/hiddata.o: ../../repl_common/linux/hiddata.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/hiddata.o ../../repl_common/linux/hiddata.c

${OBJECTDIR}/_ext/4b0cfd00/hidtool.o: ../../repl_common/linux/hidtool.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/hidtool.o ../../repl_common/linux/hidtool.c

${OBJECTDIR}/_ext/6850d604/Buffer.o: ../../repl_gdbserver_stm32/Buffer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6850d604
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6850d604/Buffer.o ../../repl_gdbserver_stm32/Buffer.cpp

${OBJECTDIR}/_ext/6850d604/Controller.o: ../../repl_gdbserver_stm32/Controller.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6850d604
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6850d604/Controller.o ../../repl_gdbserver_stm32/Controller.cpp

${OBJECTDIR}/_ext/6850d604/GDBDecoder.o: ../../repl_gdbserver_stm32/GDBDecoder.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6850d604
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6850d604/GDBDecoder.o ../../repl_gdbserver_stm32/GDBDecoder.cpp

${OBJECTDIR}/_ext/6850d604/main.o: ../../repl_gdbserver_stm32/main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6850d604
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6850d604/main.o ../../repl_gdbserver_stm32/main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
