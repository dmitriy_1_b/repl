#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=repl_linker
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/repl_linker
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=repllinker.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/repllinker.tar
# Release configuration
CND_PLATFORM_Release=MinGW-Windows
CND_ARTIFACT_DIR_Release=dist/Release/MinGW-Windows
CND_ARTIFACT_NAME_Release=repl_linker__x86_
CND_ARTIFACT_PATH_Release=dist/Release/MinGW-Windows/repl_linker__x86_
CND_PACKAGE_DIR_Release=dist/Release/MinGW-Windows/package
CND_PACKAGE_NAME_Release=repllinker(x86).tar
CND_PACKAGE_PATH_Release=dist/Release/MinGW-Windows/package/repllinker(x86).tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
