#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/4b0cfd00/TcpClient.o \
	${OBJECTDIR}/_ext/4b0cfd00/TcpServer.o \
	${OBJECTDIR}/_ext/54359e09/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_firmware_sender

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_firmware_sender: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/repl_firmware_sender ${OBJECTFILES} ${LDLIBSOPTIONS} -static -static-libgcc -static-libstdc++

${OBJECTDIR}/_ext/4b0cfd00/TcpClient.o: ../../repl_common/linux/TcpClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/TcpClient.o ../../repl_common/linux/TcpClient.cpp

${OBJECTDIR}/_ext/4b0cfd00/TcpServer.o: ../../repl_common/linux/TcpServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/4b0cfd00
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4b0cfd00/TcpServer.o ../../repl_common/linux/TcpServer.cpp

${OBJECTDIR}/_ext/54359e09/main.o: ../../repl_firmware_sender/main.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/54359e09
	${RM} "$@.d"
	$(COMPILE.cc) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/54359e09/main.o ../../repl_firmware_sender/main.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
