#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug/GNU-Linux
CND_ARTIFACT_NAME_Debug=repl_firmware_sender
CND_ARTIFACT_PATH_Debug=dist/Debug/GNU-Linux/repl_firmware_sender
CND_PACKAGE_DIR_Debug=dist/Debug/GNU-Linux/package
CND_PACKAGE_NAME_Debug=replfirmwaresender.tar
CND_PACKAGE_PATH_Debug=dist/Debug/GNU-Linux/package/replfirmwaresender.tar
# Release configuration
CND_PLATFORM_Release=None-Linux
CND_ARTIFACT_DIR_Release=dist/Release/None-Linux
CND_ARTIFACT_NAME_Release=repl_firmware_sender
CND_ARTIFACT_PATH_Release=dist/Release/None-Linux/repl_firmware_sender
CND_PACKAGE_DIR_Release=dist/Release/None-Linux/package
CND_PACKAGE_NAME_Release=replfirmwaresender.tar
CND_PACKAGE_PATH_Release=dist/Release/None-Linux/package/replfirmwaresender.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
